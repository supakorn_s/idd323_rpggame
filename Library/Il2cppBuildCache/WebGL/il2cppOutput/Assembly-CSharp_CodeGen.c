﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 UnityEngine.InputSystem.InputActionAsset Input_master::get_asset()
extern void Input_master_get_asset_mB578FB991F84D0700B5FA9B4B34C7048BE5637EF (void);
// 0x00000002 System.Void Input_master::.ctor()
extern void Input_master__ctor_m05570375675155D2756C12748D40E3A55E995118 (void);
// 0x00000003 System.Void Input_master::Dispose()
extern void Input_master_Dispose_m4E1AB80951FBD83DE2BBD7152B250AD5B62DD907 (void);
// 0x00000004 System.Nullable`1<UnityEngine.InputSystem.InputBinding> Input_master::get_bindingMask()
extern void Input_master_get_bindingMask_mA351518215DD2A46E0AC3B43C4F21E07FF4D6DAB (void);
// 0x00000005 System.Void Input_master::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
extern void Input_master_set_bindingMask_m47CCB6C56E9964B4F39B0C3D83AD6FBFBACCD5DA (void);
// 0x00000006 System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> Input_master::get_devices()
extern void Input_master_get_devices_mC144889E948D43D5E0BE4762C1897D94CACAEDE8 (void);
// 0x00000007 System.Void Input_master::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
extern void Input_master_set_devices_mCE7B8BD9B4FA936178C0A7A5012C01D6227BD8C9 (void);
// 0x00000008 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> Input_master::get_controlSchemes()
extern void Input_master_get_controlSchemes_mA89274C4467E476A0A54E36E5AE827B527453038 (void);
// 0x00000009 System.Boolean Input_master::Contains(UnityEngine.InputSystem.InputAction)
extern void Input_master_Contains_m3B11EA2D2A14801BD2ECD5CE5FDF548B82EFFCC3 (void);
// 0x0000000A System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> Input_master::GetEnumerator()
extern void Input_master_GetEnumerator_m576439B219C6489068DF54CD3A888D911AF81BD4 (void);
// 0x0000000B System.Collections.IEnumerator Input_master::System.Collections.IEnumerable.GetEnumerator()
extern void Input_master_System_Collections_IEnumerable_GetEnumerator_m5C5C020C36FECC05CBA84F4112857A516B18059C (void);
// 0x0000000C System.Void Input_master::Enable()
extern void Input_master_Enable_mAA5A4A5A3B5048D37FE1BE9BE3E654439F40771D (void);
// 0x0000000D System.Void Input_master::Disable()
extern void Input_master_Disable_m375CAC17441BA7AEFDE01B73AC869E9974E00A6A (void);
// 0x0000000E Input_master/PlayerActions Input_master::get_Player()
extern void Input_master_get_Player_mC7F6E1D47AB6D2D60FC4C5397098EB159F4660B9 (void);
// 0x0000000F Input_master/UIActions Input_master::get_UI()
extern void Input_master_get_UI_m7F8ED2828F6A385587139ED712AF2780A3E7D422 (void);
// 0x00000010 System.Void Input_master/PlayerActions::.ctor(Input_master)
extern void PlayerActions__ctor_m7481EA18F112C70293D3474392F06706C811E621 (void);
// 0x00000011 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_Movement()
extern void PlayerActions_get_Movement_mEA5BB53310B50C0F518A9F4F610423C3905E9207 (void);
// 0x00000012 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_Interaction()
extern void PlayerActions_get_Interaction_m77608CD4C8D9A2A8B508BD745FAFA1622C5835C7 (void);
// 0x00000013 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_Escape()
extern void PlayerActions_get_Escape_mB334771D35D03DCB58855F20B5A151BEB95681DD (void);
// 0x00000014 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_LeftMouse()
extern void PlayerActions_get_LeftMouse_m5E5CAEA8DAC2A5065B4890005B37F0A63B2F8A4D (void);
// 0x00000015 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_LeftMouseReleash()
extern void PlayerActions_get_LeftMouseReleash_m8B39D61E60E15BCD618823B459D8B35D0C729B29 (void);
// 0x00000016 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_RightMouse()
extern void PlayerActions_get_RightMouse_mC71D836809242B2B2931CDED4D47B659DB5CD223 (void);
// 0x00000017 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_MousePosition()
extern void PlayerActions_get_MousePosition_m12C40F11BB2A48AA6F4CBEAC2307BE06F6CADC26 (void);
// 0x00000018 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_X()
extern void PlayerActions_get_X_mC015291F0DD75D6C781276BADA2E2DE4325531BA (void);
// 0x00000019 UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_C()
extern void PlayerActions_get_C_mF777A74F1B446CDD7CE1FD7E151C89781B115492 (void);
// 0x0000001A UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_P()
extern void PlayerActions_get_P_m04B5F31DBC4699181C495E9BC290D5FBC292C0EA (void);
// 0x0000001B UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_O()
extern void PlayerActions_get_O_mB7AF304DB8C29291B5D5A622F1D5898A370972FF (void);
// 0x0000001C UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_ForwardDialogue()
extern void PlayerActions_get_ForwardDialogue_mE207E1B693F9DE4F991D8709CD8DD5D2B85BE6C2 (void);
// 0x0000001D UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_Tab()
extern void PlayerActions_get_Tab_m338B414903369841F681082F90CD34C17E3CA12B (void);
// 0x0000001E UnityEngine.InputSystem.InputAction Input_master/PlayerActions::get_Debugging()
extern void PlayerActions_get_Debugging_mE0B8A7A3F30DF25EFED5C27CD4F1088F0377456E (void);
// 0x0000001F UnityEngine.InputSystem.InputActionMap Input_master/PlayerActions::Get()
extern void PlayerActions_Get_m9E302E68280071685925C8B03873ED42656775A8 (void);
// 0x00000020 System.Void Input_master/PlayerActions::Enable()
extern void PlayerActions_Enable_m00A9A57EDAC1A58DFB03DC10C4E266AC921B552C (void);
// 0x00000021 System.Void Input_master/PlayerActions::Disable()
extern void PlayerActions_Disable_mB03301453FDA738D343E68B47B67493E74C6FEA9 (void);
// 0x00000022 System.Boolean Input_master/PlayerActions::get_enabled()
extern void PlayerActions_get_enabled_m0C8FBC0086D06E55C6C2FDCFBC7C53C1A06F15B7 (void);
// 0x00000023 UnityEngine.InputSystem.InputActionMap Input_master/PlayerActions::op_Implicit(Input_master/PlayerActions)
extern void PlayerActions_op_Implicit_m00773CF39CF596BDE1B46506EC4558388936D99B (void);
// 0x00000024 System.Void Input_master/PlayerActions::SetCallbacks(Input_master/IPlayerActions)
extern void PlayerActions_SetCallbacks_m0CAD45BB19CA2B72DB0BA6C127A5137CB5D458E7 (void);
// 0x00000025 System.Void Input_master/UIActions::.ctor(Input_master)
extern void UIActions__ctor_m944934E74521224F76E9F1FB5C83CB23EDC1D41B (void);
// 0x00000026 UnityEngine.InputSystem.InputAction Input_master/UIActions::get_Navigate()
extern void UIActions_get_Navigate_m6C827DD1D55866AAB36A4E59CC372409621AC711 (void);
// 0x00000027 UnityEngine.InputSystem.InputActionMap Input_master/UIActions::Get()
extern void UIActions_Get_m65A7C87B82A96CBAD0B2662044414AB697A0266A (void);
// 0x00000028 System.Void Input_master/UIActions::Enable()
extern void UIActions_Enable_m49364651EAEA42FB35D42CC6AE6E4305195295BA (void);
// 0x00000029 System.Void Input_master/UIActions::Disable()
extern void UIActions_Disable_m90FE4E19AC2051688F7FE7D3F593BBCDEB65E913 (void);
// 0x0000002A System.Boolean Input_master/UIActions::get_enabled()
extern void UIActions_get_enabled_m19088D7D7D446891B6DD16C542B70E24A65CD4E8 (void);
// 0x0000002B UnityEngine.InputSystem.InputActionMap Input_master/UIActions::op_Implicit(Input_master/UIActions)
extern void UIActions_op_Implicit_mAFB6947101E88F51845A740D582B517DC13E86D2 (void);
// 0x0000002C System.Void Input_master/UIActions::SetCallbacks(Input_master/IUIActions)
extern void UIActions_SetCallbacks_mF059380A033D1DCCBDEEF8FA931A92C7AFABEE2C (void);
// 0x0000002D System.Void Input_master/IPlayerActions::OnMovement(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000002E System.Void Input_master/IPlayerActions::OnInteraction(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000002F System.Void Input_master/IPlayerActions::OnEscape(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000030 System.Void Input_master/IPlayerActions::OnLeftMouse(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000031 System.Void Input_master/IPlayerActions::OnLeftMouseReleash(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000032 System.Void Input_master/IPlayerActions::OnRightMouse(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000033 System.Void Input_master/IPlayerActions::OnMousePosition(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000034 System.Void Input_master/IPlayerActions::OnX(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000035 System.Void Input_master/IPlayerActions::OnC(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000036 System.Void Input_master/IPlayerActions::OnP(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000037 System.Void Input_master/IPlayerActions::OnO(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000038 System.Void Input_master/IPlayerActions::OnForwardDialogue(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x00000039 System.Void Input_master/IPlayerActions::OnTab(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000003A System.Void Input_master/IPlayerActions::OnDebugging(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000003B System.Void Input_master/IUIActions::OnNavigate(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x0000003C PlayerInputController PlayerInputController::get_Instance()
extern void PlayerInputController_get_Instance_m1B5AA8013FFB4BA31511DB22E772E66D5738C01D (void);
// 0x0000003D System.Void PlayerInputController::Awake()
extern void PlayerInputController_Awake_m334C96DC775AE5059ED1D52D3EA44ABDE69A8425 (void);
// 0x0000003E System.Void PlayerInputController::OnEnable()
extern void PlayerInputController_OnEnable_m50D74F6E26D85540E0A7E2EF2E44C19E7A98F763 (void);
// 0x0000003F System.Void PlayerInputController::OnDisable()
extern void PlayerInputController_OnDisable_m70AD87038071D9611A9C80FC2A6D4D600E8D140A (void);
// 0x00000040 UnityEngine.Vector2 PlayerInputController::get_GetMovementValue()
extern void PlayerInputController_get_GetMovementValue_mEDEE3A0D2F095148251748F65A4BC79C4035B53F (void);
// 0x00000041 System.Boolean PlayerInputController::GetMovement()
extern void PlayerInputController_GetMovement_m153DFF309E5A13FB63ACE059FE575356639E2782 (void);
// 0x00000042 UnityEngine.Vector2 PlayerInputController::get_MousePosition()
extern void PlayerInputController_get_MousePosition_m497FF44822EC8B4CAD48123AD8492ABBA8E5DC0F (void);
// 0x00000043 System.Boolean PlayerInputController::LeftMouseClick()
extern void PlayerInputController_LeftMouseClick_mFD99D5C6E414C3F2A62D92F4E85A960870F78B09 (void);
// 0x00000044 System.Single PlayerInputController::get_LeftMouseValue()
extern void PlayerInputController_get_LeftMouseValue_m6D27F378E8D2CCEC15A703B73960861DAA6E9F28 (void);
// 0x00000045 System.Single PlayerInputController::get_LeftMouseReleashValue()
extern void PlayerInputController_get_LeftMouseReleashValue_mCA3DBAEDF421276E6AFD9611AC2C8D582846B273 (void);
// 0x00000046 System.Boolean PlayerInputController::Interact()
extern void PlayerInputController_Interact_m36E4EE7FF96BF81BAF51657427E2E2A91D83B064 (void);
// 0x00000047 System.Boolean PlayerInputController::SkipDialogue()
extern void PlayerInputController_SkipDialogue_mFAADE025BBF7457E39CE420D29FCF70F4C725B7C (void);
// 0x00000048 System.Boolean PlayerInputController::Tab()
extern void PlayerInputController_Tab_mCFD7D19A1C1C2A455D17E3588D72333C089CC4DB (void);
// 0x00000049 System.Boolean PlayerInputController::Escape()
extern void PlayerInputController_Escape_mC59CA988739B661ADC08977297BDAEE9A65014EA (void);
// 0x0000004A System.Boolean PlayerInputController::Debugging()
extern void PlayerInputController_Debugging_mAE6DBE0F9995592114EAF09BAE49AD5C2D72E83C (void);
// 0x0000004B System.Void PlayerInputController::.ctor()
extern void PlayerInputController__ctor_m4D6570072769159A1458FB4FB7CE42C3B82F4B81 (void);
// 0x0000004C System.Void DebuggingMenu::Start()
extern void DebuggingMenu_Start_mD454939A17A47E827ADC4D1EC50FFEBA9D6ED2FA (void);
// 0x0000004D System.Void DebuggingMenu::Update()
extern void DebuggingMenu_Update_m66B7B618510B314720C4B84251891C3C9C121008 (void);
// 0x0000004E System.Void DebuggingMenu::OpenCloseMenuTab()
extern void DebuggingMenu_OpenCloseMenuTab_m17BE4FA32A4E227AD0B15D87642098C6632DABE5 (void);
// 0x0000004F System.Void DebuggingMenu::CheckPlayerInformation()
extern void DebuggingMenu_CheckPlayerInformation_mA2B5DB4C7DF407CE00D472E7C8AF468B04C83A2F (void);
// 0x00000050 System.Void DebuggingMenu::UpdateText()
extern void DebuggingMenu_UpdateText_m5F172B900146036AF02FA22936569BD57D96AB8C (void);
// 0x00000051 System.Void DebuggingMenu::IncreaseStat(System.Int32)
extern void DebuggingMenu_IncreaseStat_mB1BA13A5B9995403442FB5B7F9EE26A5C769D1E0 (void);
// 0x00000052 System.Void DebuggingMenu::DecreaseStat(System.Int32)
extern void DebuggingMenu_DecreaseStat_m98937ADC8C8B359D9C330FBC961B90F0A1BC188C (void);
// 0x00000053 System.Void DebuggingMenu::OpenCloseSubTab(System.Int32)
extern void DebuggingMenu_OpenCloseSubTab_m6B092E0D4400844AD1EA438FBD8AC06E31011515 (void);
// 0x00000054 System.Void DebuggingMenu::CloseDebugMenu()
extern void DebuggingMenu_CloseDebugMenu_m8CDBDAB32274BD0FACABEE4B2CC0FDD3E3506D33 (void);
// 0x00000055 System.Void DebuggingMenu::DebugRefillHealth()
extern void DebuggingMenu_DebugRefillHealth_m7DEA276C28B1E09EF2B7C5CD51A2470907D8DE0A (void);
// 0x00000056 System.Void DebuggingMenu::DebugResetStat()
extern void DebuggingMenu_DebugResetStat_mE916784AB1693886E9A5C15E6054064D5AC62462 (void);
// 0x00000057 System.Void DebuggingMenu::DebugResetLevel()
extern void DebuggingMenu_DebugResetLevel_m84AB0A33CD01D14DB7B1C8DA42DA5CB632FF46B1 (void);
// 0x00000058 System.Void DebuggingMenu::DebugSetEnemyHPTo10()
extern void DebuggingMenu_DebugSetEnemyHPTo10_m3E4C54F84B452AA6CDC898268B86A154EB191BA7 (void);
// 0x00000059 System.Void DebuggingMenu::.ctor()
extern void DebuggingMenu__ctor_m57C26B55C2816EC65DC8F51EFEB44998E1C25D74 (void);
// 0x0000005A System.Void DebuggingMenu/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m1BF762E0CA54B1731560D5297FF8B4717C409AC0 (void);
// 0x0000005B System.Void DebuggingMenu/<>c__DisplayClass26_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass26_0_U3CStartU3Eb__0_mA6D6329F4AF68DDE8291B415799548F539C3EFCA (void);
// 0x0000005C System.Void DebuggingMenu/<>c__DisplayClass26_1::.ctor()
extern void U3CU3Ec__DisplayClass26_1__ctor_m7595C240A99180A8BF1D4EB9CF0974EB43DDA9D5 (void);
// 0x0000005D System.Void DebuggingMenu/<>c__DisplayClass26_1::<Start>b__1()
extern void U3CU3Ec__DisplayClass26_1_U3CStartU3Eb__1_mFDCE13723B36FD230536AA2B8DEC4886446DCDED (void);
// 0x0000005E System.Void DialogueChoice::OnClickUI()
extern void DialogueChoice_OnClickUI_m1F9676DB3A743A1780B047F1E26C683D5139A312 (void);
// 0x0000005F System.Void DialogueChoice::OnCancelledUI()
extern void DialogueChoice_OnCancelledUI_m3CDD4170785804A87502C4B89196BE4887B57024 (void);
// 0x00000060 System.Void DialogueChoice::OnHoverUI()
extern void DialogueChoice_OnHoverUI_m92C1C92E94F000D932746CD536DF652F736D97BC (void);
// 0x00000061 System.Void DialogueChoice::.ctor()
extern void DialogueChoice__ctor_m50CC4EE08F1A7F2C75AD0DDA41F00278F50760F4 (void);
// 0x00000062 UnityEngine.Sprite NarrationCharacter::get_characterSprite()
extern void NarrationCharacter_get_characterSprite_m8FD221AC65F3655C7F67B9BEA0264CF87617D9AB (void);
// 0x00000063 System.String NarrationCharacter::get_characterName()
extern void NarrationCharacter_get_characterName_mD994EBC69F8DDC1ABB13EFFA36F57025063E6917 (void);
// 0x00000064 System.Void NarrationCharacter::ChangePlayerNarration(UnityEngine.Sprite,System.String)
extern void NarrationCharacter_ChangePlayerNarration_mF41EFC540EB3B7B1FF89B98EBE98E3509AFA22C5 (void);
// 0x00000065 System.Void NarrationCharacter::.ctor()
extern void NarrationCharacter__ctor_m4F3F981D43B57D24B0CC88DE448BDD4AFA8B5891 (void);
// 0x00000066 NarrationLine NarrationLine::get_speaker()
extern void NarrationLine_get_speaker_m0430AE37C9502172CE5164D138D5DE76047F6C31 (void);
// 0x00000067 System.String NarrationLine::get_text()
extern void NarrationLine_get_text_m3A93671496008C6B99092A228F4F62AFDBCA6C71 (void);
// 0x00000068 System.Void NarrationLine::.ctor()
extern void NarrationLine__ctor_m98F31EB8B60C24586A2D7DFE6CB3CF528AB1ADFB (void);
// 0x00000069 System.String BaseNode::GetString()
extern void BaseNode_GetString_m64FC1E63D9CBE2DDD0829134F49D215623A528F4 (void);
// 0x0000006A System.String[] BaseNode::GetStringArray()
extern void BaseNode_GetStringArray_m85BE362542A58E253DF85F30C57B297873368F51 (void);
// 0x0000006B UnityEngine.Sprite BaseNode::GetSprite()
extern void BaseNode_GetSprite_mBF37E86C6ACCBFED8F329E3CF54A0751A384E9FE (void);
// 0x0000006C System.Int32 BaseNode::GetInt()
extern void BaseNode_GetInt_m6EDF5180A2BE10C7187F47EC8834B79963BB8B8A (void);
// 0x0000006D UnitProfile[] BaseNode::GetUnitProfile()
extern void BaseNode_GetUnitProfile_m158C0FB7003FA6C9ACB8A51ECAF4DA89094CF327 (void);
// 0x0000006E System.Void BaseNode::.ctor()
extern void BaseNode__ctor_mD6F065C5F0E972AFF7002AF9370B528C74CE96E2 (void);
// 0x0000006F System.String BattleCutIn::GetString()
extern void BattleCutIn_GetString_mFAEB2E3D1BA2A4B1686EF68AD3C3C5FA18F5EDFA (void);
// 0x00000070 System.Void BattleCutIn::.ctor()
extern void BattleCutIn__ctor_m92A85EE28296A2FC2D31C528514EC57E4677D53D (void);
// 0x00000071 System.String BattleNode::GetString()
extern void BattleNode_GetString_m968C6F2A68030D9F1DECEAEBB33907F72CBB0970 (void);
// 0x00000072 UnitProfile[] BattleNode::GetUnitProfile()
extern void BattleNode_GetUnitProfile_m4A230B5604191FCDC4491F9451B0A68F3A9A2056 (void);
// 0x00000073 System.Void BattleNode::.ctor()
extern void BattleNode__ctor_mA2ED470E48C006CB7F5DC5DB4763C4E3CB9DCF4E (void);
// 0x00000074 System.String ChoiceNode::GetString()
extern void ChoiceNode_GetString_m5E4478EE466BD0DF9CD307095D2FD2D9DF10180D (void);
// 0x00000075 System.String[] ChoiceNode::GetStringArray()
extern void ChoiceNode_GetStringArray_m15A3B5FE3F2D9ECC3BA99225B2024A93AF4B726F (void);
// 0x00000076 UnityEngine.Sprite ChoiceNode::GetSprite()
extern void ChoiceNode_GetSprite_m425F46293512CCC35FBC6AF66CF9B143197CC757 (void);
// 0x00000077 System.Int32 ChoiceNode::GetInt()
extern void ChoiceNode_GetInt_mB702F05D34FC998C842C915722CD5D9F06A100A0 (void);
// 0x00000078 System.Void ChoiceNode::.ctor()
extern void ChoiceNode__ctor_m1EFBC99899BDE6BD20E11BA41D871B05760EE57F (void);
// 0x00000079 System.Void DialogueGraph::.ctor()
extern void DialogueGraph__ctor_m8D2152FF1CB552124EFD6592BB66EE0585E7AA7F (void);
// 0x0000007A System.String DialogueNode::GetString()
extern void DialogueNode_GetString_mA40ECA92C7C76D086A9B776FC306A34A916F6483 (void);
// 0x0000007B UnityEngine.Sprite DialogueNode::GetSprite()
extern void DialogueNode_GetSprite_m8BB6A6309D09A33D67CC2D0F675E712581EAD533 (void);
// 0x0000007C System.Int32 DialogueNode::GetInt()
extern void DialogueNode_GetInt_m307E06D26944654B04B0F3596073AE2E4FE8D152 (void);
// 0x0000007D System.Void DialogueNode::.ctor()
extern void DialogueNode__ctor_m2DBC6A16614EE3B3D7A7A84AAFB09DFDD5EB63DA (void);
// 0x0000007E System.String EndNode::GetString()
extern void EndNode_GetString_mCD865F51677747DEDCF1A8E55C8AB9A97197554E (void);
// 0x0000007F System.Void EndNode::.ctor()
extern void EndNode__ctor_mC991B7FCF10E8DC0B3B59F1FA8BF5DA3A8D46885 (void);
// 0x00000080 NodeReader NodeReader::get_Instance()
extern void NodeReader_get_Instance_m79F918993194F446ADC18D96BEAB9F7BD14DA988 (void);
// 0x00000081 System.Void NodeReader::Awake()
extern void NodeReader_Awake_m160313717EC13BDE138A6C6C04270EAC121CCC90 (void);
// 0x00000082 System.Void NodeReader::Start()
extern void NodeReader_Start_m74A53C7E51328EE7119612A5BC233AAD3BE22417 (void);
// 0x00000083 System.Void NodeReader::Update()
extern void NodeReader_Update_m92329D9A3B8DAE21F78507C16FF6B599359A6F7B (void);
// 0x00000084 System.Void NodeReader::StartDialogue(DialogueGraph)
extern void NodeReader_StartDialogue_mFDC3B702A57B37F9E2C734F0633E4F3C9C23232F (void);
// 0x00000085 System.Void NodeReader::CustomStartDialogue(System.String)
extern void NodeReader_CustomStartDialogue_mE1DDE55BB452FB6FB5F59E071977CDD4932A556E (void);
// 0x00000086 System.Collections.IEnumerator NodeReader::ParserNode()
extern void NodeReader_ParserNode_m3EAB881B5ECB17F0B0BDD84C5CA93EEFF15B1E30 (void);
// 0x00000087 System.Collections.IEnumerator NodeReader::CustomDialogue()
extern void NodeReader_CustomDialogue_mDA2FFD006849C2851B90F8E19927B5E4E409D418 (void);
// 0x00000088 System.Collections.IEnumerator NodeReader::ParseChoice()
extern void NodeReader_ParseChoice_mE64E48CC0F78A2DE7D4257483A4347453929C73D (void);
// 0x00000089 System.Void NodeReader::NextNode(System.String)
extern void NodeReader_NextNode_m43243890BFBB821826CCB331C2E51F2D0C37D89B (void);
// 0x0000008A System.Void NodeReader::RunningDialogueButton()
extern void NodeReader_RunningDialogueButton_m7D87247FFFB4C18089B2E8236D702A12BAEBA269 (void);
// 0x0000008B System.Void NodeReader::RunningDialogue()
extern void NodeReader_RunningDialogue_m4C92C28AE90FEE3D502AD927CA208A29A6631DC5 (void);
// 0x0000008C System.Void NodeReader::SkipDialogueButton()
extern void NodeReader_SkipDialogueButton_m6F28D80E6334E9AA67EED744C8A00390976EE0C6 (void);
// 0x0000008D System.Void NodeReader::SkipDialogue()
extern void NodeReader_SkipDialogue_m497A40DAFFD871C7DE21300657A121B2F4056290 (void);
// 0x0000008E System.Collections.IEnumerator NodeReader::TypeSentence(TMPro.TextMeshProUGUI,System.String)
extern void NodeReader_TypeSentence_mE846AFE4DD1D8B4E9B5E084E82823DC410D03762 (void);
// 0x0000008F System.Void NodeReader::EndNode()
extern void NodeReader_EndNode_mFFBA7423C5E0F27395EAAB5301FB80B8B1980F6A (void);
// 0x00000090 System.Void NodeReader::BattleCutIn()
extern void NodeReader_BattleCutIn_m4D9BCA0392E3B7698EFEB7A3B47C662D06C7A217 (void);
// 0x00000091 System.Void NodeReader::BattleNode()
extern void NodeReader_BattleNode_m5E11E8AAF6DF063C0046E68DF944BFDB8170C576 (void);
// 0x00000092 System.Void NodeReader::ChooseChoice(System.Int32)
extern void NodeReader_ChooseChoice_m8008E29FE55B22969C469DD413BF3103FC428DF9 (void);
// 0x00000093 System.Boolean NodeReader::get_CheckDialogueIsPlaying()
extern void NodeReader_get_CheckDialogueIsPlaying_mCAEF25BB9ECF4DA1723C34A84BEBEECB8E98D0D1 (void);
// 0x00000094 System.Void NodeReader::StartTypingSound()
extern void NodeReader_StartTypingSound_mF8FAD347562AC7A7E4DE41388DDC525803D98866 (void);
// 0x00000095 System.Void NodeReader::.ctor()
extern void NodeReader__ctor_m4A93B76E13004A46F2923C82F037E13025CAB001 (void);
// 0x00000096 System.Boolean NodeReader::<ParseChoice>b__32_0()
extern void NodeReader_U3CParseChoiceU3Eb__32_0_mE9CB54451F924F79F3AC08623838E478D66D7A2C (void);
// 0x00000097 System.Boolean NodeReader::<ParseChoice>b__32_1()
extern void NodeReader_U3CParseChoiceU3Eb__32_1_m967C24CA1384A386651E4401E8B1E3C850BBF394 (void);
// 0x00000098 System.Void NodeReader/<ParserNode>d__30::.ctor(System.Int32)
extern void U3CParserNodeU3Ed__30__ctor_m382DAC51BDF1174D3212B4AF0B0D9DDDD122A329 (void);
// 0x00000099 System.Void NodeReader/<ParserNode>d__30::System.IDisposable.Dispose()
extern void U3CParserNodeU3Ed__30_System_IDisposable_Dispose_m14EB0FDDF590E4BDFF1FD8AC581D9AE22B70729C (void);
// 0x0000009A System.Boolean NodeReader/<ParserNode>d__30::MoveNext()
extern void U3CParserNodeU3Ed__30_MoveNext_mF51EE850ABD4015241300EDF6CFC08FC90DE6CC1 (void);
// 0x0000009B System.Object NodeReader/<ParserNode>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CParserNodeU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE06E05FE75F35AB048964B3D36B60A3B74A7F33 (void);
// 0x0000009C System.Void NodeReader/<ParserNode>d__30::System.Collections.IEnumerator.Reset()
extern void U3CParserNodeU3Ed__30_System_Collections_IEnumerator_Reset_m0CDB444124BB417931E15B820C3C9D9ABE915800 (void);
// 0x0000009D System.Object NodeReader/<ParserNode>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CParserNodeU3Ed__30_System_Collections_IEnumerator_get_Current_m5599AFCFFBBF462A44F42C2F1DAA399D2082E1AB (void);
// 0x0000009E System.Void NodeReader/<CustomDialogue>d__31::.ctor(System.Int32)
extern void U3CCustomDialogueU3Ed__31__ctor_m539D0D53D0C9852C2AA92851832BBA1F93FE5215 (void);
// 0x0000009F System.Void NodeReader/<CustomDialogue>d__31::System.IDisposable.Dispose()
extern void U3CCustomDialogueU3Ed__31_System_IDisposable_Dispose_m05AB132BB1C96C406CB4E6FB832AA9310CAF9159 (void);
// 0x000000A0 System.Boolean NodeReader/<CustomDialogue>d__31::MoveNext()
extern void U3CCustomDialogueU3Ed__31_MoveNext_m0FA30F52AE3CAB4A7D1E775F39C388780CC4A919 (void);
// 0x000000A1 System.Object NodeReader/<CustomDialogue>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCustomDialogueU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB652CBA767F5E7D4BF9661F5D60318DA0C3562DF (void);
// 0x000000A2 System.Void NodeReader/<CustomDialogue>d__31::System.Collections.IEnumerator.Reset()
extern void U3CCustomDialogueU3Ed__31_System_Collections_IEnumerator_Reset_m174F4229540442B55B3F7CCCDD051E832E908526 (void);
// 0x000000A3 System.Object NodeReader/<CustomDialogue>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CCustomDialogueU3Ed__31_System_Collections_IEnumerator_get_Current_m039BFABC311EAD1031D0A8D7D1E90260AE943B8C (void);
// 0x000000A4 System.Void NodeReader/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m7BA21C65BDC489FACCEE4EA5B9B7D4AE5A5BC63B (void);
// 0x000000A5 System.Void NodeReader/<>c__DisplayClass32_0::<ParseChoice>b__2()
extern void U3CU3Ec__DisplayClass32_0_U3CParseChoiceU3Eb__2_mF660B8B93FEDF189BF6CC164D6895CA6068151E3 (void);
// 0x000000A6 System.Void NodeReader/<ParseChoice>d__32::.ctor(System.Int32)
extern void U3CParseChoiceU3Ed__32__ctor_m8CD7150757546CD2F3182C33BBAE91519AAB9000 (void);
// 0x000000A7 System.Void NodeReader/<ParseChoice>d__32::System.IDisposable.Dispose()
extern void U3CParseChoiceU3Ed__32_System_IDisposable_Dispose_mC62B6A94C7B7FEAA59D9B2BF082E33A1E28A1293 (void);
// 0x000000A8 System.Boolean NodeReader/<ParseChoice>d__32::MoveNext()
extern void U3CParseChoiceU3Ed__32_MoveNext_m64C4B15D5CF4BA1BAD07F532BB3C423168B1F42B (void);
// 0x000000A9 System.Object NodeReader/<ParseChoice>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CParseChoiceU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F3C38F7EFC22E10D93A71F5136ABE6343D8E287 (void);
// 0x000000AA System.Void NodeReader/<ParseChoice>d__32::System.Collections.IEnumerator.Reset()
extern void U3CParseChoiceU3Ed__32_System_Collections_IEnumerator_Reset_m45250654797E7892C0D72ECDBDF79A12C230ABBF (void);
// 0x000000AB System.Object NodeReader/<ParseChoice>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CParseChoiceU3Ed__32_System_Collections_IEnumerator_get_Current_m34989581633799DDA06EE8408DA1FF9B977EED46 (void);
// 0x000000AC System.Void NodeReader/<TypeSentence>d__38::.ctor(System.Int32)
extern void U3CTypeSentenceU3Ed__38__ctor_mD2CE5896F92C199BFA6B203DC27462B78C8D2FBB (void);
// 0x000000AD System.Void NodeReader/<TypeSentence>d__38::System.IDisposable.Dispose()
extern void U3CTypeSentenceU3Ed__38_System_IDisposable_Dispose_m3B08D4B564EB130E4D1160752C31C799AEBC00FB (void);
// 0x000000AE System.Boolean NodeReader/<TypeSentence>d__38::MoveNext()
extern void U3CTypeSentenceU3Ed__38_MoveNext_m7761734731ABA6F19EEF515CBC3F0FFE960B7F9D (void);
// 0x000000AF System.Object NodeReader/<TypeSentence>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTypeSentenceU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C44140AB27A4C7204C12F1564FC47CCFDAC8C69 (void);
// 0x000000B0 System.Void NodeReader/<TypeSentence>d__38::System.Collections.IEnumerator.Reset()
extern void U3CTypeSentenceU3Ed__38_System_Collections_IEnumerator_Reset_mA7053DC759C764A5E1E71EF04AED17866CA1575B (void);
// 0x000000B1 System.Object NodeReader/<TypeSentence>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CTypeSentenceU3Ed__38_System_Collections_IEnumerator_get_Current_mE7624AC3A9AFB6B227EC4D69375762C83BB9FB64 (void);
// 0x000000B2 System.String StartNode::GetString()
extern void StartNode_GetString_m7FFF9CCFF277A74935E7D4D72CED41694E9557D0 (void);
// 0x000000B3 System.Void StartNode::.ctor()
extern void StartNode__ctor_mAD3D015ED7EA0249B4E522D686960203F6C6F2EA (void);
// 0x000000B4 GameManager GameManager::get_Instance()
extern void GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232 (void);
// 0x000000B5 System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x000000B6 System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x000000B7 System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x000000B8 System.Void NPC::Start()
extern void NPC_Start_m20C2D17950CB234A91905C91CCEADA0870B913B9 (void);
// 0x000000B9 System.Void NPC::Update()
extern void NPC_Update_m385369B628E634D1BB4300956637169CBAFDDEAE (void);
// 0x000000BA System.Void NPC::NPCState()
extern void NPC_NPCState_m16FD7EE3093100F535540A8D62DE0664FB09603E (void);
// 0x000000BB System.Void NPC::NPCNotificationBox()
extern void NPC_NPCNotificationBox_mEE0CE12E183C3233097308141C84936C509FFE20 (void);
// 0x000000BC System.Void NPC::Interaction()
extern void NPC_Interaction_mBE822D12AE7FA329CB3C794B96F6D97EE0D974D3 (void);
// 0x000000BD System.Void NPC::PlayDialogue(DialogueGraph)
extern void NPC_PlayDialogue_m10A92A7DBBC4B6AE9AA9AD99A2549CDAD91391B6 (void);
// 0x000000BE System.Void NPC::MouseHovering()
extern void NPC_MouseHovering_m8EEAD6599CBDCF66D9AA14CFBA343588F64F4CEA (void);
// 0x000000BF System.Void NPC::MouseNotHovering()
extern void NPC_MouseNotHovering_m3EB10E45423C5FA934E23800BC687B9EFBFAC7FD (void);
// 0x000000C0 System.Void NPC::ChangeProfile(UnitProfile)
extern void NPC_ChangeProfile_mD759886C03028BEFE7802EF35C12FB4967DF5DAD (void);
// 0x000000C1 System.Void NPC::.ctor()
extern void NPC__ctor_mC9AA4F3CBCBADCB1866559D68B31E231A879CACE (void);
// 0x000000C2 System.Void NPCTriggerCollider::Start()
extern void NPCTriggerCollider_Start_mC1DE92FF508FAAB938EB0E099635F237D7226294 (void);
// 0x000000C3 System.Void NPCTriggerCollider::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void NPCTriggerCollider_OnTriggerEnter2D_m7837A332B4E6F283D2BB5C70E0B78E2500A0E5FF (void);
// 0x000000C4 System.Void NPCTriggerCollider::OnTriggerExit2D(UnityEngine.Collider2D)
extern void NPCTriggerCollider_OnTriggerExit2D_m6727010F3047E3B29869368708B34C31B248B5A4 (void);
// 0x000000C5 System.Void NPCTriggerCollider::.ctor()
extern void NPCTriggerCollider__ctor_m4F1029058C03705CAE09423FD4524F46183FDFF8 (void);
// 0x000000C6 System.Void ObjectiveInfoSO::.ctor()
extern void ObjectiveInfoSO__ctor_m58026E32811CDA4D4DF6BF945C2B44D5345742E4 (void);
// 0x000000C7 System.Void ObjectiveManager::StartObjective(ObjectiveInfoSO)
extern void ObjectiveManager_StartObjective_m32FA1274D3D8175E652A28A67A05080BABC93329 (void);
// 0x000000C8 System.Void ObjectiveManager::.ctor()
extern void ObjectiveManager__ctor_mB2CC8A6A9649D74D24754F48287BC90A8B839F63 (void);
// 0x000000C9 System.Void PlayerObjective::AcceptingObjective()
extern void PlayerObjective_AcceptingObjective_mA91A40E21D26A39B77DCBF346164B18FDC453BCD (void);
// 0x000000CA System.Void PlayerObjective::.ctor()
extern void PlayerObjective__ctor_m07D6C86D9A6E753D4DB5D5EC3CCAD4532742976E (void);
// 0x000000CB System.Void RuntimeObjective::.ctor(ObjectiveInfoSO)
extern void RuntimeObjective__ctor_m394F248519C3B8ED3B5DBE076D431BED88F11E4D (void);
// 0x000000CC System.Void RuntimeObjectiveTracker::Initialize()
// 0x000000CD System.Void RuntimeObjectiveTracker::.ctor()
extern void RuntimeObjectiveTracker__ctor_mE96643297DC6452D03C85C39D339C733B0B10091 (void);
// 0x000000CE PlayerBrain PlayerBrain::get_Instance()
extern void PlayerBrain_get_Instance_m9533B6327CD58859793AD9B47C22C51F37B9832F (void);
// 0x000000CF System.Void PlayerBrain::Awake()
extern void PlayerBrain_Awake_m6408C44E8C560FDF2213D3821DE4157BF9225882 (void);
// 0x000000D0 System.Void PlayerBrain::.ctor()
extern void PlayerBrain__ctor_m8CAD32EA09F6ED25BF02F112D99F604DA309AA23 (void);
// 0x000000D1 PlayerController PlayerController::get_Instance()
extern void PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440 (void);
// 0x000000D2 System.Void PlayerController::Awake()
extern void PlayerController_Awake_m23059564DCFDD324EA9DB966473251896647CD23 (void);
// 0x000000D3 System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x000000D4 System.Void PlayerController::Movement()
extern void PlayerController_Movement_m417CCA59FA36C6719013DF91B010673B5F7277F1 (void);
// 0x000000D5 System.Boolean PlayerController::CanMove(UnityEngine.Vector2)
extern void PlayerController_CanMove_m18B797FBC598549FE78766BD4912DC152A595241 (void);
// 0x000000D6 System.Void PlayerController::PlayOneShot(UnityEngine.AudioClip)
extern void PlayerController_PlayOneShot_m5C3AF5FEC890F309670510ED154A065A29480820 (void);
// 0x000000D7 System.Void PlayerController::PlayWalkingSound()
extern void PlayerController_PlayWalkingSound_mBAEC5AD60DAED60971DAD54F795DF458D8BBEB77 (void);
// 0x000000D8 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x000000D9 System.Void PlayerModel::Start()
extern void PlayerModel_Start_m12F050E122E9E36024B7A6DF0D5B7E7FB82BD5FB (void);
// 0x000000DA System.Void PlayerModel::.ctor()
extern void PlayerModel__ctor_m251F2D079FCA1F01DE72142D041B9BE652411BD6 (void);
// 0x000000DB PlayerMouseDetection PlayerMouseDetection::get_Instance()
extern void PlayerMouseDetection_get_Instance_mDE07DF7A7DBBBAAB2AAFAABE5018583617B4AACB (void);
// 0x000000DC System.Void PlayerMouseDetection::Awake()
extern void PlayerMouseDetection_Awake_mA5B035F2C10AAD3AC3A1C3F576FAB78566FE04F0 (void);
// 0x000000DD System.Void PlayerMouseDetection::Update()
extern void PlayerMouseDetection_Update_m9A5A2213DFBEBC61503A3984D551A3F4CFD7740F (void);
// 0x000000DE System.Void PlayerMouseDetection::checkifPressOnObjectBackUp()
extern void PlayerMouseDetection_checkifPressOnObjectBackUp_mD333437F81231A3337157CC52C52B5B46AB3CC40 (void);
// 0x000000DF System.Boolean PlayerMouseDetection::CheckMouseOnObject(UnityEngine.GameObject)
extern void PlayerMouseDetection_CheckMouseOnObject_m0DDB07EE04416C2DCF706DAAB4518CB45F1CE1D2 (void);
// 0x000000E0 System.Void PlayerMouseDetection::.ctor()
extern void PlayerMouseDetection__ctor_m345244A82751DB65E3D1ED67DB23AB4766750043 (void);
// 0x000000E1 T Singleton`1::get_Instance()
// 0x000000E2 System.Void Singleton`1::Awake()
// 0x000000E3 System.Void Singleton`1::OnDestroy()
// 0x000000E4 System.Void Singleton`1::.ctor()
// 0x000000E5 T SingletonClass`1::get_Instance()
// 0x000000E6 System.Void SingletonClass`1::Awake()
// 0x000000E7 System.Void SingletonClass`1::OnDestroy()
// 0x000000E8 System.Void SingletonClass`1::.ctor()
// 0x000000E9 T SingletonStaticClass`1::get_Instance()
// 0x000000EA System.Void SingletonStaticClass`1::Awake()
// 0x000000EB System.Void SingletonStaticClass`1::.ctor()
// 0x000000EC System.String AttackProfile::get_attackName()
extern void AttackProfile_get_attackName_mC97A9FE870418F6C3362D62800732D804AF3EA6C (void);
// 0x000000ED System.String AttackProfile::get_attackDescription()
extern void AttackProfile_get_attackDescription_m93DDDD8DF15875C93EA592647606DED08BA8F2D4 (void);
// 0x000000EE UnityEngine.Sprite AttackProfile::get_attackIcon()
extern void AttackProfile_get_attackIcon_mFE491EBE2F25AB0CBB6AAC783F0613F0E29AEF03 (void);
// 0x000000EF UnityEngine.AnimationClip AttackProfile::get_attackAnim()
extern void AttackProfile_get_attackAnim_mD579E33A1B52FFDBB8578F6AD437DB2BA72E3F70 (void);
// 0x000000F0 System.Single AttackProfile::get_staminaCost()
extern void AttackProfile_get_staminaCost_m91A47A6E567713F29BFA474EF43A2137F1D670D3 (void);
// 0x000000F1 System.Single AttackProfile::get_ideaCost()
extern void AttackProfile_get_ideaCost_mFB9C30CD4F58F45A1144A9625362BA28616FE038 (void);
// 0x000000F2 System.Single AttackProfile::get_logicCost()
extern void AttackProfile_get_logicCost_m6D196B2FE9E3EAE42A4CC8CE6B483B3DEE4A6447 (void);
// 0x000000F3 System.Single AttackProfile::get_damage()
extern void AttackProfile_get_damage_m1E71A63DEA10998B8447BD2FB1784FDFAF48F5A1 (void);
// 0x000000F4 System.Int32 AttackProfile::get_critRate()
extern void AttackProfile_get_critRate_mC1439D190AA3B686B64F7B878AD2509F15DE2310 (void);
// 0x000000F5 System.Boolean AttackProfile::get_heal()
extern void AttackProfile_get_heal_mBB7EBA1FCD6D52A9F125AFBBF8AA0F5F2D15FBEC (void);
// 0x000000F6 System.Boolean AttackProfile::get_isDesignSkill()
extern void AttackProfile_get_isDesignSkill_m3889BC71D3542F76BBBC62D565D4A47FC3BCB7A0 (void);
// 0x000000F7 System.Boolean AttackProfile::get_isCodeSkill()
extern void AttackProfile_get_isCodeSkill_mF5E38048ED6FAE858EF31620162E2DC7A1FB2470 (void);
// 0x000000F8 System.Int32 AttackProfile::get_healRate()
extern void AttackProfile_get_healRate_mAD9546C4E074D779488EA00EE60A1DBFF8FEE151 (void);
// 0x000000F9 System.Int32 AttackProfile::get_healAmount()
extern void AttackProfile_get_healAmount_mC089C99644BE15D2EC0101D9FDCCF3613AF29D8F (void);
// 0x000000FA System.Void AttackProfile::.ctor()
extern void AttackProfile__ctor_m7A187871CFA9B24459BB51C0D8B126B41466981A (void);
// 0x000000FB System.String UnitProfile::get_unitName()
extern void UnitProfile_get_unitName_m53905CB500D1E07809ADCAB0271090DFAD182CAE (void);
// 0x000000FC UnityEngine.Sprite UnitProfile::get_unitSprite()
extern void UnitProfile_get_unitSprite_mDAC5B1B56F0EC079C7AB0434EA89C60C21D79BB0 (void);
// 0x000000FD CharacterStat UnitProfile::get_stat()
extern void UnitProfile_get_stat_mCC53416A80E5B4D2498BD1F25846900793BE22A2 (void);
// 0x000000FE System.Collections.Generic.List`1<AttackProfile> UnitProfile::get_attackProfile()
extern void UnitProfile_get_attackProfile_mF74B7109FAEA6809DA5B7D5921F7C3FCC6704D68 (void);
// 0x000000FF System.Collections.Generic.List`1<AttackProfile> UnitProfile::get_skillProfile()
extern void UnitProfile_get_skillProfile_m81873147F6B3EA0A6ABBF5FABA56D2F794220B8D (void);
// 0x00000100 DialogueGraph UnitProfile::get_startingDialogue()
extern void UnitProfile_get_startingDialogue_mD3D96018A62DBC5FB8F2D7AE4859D9AD1F2B9030 (void);
// 0x00000101 DialogueGraph UnitProfile::get_introBattle()
extern void UnitProfile_get_introBattle_mA5E48E94582D47BAA684F2AC83F336AF408A88F2 (void);
// 0x00000102 Reward UnitProfile::get_reward()
extern void UnitProfile_get_reward_m4D6C9B4C79E7E49C51F676498EAD8B442A63DD7D (void);
// 0x00000103 System.Void UnitProfile::ResetDefaultState()
extern void UnitProfile_ResetDefaultState_mAE94449F2BE5746CD6C94528C1BF7D083A4F3B12 (void);
// 0x00000104 System.Void UnitProfile::ResettingFightingStat()
extern void UnitProfile_ResettingFightingStat_m8ACAC82FA2F0D84722A15EF6F9642A4D72306321 (void);
// 0x00000105 System.Void UnitProfile::DefaultStartingLevel()
extern void UnitProfile_DefaultStartingLevel_m4BFFDA1B009B2A2AFC96C61A77994A98F7A11FE8 (void);
// 0x00000106 System.Void UnitProfile::RestoreCharacter()
extern void UnitProfile_RestoreCharacter_mB341D0B068F9C89065FF79BF1FE6378FE0807382 (void);
// 0x00000107 System.Void UnitProfile::SetNewProfile(UnityEngine.Sprite,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void UnitProfile_SetNewProfile_mB35E3BA67D46C8BD16A8DD2EE7E1C0D2BC5F9B46 (void);
// 0x00000108 System.Void UnitProfile::ChangeName(System.String)
extern void UnitProfile_ChangeName_m1DFE7879F051DD211B33B9DF8AB528390063975B (void);
// 0x00000109 System.Void UnitProfile::AddNewSkill(AttackProfile)
extern void UnitProfile_AddNewSkill_mAF952E70E7724E8655C4C74A9DCF8580844E5FC7 (void);
// 0x0000010A System.Void UnitProfile::ResetSkill(System.Collections.Generic.List`1<AttackProfile>)
extern void UnitProfile_ResetSkill_mE32E06E4948BB6C8F9BEC872CB94467350B504C0 (void);
// 0x0000010B System.Void UnitProfile::.ctor()
extern void UnitProfile__ctor_mDD0903476ACFC4AB1BBB6E48DF1BC47184A7E016 (void);
// 0x0000010C System.Void CharacterStat::GainEXP(System.Int32)
extern void CharacterStat_GainEXP_m3A38088CA47955D129E82561E2DA3F9416E1384C (void);
// 0x0000010D System.Void CharacterStat::LevelingUp()
extern void CharacterStat_LevelingUp_mFBFE6E0D6D81FBFCDFC9C2475C54C1B95CC78573 (void);
// 0x0000010E System.Void CharacterStat::LevelingStatCheck()
extern void CharacterStat_LevelingStatCheck_m743BF6C8D2C6E3EEE78F98B12C5FF5C44618122E (void);
// 0x0000010F System.Void CharacterStat::TakeDamage(System.Single,System.Single)
extern void CharacterStat_TakeDamage_m63406F2029F75095CEBFFC90CFA002C4F3081F65 (void);
// 0x00000110 System.Void CharacterStat::TakeSpecialDamage(System.Single,System.Single,System.Single,System.Boolean)
extern void CharacterStat_TakeSpecialDamage_m054B0EE229E9B21CD600EA2E6BD552F24A0AA689 (void);
// 0x00000111 System.Int32 CharacterStat::get_GetCurrentMaxHP()
extern void CharacterStat_get_GetCurrentMaxHP_mCC925038DDDBE548B20160AD52DC8AE96429CF53 (void);
// 0x00000112 System.Int32 CharacterStat::get_GetCurrentIdea()
extern void CharacterStat_get_GetCurrentIdea_mE67A36836A194AD697F022708E5ED3A7EBABC238 (void);
// 0x00000113 System.Int32 CharacterStat::get_GetCurrentLogic()
extern void CharacterStat_get_GetCurrentLogic_mDCB673ABBC41FD04B8487174633FC91D58CEF971 (void);
// 0x00000114 System.Int32 CharacterStat::get_GetCurrentAttack()
extern void CharacterStat_get_GetCurrentAttack_m612A622ACC3FC82C46C082CC27EB903C941D0A8E (void);
// 0x00000115 System.Int32 CharacterStat::get_GetCurrentDefence()
extern void CharacterStat_get_GetCurrentDefence_mBE06311CE3FF99F492251E61DC06CD13EFB9DE6C (void);
// 0x00000116 System.Void CharacterStat::.ctor()
extern void CharacterStat__ctor_mE8453187755B24D6B71C4241AB73607BD8C0E8ED (void);
// 0x00000117 System.Int32 Reward::get_rewardEXP()
extern void Reward_get_rewardEXP_mA003AABC3AEFB472A595FCF17148F2D0F00F6ED8 (void);
// 0x00000118 System.Void Reward::.ctor()
extern void Reward__ctor_mC71832CFC8A6C052EC4051BD5288B600B9587DF8 (void);
// 0x00000119 System.Int32 LevelingStat::get_increaseBaseHP()
extern void LevelingStat_get_increaseBaseHP_m82B3B4996A1F0103CB82BA4BFC97DA790AFE0AB2 (void);
// 0x0000011A System.Int32 LevelingStat::get_increaseBaseIdea()
extern void LevelingStat_get_increaseBaseIdea_m65C073FE236242E61623F892F9369083B85EE4EA (void);
// 0x0000011B System.Int32 LevelingStat::get_increaseBaseLogic()
extern void LevelingStat_get_increaseBaseLogic_mE5B57D361629083B4C264AC7D955667BC37A4F6B (void);
// 0x0000011C System.Int32 LevelingStat::get_increaseBaseAtk()
extern void LevelingStat_get_increaseBaseAtk_mB8C3279B530EBDA97B3D7A6CE870D1780B2ED804 (void);
// 0x0000011D System.Int32 LevelingStat::get_increaseBaseDef()
extern void LevelingStat_get_increaseBaseDef_m3BFE123B6634480C6A5F9F2E555990A241EF7209 (void);
// 0x0000011E System.Int32 LevelingStat::get_skillPoint()
extern void LevelingStat_get_skillPoint_mE3A2E3A63211FDB587708907D62EFDDDE2BEFB91 (void);
// 0x0000011F System.Void LevelingStat::.ctor()
extern void LevelingStat__ctor_mAB24A06E211DDE4F98AAB79CE811EFFDDC71C843 (void);
// 0x00000120 UnityEngine.AudioSource SoundManager::get_bgmSource()
extern void SoundManager_get_bgmSource_m513EAF47372F98B602C885AD31D38CCDC73D1757 (void);
// 0x00000121 UnityEngine.AudioClip SoundManager::get_onClick()
extern void SoundManager_get_onClick_m18B7A6DDF53EE9F37F5A62EBFAA82891F0685140 (void);
// 0x00000122 UnityEngine.AudioClip SoundManager::get_onCancelled()
extern void SoundManager_get_onCancelled_m857F1443D4263FCB8E6030D1B9091D6FE99D83FA (void);
// 0x00000123 UnityEngine.AudioClip SoundManager::get_onHover()
extern void SoundManager_get_onHover_m51D38D885188E4EE9D266ECF340A440F6F19D338 (void);
// 0x00000124 System.Void SoundManager::Update()
extern void SoundManager_Update_mDD188B65FF1E9B1DF1B8345A6290D149E70E657C (void);
// 0x00000125 System.Void SoundManager::OnSceneLoaded()
extern void SoundManager_OnSceneLoaded_m77B3E475EE4B25348C20D0B125F9FF6690057132 (void);
// 0x00000126 System.Void SoundManager::CheckIfPlaying()
extern void SoundManager_CheckIfPlaying_m74F7C53D4283A6061B3A3516308D994AF281DC87 (void);
// 0x00000127 System.Void SoundManager::OnClickUI()
extern void SoundManager_OnClickUI_m4AAC7AE59343DA3C3B8D08F0230159F3317F5DD3 (void);
// 0x00000128 System.Void SoundManager::OnCancelledUI()
extern void SoundManager_OnCancelledUI_m314B21D5255A36BCFE8E8E7561FECD515DFFA948 (void);
// 0x00000129 System.Void SoundManager::OnHoverUI()
extern void SoundManager_OnHoverUI_mF2796CAFCC262451D8BF99918CBBF013B0AABA21 (void);
// 0x0000012A System.Void SoundManager::PlayOneShot(UnityEngine.AudioClip)
extern void SoundManager_PlayOneShot_m7117DF0B0DB6E24B88955F905D8039516C647A99 (void);
// 0x0000012B System.Void SoundManager::PlayWorldBGM(System.Int32)
extern void SoundManager_PlayWorldBGM_m177F5F008721ABA09ECD4823B8D221661432C4C8 (void);
// 0x0000012C System.Void SoundManager::PauseWorldBGM()
extern void SoundManager_PauseWorldBGM_m1C56B7527DBC6C49FE5F0C593B73F54DB0DE4100 (void);
// 0x0000012D System.Void SoundManager::ResumeWorldBGM()
extern void SoundManager_ResumeWorldBGM_mDDFF7A9C1A853E35FB8E2A8B1EFC94E0FAC5643F (void);
// 0x0000012E System.Void SoundManager::ClearWorldBGM()
extern void SoundManager_ClearWorldBGM_mECB2BE0484A089CA62C7A21AA2D19839AAD1023D (void);
// 0x0000012F System.Void SoundManager::PlayBattleBGM(System.Int32)
extern void SoundManager_PlayBattleBGM_m6E6A443F6B026CFB62F133FD3F0D462601787507 (void);
// 0x00000130 System.Void SoundManager::PlayRandomBattleFightSFX()
extern void SoundManager_PlayRandomBattleFightSFX_m9866DC15D5B51A73F072E193A6F2671F3C4E9CCF (void);
// 0x00000131 System.Void SoundManager::PlayRandomHealingSFX()
extern void SoundManager_PlayRandomHealingSFX_m32886A48C052825CAA816F6E65159042D06B3C98 (void);
// 0x00000132 System.Void SoundManager::FadeToNewMusic()
extern void SoundManager_FadeToNewMusic_m92AC93DD3C3A9E8A3D66418DF6CFB092FE9C3A9C (void);
// 0x00000133 System.Void SoundManager::TriggerFade(UnityEngine.AudioClip)
extern void SoundManager_TriggerFade_mC01BBBDBA38E40AAE19601091DC596C3FE5D5D86 (void);
// 0x00000134 System.Void SoundManager::.ctor()
extern void SoundManager__ctor_m30D18BC25871BD3FF7FB195A1CAE50A2EE48FCAE (void);
// 0x00000135 System.Void ChangeProfileEvent::Event()
extern void ChangeProfileEvent_Event_m957556E98776D0D09702CE223352E0C89007709B (void);
// 0x00000136 System.Void ChangeProfileEvent::.ctor()
extern void ChangeProfileEvent__ctor_m982CE719834A13D88299A3147A63D76FA9BC428E (void);
// 0x00000137 System.Void EventClass::Start()
extern void EventClass_Start_mB77D35247F4FE51053EA69569D0721B4E890BE2F (void);
// 0x00000138 System.Void EventClass::Update()
extern void EventClass_Update_m7D43536DE39D104396A1822DCB829832334449D7 (void);
// 0x00000139 System.Void EventClass::Event()
extern void EventClass_Event_m871008C3DF478FC32B786A7C8A4179EF8E3C62B1 (void);
// 0x0000013A System.Void EventClass::.ctor()
extern void EventClass__ctor_mBABE26471514E06A3C30BC76565EF0098844B04C (void);
// 0x0000013B System.Void EventTriggerClass::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void EventTriggerClass_OnTriggerEnter2D_m31036183084267307731C3989654E5FDC70EBFA5 (void);
// 0x0000013C System.Void EventTriggerClass::Event()
extern void EventTriggerClass_Event_m92662E5A39701B3BAA25B555ABFD8A1889A99540 (void);
// 0x0000013D System.Void EventTriggerClass::.ctor()
extern void EventTriggerClass__ctor_m065F74D52CA7C1A8F60822B76656C4F43BB786D2 (void);
// 0x0000013E System.Void GettingNewSkillEvent::Start()
extern void GettingNewSkillEvent_Start_m597303A6B8327A9577F7E1C8225AB1D1A3751A15 (void);
// 0x0000013F System.Void GettingNewSkillEvent::Event()
extern void GettingNewSkillEvent_Event_m97B419231B963A6BC453D976B66479236DD36B98 (void);
// 0x00000140 System.Void GettingNewSkillEvent::.ctor()
extern void GettingNewSkillEvent__ctor_m1EA90F4305DABEBB835F1DF73F85D452CF5A0C7C (void);
// 0x00000141 System.Void HealBox::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void HealBox_OnTriggerEnter2D_m9240FD1CDEE1662B8E2A1E090F51FE59EEC216C3 (void);
// 0x00000142 System.Void HealBox::.ctor()
extern void HealBox__ctor_mE820B1FEE45EA43430D083CA17F3D9A2B115AF5E (void);
// 0x00000143 System.Void MoveEvent::Event()
extern void MoveEvent_Event_m797DA0E9DBF57058D2C392AA6103E797F2EB6BC1 (void);
// 0x00000144 System.Void MoveEvent::.ctor()
extern void MoveEvent__ctor_mDBAD24BBD72CD75398F7D3C023E2FB238007C8E1 (void);
// 0x00000145 System.Void TriggerDialogue::Event()
extern void TriggerDialogue_Event_m500342BB968BA39A9C63277293B7C80284AC034F (void);
// 0x00000146 System.Void TriggerDialogue::.ctor()
extern void TriggerDialogue__ctor_m3BDA5E5F71BC525FFF15F7063A404CBAD50D8B4F (void);
// 0x00000147 System.Void TriggerMusic::Update()
extern void TriggerMusic_Update_m3D81E4170773A1DC48AAEFA5EF839FDC313FF565 (void);
// 0x00000148 System.Void TriggerMusic::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void TriggerMusic_OnTriggerEnter2D_mBBDB7F7767A26B8DC917670376CDAC4EBF1A665C (void);
// 0x00000149 System.Void TriggerMusic::Event()
extern void TriggerMusic_Event_m80AE3DB2CD83E22A176D6510F8DADF2CFEFBB6EC (void);
// 0x0000014A System.Void TriggerMusic::.ctor()
extern void TriggerMusic__ctor_m8080BD1615302632F3FD85AFFE20A9AFA99F695B (void);
// 0x0000014B System.Void ActionButton::ButtonSetup(AttackProfile)
extern void ActionButton_ButtonSetup_mE94F1B38707F2E7F0EF62D544BB3045D487A3208 (void);
// 0x0000014C System.Void ActionButton::ChangeAttackDescription()
extern void ActionButton_ChangeAttackDescription_m76426CC654AB161E2D93407B9D56BB81D7611409 (void);
// 0x0000014D System.Void ActionButton::ResetAttackDescription()
extern void ActionButton_ResetAttackDescription_m73FC3C03B99C025FF50E090F57589554390F98FB (void);
// 0x0000014E System.Void ActionButton::OnClickUI()
extern void ActionButton_OnClickUI_m9894C8EF1E2D4911A070C8EBCE7734A7F537695D (void);
// 0x0000014F System.Void ActionButton::OnCancelledUI()
extern void ActionButton_OnCancelledUI_mD61186489A05491FB4AF7A664B0A9E7B080C963F (void);
// 0x00000150 System.Void ActionButton::OnHoverUI()
extern void ActionButton_OnHoverUI_m4FCC381E3734596D42A64498FA2BA413FC71B771 (void);
// 0x00000151 System.Void ActionButton::.ctor()
extern void ActionButton__ctor_m8919EB676AE8997E975D2B1280671B00E1E90FF9 (void);
// 0x00000152 System.Void ActionButton::<ButtonSetup>b__4_0()
extern void ActionButton_U3CButtonSetupU3Eb__4_0_m08761867745A1895E5FC24E48212246605E32BA7 (void);
// 0x00000153 System.Void ActionButton::<ButtonSetup>b__4_1()
extern void ActionButton_U3CButtonSetupU3Eb__4_1_m5FF477DF8FAFE5AEC5C37EAF80E03DF9C1E1F497 (void);
// 0x00000154 System.Void ActionButton::<ButtonSetup>b__4_2()
extern void ActionButton_U3CButtonSetupU3Eb__4_2_m95AF25B868B387888CAE76BAA0C58A74D909B80A (void);
// 0x00000155 System.Void ActionButton::<ButtonSetup>b__4_3()
extern void ActionButton_U3CButtonSetupU3Eb__4_3_mD693DC64E13A8671086D2579FDE4F3CAA841305B (void);
// 0x00000156 BattleDialogue BattleDialogue::get_Instance()
extern void BattleDialogue_get_Instance_mD0C203A1C23ADF93551FE18C2669FC6A6F281A01 (void);
// 0x00000157 System.Void BattleDialogue::Awake()
extern void BattleDialogue_Awake_mB59F81F24B8D7016C035EC582AD1825EF4B915AA (void);
// 0x00000158 System.Void BattleDialogue::Update()
extern void BattleDialogue_Update_mB3E5A3299787A2FBC10682AD8D3CA233FE66F462 (void);
// 0x00000159 System.Void BattleDialogue::StartBattleDialogue()
extern void BattleDialogue_StartBattleDialogue_m0C9CB81C08435E0CF5D481A0DE771B8584539AEF (void);
// 0x0000015A System.Void BattleDialogue::ChangeSentence(UnitProfile,System.String)
extern void BattleDialogue_ChangeSentence_m7A50861150E21D1E918DB70EFDD0B69FBBF54D7B (void);
// 0x0000015B System.Collections.IEnumerator BattleDialogue::TypeSentence(TMPro.TextMeshProUGUI,System.String)
extern void BattleDialogue_TypeSentence_m03DBCF9A9051983E515A2A9D99379FFA2B616BE0 (void);
// 0x0000015C System.Void BattleDialogue::SkipDialogueButton()
extern void BattleDialogue_SkipDialogueButton_mB14C438B3C4CA11DA23CD0DD7B48A53D41243009 (void);
// 0x0000015D System.Void BattleDialogue::SkipDialogue()
extern void BattleDialogue_SkipDialogue_m6ABFE2A69F9197D674F77913BB87219C11AD494B (void);
// 0x0000015E System.Void BattleDialogue::EndBattleDialogue()
extern void BattleDialogue_EndBattleDialogue_mD2DBA7C09E498F0DE6DDE30373B455A85D27F5BF (void);
// 0x0000015F System.Void BattleDialogue::.ctor()
extern void BattleDialogue__ctor_m2613E45EB3EAB65C795A472302EA508EFE44CC6F (void);
// 0x00000160 System.Boolean BattleDialogue::<TypeSentence>b__15_0()
extern void BattleDialogue_U3CTypeSentenceU3Eb__15_0_m458E168B5080C9C7915C6C32DE32D5F131C05942 (void);
// 0x00000161 System.Void BattleDialogue/<>c::.cctor()
extern void U3CU3Ec__cctor_mDAD38F28B51926BD0142F0852E560473208BE6EA (void);
// 0x00000162 System.Void BattleDialogue/<>c::.ctor()
extern void U3CU3Ec__ctor_mF26FE1D29F9CD75FC3C5F1D40F8E6E1C6D490BA1 (void);
// 0x00000163 System.Boolean BattleDialogue/<>c::<TypeSentence>b__15_1()
extern void U3CU3Ec_U3CTypeSentenceU3Eb__15_1_m669A9B245BC4C5673981C06453718715AB3028CF (void);
// 0x00000164 System.Void BattleDialogue/<TypeSentence>d__15::.ctor(System.Int32)
extern void U3CTypeSentenceU3Ed__15__ctor_m714D8743D196B43AECF81689E5212CEE2D292FA2 (void);
// 0x00000165 System.Void BattleDialogue/<TypeSentence>d__15::System.IDisposable.Dispose()
extern void U3CTypeSentenceU3Ed__15_System_IDisposable_Dispose_m9FAED1ED91495E5D1C54C55022788CE9C1880A6F (void);
// 0x00000166 System.Boolean BattleDialogue/<TypeSentence>d__15::MoveNext()
extern void U3CTypeSentenceU3Ed__15_MoveNext_m6F4090DC69FF856E2D8FC93959456EBABD9D62FE (void);
// 0x00000167 System.Object BattleDialogue/<TypeSentence>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTypeSentenceU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BD268DFD0A34C14F5343375B8F74AE693EB2495 (void);
// 0x00000168 System.Void BattleDialogue/<TypeSentence>d__15::System.Collections.IEnumerator.Reset()
extern void U3CTypeSentenceU3Ed__15_System_Collections_IEnumerator_Reset_mD678F10373F8B0D8D78BE91742776FF56EC4654F (void);
// 0x00000169 System.Object BattleDialogue/<TypeSentence>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CTypeSentenceU3Ed__15_System_Collections_IEnumerator_get_Current_m270C4F3E2E87C7235ED7F7457E25DEA8695791B5 (void);
// 0x0000016A System.Void TurnBasePlayer::Update()
extern void TurnBasePlayer_Update_m407E0DF6C190A5809F7516BE8F7C91814B3E1557 (void);
// 0x0000016B System.Void TurnBasePlayer::SetUnitProfile(UnitProfile)
extern void TurnBasePlayer_SetUnitProfile_m2394130706ACB14F43C7D377CE61DCE4990B02D7 (void);
// 0x0000016C System.Void TurnBasePlayer::SetUnit()
extern void TurnBasePlayer_SetUnit_m8E9349191C58C0E2E106B921786CC3646A91B4DB (void);
// 0x0000016D System.Void TurnBasePlayer::UpdateStatus()
extern void TurnBasePlayer_UpdateStatus_mB553802459C3E16BD07666672E8FD3A3D8957C62 (void);
// 0x0000016E System.Void TurnBasePlayer::DebugFullPlayerStatue()
extern void TurnBasePlayer_DebugFullPlayerStatue_m3FD29EC404555DD300CDAC0DBBFE7F63B514F257 (void);
// 0x0000016F System.Void TurnBasePlayer::.ctor()
extern void TurnBasePlayer__ctor_m2F938D8F16DD039005D079D9263082836439786B (void);
// 0x00000170 TurnBaseUnit TurnBaseSystem::get_enemyUnit()
extern void TurnBaseSystem_get_enemyUnit_m51350F4B8790ADA3A6CBD2F73066529F13E22320 (void);
// 0x00000171 TurnBasePlayer TurnBaseSystem::get_playerUnit()
extern void TurnBaseSystem_get_playerUnit_m8B8D68B355582068989616C845F11AF8391A9253 (void);
// 0x00000172 TurnBaseSystem TurnBaseSystem::get_Instance()
extern void TurnBaseSystem_get_Instance_mF91629977DE4E454B34331DE163F5377D2169229 (void);
// 0x00000173 System.Boolean TurnBaseSystem::get_inbattle()
extern void TurnBaseSystem_get_inbattle_m99F1923942FEEC348F29314A6FAE608D0E337700 (void);
// 0x00000174 System.Void TurnBaseSystem::Awake()
extern void TurnBaseSystem_Awake_m4352AAB3D281039297BF3CDD38B8C38477A89A8A (void);
// 0x00000175 System.Void TurnBaseSystem::Start()
extern void TurnBaseSystem_Start_m2D8F03F1DB3F5CC64C349E32EDE932102535EE14 (void);
// 0x00000176 System.Void TurnBaseSystem::Update()
extern void TurnBaseSystem_Update_m8016CF36F5E4BF6B3B80D4C5BB3C0DC1FB7FA551 (void);
// 0x00000177 System.Void TurnBaseSystem::SetupBattle(UnitProfile[])
extern void TurnBaseSystem_SetupBattle_mA8A505D87F206EAF8C6FF1BF7EA4EE030859E683 (void);
// 0x00000178 System.Void TurnBaseSystem::EndBattle()
extern void TurnBaseSystem_EndBattle_m04FE104E66DD5B7603A5E7E310019698B54FA902 (void);
// 0x00000179 System.Void TurnBaseSystem::CurrentNPCTarget(UnityEngine.GameObject)
extern void TurnBaseSystem_CurrentNPCTarget_m5EA854441406654AF6E70ADCE564772AB7A2273D (void);
// 0x0000017A System.Void TurnBaseSystem::EnterDialogue(DialogueGraph)
extern void TurnBaseSystem_EnterDialogue_mAF29D4AAE654BE261F650DAAF854564F22FFD4A5 (void);
// 0x0000017B System.Void TurnBaseSystem::OpenActionTab()
extern void TurnBaseSystem_OpenActionTab_m3B8581A9F45E7FBCF7271A2673BA2FB37ECB10F4 (void);
// 0x0000017C System.Void TurnBaseSystem::OpenAttackTab()
extern void TurnBaseSystem_OpenAttackTab_m20421553E62709D797E58A4DC33C4B7946AC51A5 (void);
// 0x0000017D System.Void TurnBaseSystem::OpenSkilllTab()
extern void TurnBaseSystem_OpenSkilllTab_mD127B6EDB855E79650032200F02117893019EF77 (void);
// 0x0000017E System.Void TurnBaseSystem::PlayerAttackAction(AttackProfile)
extern void TurnBaseSystem_PlayerAttackAction_mE08CD3B970472CED6EF0AE6A494CF5B496BFC40F (void);
// 0x0000017F System.Void TurnBaseSystem::PlayerSkillAction(AttackProfile,System.Boolean)
extern void TurnBaseSystem_PlayerSkillAction_mD6903E2C33C9B5200E08D31765160EF7B1BEAC4B (void);
// 0x00000180 System.Void TurnBaseSystem::EnemyAttackAction(AttackProfile)
extern void TurnBaseSystem_EnemyAttackAction_m57DBEF969E3F6FA517DD353D7609A47200910FD9 (void);
// 0x00000181 System.Collections.IEnumerator TurnBaseSystem::PlayerTurn(System.Single)
extern void TurnBaseSystem_PlayerTurn_m64279C163C3632B41265E29A38CE186760934439 (void);
// 0x00000182 System.Collections.IEnumerator TurnBaseSystem::PlayerHealTurn(System.Single)
extern void TurnBaseSystem_PlayerHealTurn_mE022E32CFA0E9B5A2CDFA97F4F6F3C7C02031F7E (void);
// 0x00000183 System.Collections.IEnumerator TurnBaseSystem::EnemyTurn()
extern void TurnBaseSystem_EnemyTurn_m953332AC41E491EA08D064B7566E08E5211C6076 (void);
// 0x00000184 System.Void TurnBaseSystem::Resting()
extern void TurnBaseSystem_Resting_m9F8D83DBCBCBA3032F60AC74509D2F1A8F94FE16 (void);
// 0x00000185 System.Void TurnBaseSystem::SkillHeal(AttackProfile)
extern void TurnBaseSystem_SkillHeal_mCD4C00815BE5D059CE8CCD645C032BF363BA05EC (void);
// 0x00000186 System.Void TurnBaseSystem::.ctor()
extern void TurnBaseSystem__ctor_m7C641C0BBE80EA6619F10A5271C76A985E5DF3B0 (void);
// 0x00000187 System.Boolean TurnBaseSystem::<PlayerTurn>b__35_0()
extern void TurnBaseSystem_U3CPlayerTurnU3Eb__35_0_m71013D9FECDC342FF5202C42E881593D8CB8ECA2 (void);
// 0x00000188 System.Boolean TurnBaseSystem::<PlayerHealTurn>b__36_0()
extern void TurnBaseSystem_U3CPlayerHealTurnU3Eb__36_0_m4F6A8805E871F063CB75F553296744F0FFD78D5F (void);
// 0x00000189 System.Boolean TurnBaseSystem::<EnemyTurn>b__37_0()
extern void TurnBaseSystem_U3CEnemyTurnU3Eb__37_0_m6587319AD0E3D72F409E682B319B063AAC5E1A6F (void);
// 0x0000018A System.Void TurnBaseSystem/<PlayerTurn>d__35::.ctor(System.Int32)
extern void U3CPlayerTurnU3Ed__35__ctor_m8AB2530F00AAC23868A1014008E4510CC8A85E7B (void);
// 0x0000018B System.Void TurnBaseSystem/<PlayerTurn>d__35::System.IDisposable.Dispose()
extern void U3CPlayerTurnU3Ed__35_System_IDisposable_Dispose_mBA54B120599E2137E5393768D6120030E95A336B (void);
// 0x0000018C System.Boolean TurnBaseSystem/<PlayerTurn>d__35::MoveNext()
extern void U3CPlayerTurnU3Ed__35_MoveNext_m6943F4F83434E0EB6AA8FBA38251229DB98EE141 (void);
// 0x0000018D System.Object TurnBaseSystem/<PlayerTurn>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayerTurnU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61EACE996BC752C1364CCA92B5C2166D4123E2AC (void);
// 0x0000018E System.Void TurnBaseSystem/<PlayerTurn>d__35::System.Collections.IEnumerator.Reset()
extern void U3CPlayerTurnU3Ed__35_System_Collections_IEnumerator_Reset_mE2DDF9EF573EE8506B67333FD381EC49558D4D04 (void);
// 0x0000018F System.Object TurnBaseSystem/<PlayerTurn>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CPlayerTurnU3Ed__35_System_Collections_IEnumerator_get_Current_m66E0053278E42810479BE9E520E40D27F246FAC3 (void);
// 0x00000190 System.Void TurnBaseSystem/<PlayerHealTurn>d__36::.ctor(System.Int32)
extern void U3CPlayerHealTurnU3Ed__36__ctor_m16FD053EDC1AA07B2FEFB29F67B4AA7000E791F2 (void);
// 0x00000191 System.Void TurnBaseSystem/<PlayerHealTurn>d__36::System.IDisposable.Dispose()
extern void U3CPlayerHealTurnU3Ed__36_System_IDisposable_Dispose_m93A030B3E7AF0DCF29F04DFA1A1047D57632DA42 (void);
// 0x00000192 System.Boolean TurnBaseSystem/<PlayerHealTurn>d__36::MoveNext()
extern void U3CPlayerHealTurnU3Ed__36_MoveNext_mFA42B2A4EABBF0763C8926D529BF5964DAA5AD05 (void);
// 0x00000193 System.Object TurnBaseSystem/<PlayerHealTurn>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPlayerHealTurnU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA71D3FD00986769C1CECB82B8711E700D8430D99 (void);
// 0x00000194 System.Void TurnBaseSystem/<PlayerHealTurn>d__36::System.Collections.IEnumerator.Reset()
extern void U3CPlayerHealTurnU3Ed__36_System_Collections_IEnumerator_Reset_mC26A5D24136DEF19D8186666D9610B2B419215E1 (void);
// 0x00000195 System.Object TurnBaseSystem/<PlayerHealTurn>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CPlayerHealTurnU3Ed__36_System_Collections_IEnumerator_get_Current_m0F726B46089D8ACB32B00F591BE0D9B9361BE751 (void);
// 0x00000196 System.Void TurnBaseSystem/<EnemyTurn>d__37::.ctor(System.Int32)
extern void U3CEnemyTurnU3Ed__37__ctor_m9E7906BD17A2D26440DDBE0432B224E44AF8DAFA (void);
// 0x00000197 System.Void TurnBaseSystem/<EnemyTurn>d__37::System.IDisposable.Dispose()
extern void U3CEnemyTurnU3Ed__37_System_IDisposable_Dispose_m4DDADCDAB60E4E76F6BF71E63260855719A5B556 (void);
// 0x00000198 System.Boolean TurnBaseSystem/<EnemyTurn>d__37::MoveNext()
extern void U3CEnemyTurnU3Ed__37_MoveNext_mB6A16F0679A1CE9A1AC81B7354CA9DA592FB6560 (void);
// 0x00000199 System.Object TurnBaseSystem/<EnemyTurn>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEnemyTurnU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7E55AF6A8D378D610248C15AABB1DFB37EA4FF6 (void);
// 0x0000019A System.Void TurnBaseSystem/<EnemyTurn>d__37::System.Collections.IEnumerator.Reset()
extern void U3CEnemyTurnU3Ed__37_System_Collections_IEnumerator_Reset_m37EA51052F303CAEB8428D0A0BEA66936454B4DA (void);
// 0x0000019B System.Object TurnBaseSystem/<EnemyTurn>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CEnemyTurnU3Ed__37_System_Collections_IEnumerator_get_Current_mBFE15F4A828D23A7AA24CF09039B4D93C3870896 (void);
// 0x0000019C System.Collections.Generic.List`1<AttackProfile> TurnBaseUnit::get_attackProfile()
extern void TurnBaseUnit_get_attackProfile_m5EC8B0BB4333910F3F7A583C510AD1CBE9220C3A (void);
// 0x0000019D System.Void TurnBaseUnit::Update()
extern void TurnBaseUnit_Update_m3EE9F07A2BD9F457618C7DFECEA22526860FC77F (void);
// 0x0000019E System.Void TurnBaseUnit::SetUnitProfile(UnitProfile[])
extern void TurnBaseUnit_SetUnitProfile_m4ADA07FD6D13CBDC18941F0FB8835524793B1509 (void);
// 0x0000019F System.Void TurnBaseUnit::SetUnit()
extern void TurnBaseUnit_SetUnit_mD33BB1EAAC743E34C7BB891EF50722322135E0BB (void);
// 0x000001A0 System.Void TurnBaseUnit::UpdateStatus()
extern void TurnBaseUnit_UpdateStatus_m7BDB439E6F228B0C4C2E63D272FCF87DFDB97786 (void);
// 0x000001A1 System.Void TurnBaseUnit::DebugSetHPtoLow()
extern void TurnBaseUnit_DebugSetHPtoLow_mCD17C7C107760924287F33E0AE7EEBC8BD18CBD7 (void);
// 0x000001A2 System.Void TurnBaseUnit::.ctor()
extern void TurnBaseUnit__ctor_mD5D120FAB64631026ABEAD1DC1DA043740DA5039 (void);
// 0x000001A3 TurnBasedUIManager TurnBasedUIManager::get_Instance()
extern void TurnBasedUIManager_get_Instance_m75EF5E7A730B20708AD7B3BBAFBA3966EBE9B995 (void);
// 0x000001A4 System.Void TurnBasedUIManager::Awake()
extern void TurnBasedUIManager_Awake_m9A6345BB0DCF94D18C0FC0C0DE6F3D9F8E22A70D (void);
// 0x000001A5 System.Void TurnBasedUIManager::Start()
extern void TurnBasedUIManager_Start_m5A425962A1586A14D316FB09B1C831B7D512C9A2 (void);
// 0x000001A6 System.Void TurnBasedUIManager::GenerateAttackButton(System.Collections.Generic.List`1<AttackProfile>)
extern void TurnBasedUIManager_GenerateAttackButton_m9E301967E44348C58855DFF3A6F01D5426DC4817 (void);
// 0x000001A7 System.Void TurnBasedUIManager::ClearButton()
extern void TurnBasedUIManager_ClearButton_m96C1D0D24209738D29C797EA2ECA2A88079A555C (void);
// 0x000001A8 System.Void TurnBasedUIManager::ChangeAttackDetailText(System.String,System.String,UnityEngine.Sprite)
extern void TurnBasedUIManager_ChangeAttackDetailText_mBEF7EC8E397C87A011E6C953506015C7CA4E7DF7 (void);
// 0x000001A9 System.Void TurnBasedUIManager::ChangeStaminaUsage(System.Single,System.Single)
extern void TurnBasedUIManager_ChangeStaminaUsage_m81BF49A3A6CA5C9B1280DD51022027D123AA554B (void);
// 0x000001AA System.Void TurnBasedUIManager::OpenActionTab()
extern void TurnBasedUIManager_OpenActionTab_mA5D8D1B4753A96A8810A1E85E1F42A6D9BB95E0A (void);
// 0x000001AB System.Void TurnBasedUIManager::OpenAttackTab()
extern void TurnBasedUIManager_OpenAttackTab_m901A501A97162735F0CDF4B263CB975010F6C860 (void);
// 0x000001AC System.Void TurnBasedUIManager::OpenSkillTab()
extern void TurnBasedUIManager_OpenSkillTab_m8E1C03C81CCB5CF67DCCAA7D270667BC6AF09BCD (void);
// 0x000001AD System.Void TurnBasedUIManager::Resting()
extern void TurnBasedUIManager_Resting_m4DF4A614BE945B9C50F59928733BAA2F87E4998B (void);
// 0x000001AE System.Void TurnBasedUIManager::OpenResult(System.Boolean)
extern void TurnBasedUIManager_OpenResult_m3104EBA7FEAD2C6F8A4A2868D34C98CC286F8914 (void);
// 0x000001AF System.Void TurnBasedUIManager::EndBattle()
extern void TurnBasedUIManager_EndBattle_m974B25B43D9169D06772125BEF2FE3F6B11FE03D (void);
// 0x000001B0 System.Void TurnBasedUIManager::.ctor()
extern void TurnBasedUIManager__ctor_m9F086F9214D6626F4ABAF1FD0C99A414EC036AEB (void);
// 0x000001B1 System.Void CameraManager::Start()
extern void CameraManager_Start_m3A8F66B836B9A9BC419B62EF88451549BBC9CCD0 (void);
// 0x000001B2 System.Void CameraManager::Update()
extern void CameraManager_Update_m911D2029A80BF20148CD94D496452F232343FF60 (void);
// 0x000001B3 System.Void CameraManager::.ctor()
extern void CameraManager__ctor_m932EDBCBAE89115380AA929ED5BC9823B9371185 (void);
// 0x000001B4 System.Void CharacterCreationMneu::Start()
extern void CharacterCreationMneu_Start_m32257EEE3887D9BB68471BBBC430B9DBFFD2DD2B (void);
// 0x000001B5 System.Void CharacterCreationMneu::ChangeCharacterImageRight()
extern void CharacterCreationMneu_ChangeCharacterImageRight_mD2AC61C9426B83974F7EF0CAA92DD9AEDB995535 (void);
// 0x000001B6 System.Void CharacterCreationMneu::ChangeCharacterImageLeft()
extern void CharacterCreationMneu_ChangeCharacterImageLeft_m6C4F38DE16F0FFE9E51D9F326CF3DC87DFBBC608 (void);
// 0x000001B7 System.Void CharacterCreationMneu::ChangeCharacterName()
extern void CharacterCreationMneu_ChangeCharacterName_m6C56DEE1A5A0EF30D1D57AE49B9635F3809EAA62 (void);
// 0x000001B8 System.Void CharacterCreationMneu::SetImage(UnityEngine.Sprite)
extern void CharacterCreationMneu_SetImage_m3C44F5F44EB813DAC4766E4E212E683025EE3F48 (void);
// 0x000001B9 System.Void CharacterCreationMneu::SetProfile()
extern void CharacterCreationMneu_SetProfile_mAD53DC730D7899374015B10F670FF2DEDCC0B4C5 (void);
// 0x000001BA System.Void CharacterCreationMneu::ResetProfile()
extern void CharacterCreationMneu_ResetProfile_mDA5850DEBA57886979CFF5942A804B299C32853B (void);
// 0x000001BB System.Void CharacterCreationMneu::IncreaseStat(System.Int32)
extern void CharacterCreationMneu_IncreaseStat_m8C60F3155CC238235057C6C54242C09127CE9639 (void);
// 0x000001BC System.Void CharacterCreationMneu::DecreaseStat(System.Int32)
extern void CharacterCreationMneu_DecreaseStat_m200A44D05270F427D37B4C690E096E0065DFF852 (void);
// 0x000001BD System.Void CharacterCreationMneu::SetButton()
extern void CharacterCreationMneu_SetButton_m2606BF7484D7B3A7FD5821C104CD37EF32374735 (void);
// 0x000001BE System.Void CharacterCreationMneu::.ctor()
extern void CharacterCreationMneu__ctor_m2692750A1A15113CCA5B92455B91D8619C73486F (void);
// 0x000001BF System.Void CharacterCreationMneu/<>c__DisplayClass22_0::.ctor()
extern void U3CU3Ec__DisplayClass22_0__ctor_mC99D3FCF62DF88622051F92E708A3E83DD88329E (void);
// 0x000001C0 System.Void CharacterCreationMneu/<>c__DisplayClass22_0::<SetButton>b__0()
extern void U3CU3Ec__DisplayClass22_0_U3CSetButtonU3Eb__0_mD85AD97BC66C9FB5E133C473C346D2ACD7A8E996 (void);
// 0x000001C1 System.Void CharacterCreationMneu/<>c__DisplayClass22_0::<SetButton>b__1()
extern void U3CU3Ec__DisplayClass22_0_U3CSetButtonU3Eb__1_m924BB2A7E1B4079762B8F406F54DE5805820160B (void);
// 0x000001C2 System.Void CustomCamera::Start()
extern void CustomCamera_Start_m8CB9ADCB516D51E8E5F4B33490945B2F32C08415 (void);
// 0x000001C3 System.Void CustomCamera::Update()
extern void CustomCamera_Update_m25201A4AD9D0F6357B35E02FFBD85EE2B46ED701 (void);
// 0x000001C4 System.Void CustomCamera::CameraMovement()
extern void CustomCamera_CameraMovement_mCFAE95ACAC4E135952C29C6E08D87CF55B02F73C (void);
// 0x000001C5 System.Boolean CustomCamera::get_CheckX()
extern void CustomCamera_get_CheckX_m048A9DD496A436806A907EBFC1F62183EAF20B06 (void);
// 0x000001C6 System.Boolean CustomCamera::get_CheckY()
extern void CustomCamera_get_CheckY_mA51848E656CF5A4FFC6B01CFA17C7297F4F4363A (void);
// 0x000001C7 System.Void CustomCamera::.ctor()
extern void CustomCamera__ctor_m924280DC9D40E5DC1482DCCCDCD7E501F53E985D (void);
// 0x000001C8 System.Void MenuManager::Awake()
extern void MenuManager_Awake_mEFBAF3F8CBDEF5A033B3BAD9CA897801135B6463 (void);
// 0x000001C9 System.Void MenuManager::Start()
extern void MenuManager_Start_m4E4A5EF33C27448D7F34FD29B93589635F1B2EE2 (void);
// 0x000001CA System.Void MenuManager::OpenCharacterCreationMenu()
extern void MenuManager_OpenCharacterCreationMenu_mE2561068153DA0923C7F297F5873CF2EE71DB4F6 (void);
// 0x000001CB System.Void MenuManager::OpenMainMenu()
extern void MenuManager_OpenMainMenu_mEEDFCE3B23480F6ECF1B092EC9042E1963001B43 (void);
// 0x000001CC System.Void MenuManager::OpenSetting()
extern void MenuManager_OpenSetting_m31C6ADF97C8A7E0F331653D00A266A2A5DDE25D5 (void);
// 0x000001CD System.Void MenuManager::OpenCredit()
extern void MenuManager_OpenCredit_m5366171F17422BDAAEF6A606A002BFAC491DD0AA (void);
// 0x000001CE System.Void MenuManager::ExitGame()
extern void MenuManager_ExitGame_mC7F9385BFDAD357058DAD3C97268BC8993494F6B (void);
// 0x000001CF System.Void MenuManager::CloseAll()
extern void MenuManager_CloseAll_m8DF0F562FD7E6BAE4145DCF6281DC8D5C2459B8E (void);
// 0x000001D0 System.Void MenuManager::loadScene(System.Int32)
extern void MenuManager_loadScene_mB717D5E9676CEF035C842EF68CE71DC876F68F3A (void);
// 0x000001D1 System.Void MenuManager::RunStartupCheck()
extern void MenuManager_RunStartupCheck_m3D1F693BFAAC94F6B6E4428EB1CF4A49763E12EE (void);
// 0x000001D2 System.Void MenuManager::WebCheck()
extern void MenuManager_WebCheck_m2537950FF57C182A25A718D43A14B8B5D2EBB01D (void);
// 0x000001D3 System.Void MenuManager::OnClickUI()
extern void MenuManager_OnClickUI_mB9BAF3AF7CBEE08E0270322F7EE81FA86A3EF7E9 (void);
// 0x000001D4 System.Void MenuManager::OnCancelledUI()
extern void MenuManager_OnCancelledUI_m49919F4FD962BE8A25A136B788066E4C34E87A40 (void);
// 0x000001D5 System.Void MenuManager::OnHoverUI()
extern void MenuManager_OnHoverUI_m4923418F5F93258590FB02C319219E56EF8B5E9A (void);
// 0x000001D6 System.Void MenuManager::.ctor()
extern void MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529 (void);
// 0x000001D7 System.Void MenuTabManager::Start()
extern void MenuTabManager_Start_mDA80B67CE9422788DFE0955438846C86433E1B54 (void);
// 0x000001D8 System.Void MenuTabManager::Update()
extern void MenuTabManager_Update_m5EEEB00FDF26482CA7A8BEB7B524CABA38BF6832 (void);
// 0x000001D9 System.Void MenuTabManager::OpenCloseMenuTab()
extern void MenuTabManager_OpenCloseMenuTab_mBE2D6AE74A266E8661072C33948CBD72EA57A772 (void);
// 0x000001DA System.Void MenuTabManager::OpenCloseSubTab(System.Int32)
extern void MenuTabManager_OpenCloseSubTab_m1F7C1C7913BA28CFA6ECECE1353BC72383FBAC83 (void);
// 0x000001DB System.Void MenuTabManager::CheckPlayerInformation()
extern void MenuTabManager_CheckPlayerInformation_mE5FA62007FC29E5515FCB1DB00389EB06780578A (void);
// 0x000001DC System.Void MenuTabManager::Pause()
extern void MenuTabManager_Pause_mE8E795AE9D476FD4CD349075E37873B8DBEF111F (void);
// 0x000001DD System.Void MenuTabManager::Resume()
extern void MenuTabManager_Resume_mBC6F70080425BE49A79B45792DCE7DB71660C1B5 (void);
// 0x000001DE System.Void MenuTabManager::loadScene(System.Int32)
extern void MenuTabManager_loadScene_m8C362A7505F8A97FBE52CCA1E364244C31FC11E5 (void);
// 0x000001DF System.Void MenuTabManager::IncreaseStat(System.Int32)
extern void MenuTabManager_IncreaseStat_mC55760452B1B4F86327F0A1A13177466B0AAE3C2 (void);
// 0x000001E0 System.Void MenuTabManager::UpdateStatText()
extern void MenuTabManager_UpdateStatText_m8935AB46D891F76508F5F32FBADDC1103AD169CD (void);
// 0x000001E1 System.Void MenuTabManager::WebCheck()
extern void MenuTabManager_WebCheck_mAFC1BA7FA1DB43B4BF79A16672F1B0A3AC2C31CD (void);
// 0x000001E2 System.Void MenuTabManager::OnClickUI()
extern void MenuTabManager_OnClickUI_mF237C0B938076A0D70C840155852C0878DA288B7 (void);
// 0x000001E3 System.Void MenuTabManager::OnCancelledUI()
extern void MenuTabManager_OnCancelledUI_mAB1B95B63F945B017899EB9DA7500148E74BAD25 (void);
// 0x000001E4 System.Void MenuTabManager::OnHoverUI()
extern void MenuTabManager_OnHoverUI_mB1DA6C8180247BE70AA814BCC74760470F657E8F (void);
// 0x000001E5 System.Void MenuTabManager::.ctor()
extern void MenuTabManager__ctor_m2CEA8B55C342FCEBA7824C34750C4036AD65DFC2 (void);
// 0x000001E6 System.Void MenuTabManager/<>c__DisplayClass30_0::.ctor()
extern void U3CU3Ec__DisplayClass30_0__ctor_m34E5A1AAB7275186461FA2FE7E89E73769F5937B (void);
// 0x000001E7 System.Void MenuTabManager/<>c__DisplayClass30_0::<Start>b__0()
extern void U3CU3Ec__DisplayClass30_0_U3CStartU3Eb__0_m0AAC18AFD777333DEFAE362E22BD087A711B275B (void);
// 0x000001E8 System.Void ResolutionSetting::SetResolution(System.Int32)
extern void ResolutionSetting_SetResolution_m6CE1C2CA19F20697FFF1F93D0A4E7FD718D282C2 (void);
// 0x000001E9 System.Void ResolutionSetting::SetFullscreen(System.Boolean)
extern void ResolutionSetting_SetFullscreen_m9A3ACC61D389703B6579FD109FCB33F77911A5A8 (void);
// 0x000001EA System.Void ResolutionSetting::dropDownCurrentStartResolution()
extern void ResolutionSetting_dropDownCurrentStartResolution_m561E225092EB39044346CA7CF855022DEA3AD828 (void);
// 0x000001EB System.Void ResolutionSetting::.ctor()
extern void ResolutionSetting__ctor_mB0A19DB25DD45D37837224D84E80A95C1F0739AD (void);
// 0x000001EC SceneLoader SceneLoader::get_Instance()
extern void SceneLoader_get_Instance_m260F687667E99FC4E952DEDC6B3FC65CC788F0BD (void);
// 0x000001ED System.Void SceneLoader::Awake()
extern void SceneLoader_Awake_m0618C263DDE94B4607D9D5148F87247B506A337C (void);
// 0x000001EE System.Void SceneLoader::startLoadLevel(System.Int32)
extern void SceneLoader_startLoadLevel_m04754948AF7C6B19FB5FB11C8875F6F6B8D3EA25 (void);
// 0x000001EF System.Void SceneLoader::exitToDestop()
extern void SceneLoader_exitToDestop_mABAD45692C757391FE8C86289AC7128ABFECC8F3 (void);
// 0x000001F0 System.Collections.IEnumerator SceneLoader::loadlevel(System.Int32)
extern void SceneLoader_loadlevel_m04CF2C85AEFC903C21B789AD1825E90000E5DC97 (void);
// 0x000001F1 System.Collections.IEnumerator SceneLoader::exitGame()
extern void SceneLoader_exitGame_m2924FA892A01B56DE948D8583EC159DCB0DD1D36 (void);
// 0x000001F2 System.Void SceneLoader::TransitioningBegin()
extern void SceneLoader_TransitioningBegin_mEEC98108AAFE48A2386596D3F119D103ACF22B36 (void);
// 0x000001F3 System.Void SceneLoader::TransitioningEnd()
extern void SceneLoader_TransitioningEnd_m2B0F39FE99C608DD3F7A70274CB480632E81D77B (void);
// 0x000001F4 System.Void SceneLoader::.ctor()
extern void SceneLoader__ctor_m8708D080848349110CEA260D8779F30BD5823912 (void);
// 0x000001F5 System.Void SceneLoader/<loadlevel>d__10::.ctor(System.Int32)
extern void U3CloadlevelU3Ed__10__ctor_mAE58B80B195BBFA7510F6E68EA9A1298361DBBA4 (void);
// 0x000001F6 System.Void SceneLoader/<loadlevel>d__10::System.IDisposable.Dispose()
extern void U3CloadlevelU3Ed__10_System_IDisposable_Dispose_m860B69D84C70B4F5896588A3D6F8064C6960C632 (void);
// 0x000001F7 System.Boolean SceneLoader/<loadlevel>d__10::MoveNext()
extern void U3CloadlevelU3Ed__10_MoveNext_m2D8401AFBD77283234CC84DF6144D756A800B0D6 (void);
// 0x000001F8 System.Object SceneLoader/<loadlevel>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CloadlevelU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DC83A18DF8FF983CA0BF345C8B581996D42D402 (void);
// 0x000001F9 System.Void SceneLoader/<loadlevel>d__10::System.Collections.IEnumerator.Reset()
extern void U3CloadlevelU3Ed__10_System_Collections_IEnumerator_Reset_m51A5262162ECD04D7DE7A5F02E29160606469126 (void);
// 0x000001FA System.Object SceneLoader/<loadlevel>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CloadlevelU3Ed__10_System_Collections_IEnumerator_get_Current_m3AD12E1478791BEAA33BB99964E9329A73384BD6 (void);
// 0x000001FB System.Void SceneLoader/<exitGame>d__11::.ctor(System.Int32)
extern void U3CexitGameU3Ed__11__ctor_mFF73E161755102B656F61250D9D576BE9A096C0F (void);
// 0x000001FC System.Void SceneLoader/<exitGame>d__11::System.IDisposable.Dispose()
extern void U3CexitGameU3Ed__11_System_IDisposable_Dispose_mE0A07690D50D877F2029B4AAD833CBBDF8B5FAD8 (void);
// 0x000001FD System.Boolean SceneLoader/<exitGame>d__11::MoveNext()
extern void U3CexitGameU3Ed__11_MoveNext_m0AFD3A8DEEF3C9FB4243BA7A6928D26A36AC59AC (void);
// 0x000001FE System.Object SceneLoader/<exitGame>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CexitGameU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFEE4287F4E70EF27C575F348CABF960F5CEB4DCC (void);
// 0x000001FF System.Void SceneLoader/<exitGame>d__11::System.Collections.IEnumerator.Reset()
extern void U3CexitGameU3Ed__11_System_Collections_IEnumerator_Reset_m42D8867845452E08B152564A3AD4191A22C79851 (void);
// 0x00000200 System.Object SceneLoader/<exitGame>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CexitGameU3Ed__11_System_Collections_IEnumerator_get_Current_mB57F50DA13B68471F8331091EEDAAC4788CFF4C3 (void);
// 0x00000201 System.Void VolumeMixer::Awake()
extern void VolumeMixer_Awake_m23F7C0578529E573B13C7A8FDC402A310BFD12B6 (void);
// 0x00000202 System.Void VolumeMixer::StartUpSet()
extern void VolumeMixer_StartUpSet_m715236EB9A605ECDD39FB6794B4F933F8EAED155 (void);
// 0x00000203 System.Void VolumeMixer::SetVolume(System.Single)
extern void VolumeMixer_SetVolume_mFE149DEB806A9D3FDAE5242829C35248A479D795 (void);
// 0x00000204 System.Void VolumeMixer::PlayEffectOnce()
extern void VolumeMixer_PlayEffectOnce_mC3AD134F86585FBA710D30736C38E412224012BD (void);
// 0x00000205 System.Void VolumeMixer::.ctor()
extern void VolumeMixer__ctor_m0026E479FD2F148D6067EF304C73ECDE1E915FCE (void);
// 0x00000206 WinLoseManager WinLoseManager::get_Instance()
extern void WinLoseManager_get_Instance_mC4D2B4CA667A9CDBC5B4E494174DE668F1E2344B (void);
// 0x00000207 System.Void WinLoseManager::Awake()
extern void WinLoseManager_Awake_m41D60A8A03A24DF139CE8FB566EFFD8E009279E7 (void);
// 0x00000208 System.Void WinLoseManager::Start()
extern void WinLoseManager_Start_m2E32930E3AF736B1D19D5F101559E63754AE4CE6 (void);
// 0x00000209 System.Void WinLoseManager::Update()
extern void WinLoseManager_Update_mD99C4FF5969FC2B24E42809CCBFD4FCAA49FCBF6 (void);
// 0x0000020A System.Void WinLoseManager::WinCheck()
extern void WinLoseManager_WinCheck_m40489380BFE19E913EBF5B383A9169E0166C5938 (void);
// 0x0000020B System.Void WinLoseManager::.ctor()
extern void WinLoseManager__ctor_m9BEA6A4A6A01273878129498C2CC4177230EDAC9 (void);
// 0x0000020C System.Void WorldNPC::Start()
extern void WorldNPC_Start_m63C1535B1E8237579549FCA2D997FA2D1114B3BE (void);
// 0x0000020D System.Void WorldNPC::Update()
extern void WorldNPC_Update_m6A4D1A7E6E321DFC6136E20E55F00A629641271F (void);
// 0x0000020E System.Void WorldNPC::NPCNotificationBox()
extern void WorldNPC_NPCNotificationBox_m094F73AF08085D59D373FD77BF23D13CC9F9E2FC (void);
// 0x0000020F System.Void WorldNPC::Interaction()
extern void WorldNPC_Interaction_mB686444EA3C37D5FECE2B7545FC1266DE45B5E95 (void);
// 0x00000210 System.Void WorldNPC::PlayDialogue(DialogueGraph)
extern void WorldNPC_PlayDialogue_m05EF23E7667CCB555EFDF7400C3A9E62A1157B92 (void);
// 0x00000211 System.Void WorldNPC::MouseHovering()
extern void WorldNPC_MouseHovering_m133B22B864AE178D67BFFCED1C21370CCD4A40F7 (void);
// 0x00000212 System.Void WorldNPC::MouseNotHovering()
extern void WorldNPC_MouseNotHovering_mDB720C36AD4B56AA3DFA78F538E820825EF82108 (void);
// 0x00000213 System.Void WorldNPC::PlayOneShot(UnityEngine.AudioClip)
extern void WorldNPC_PlayOneShot_m231B1C37029CCFDB1605FEF3BF694D1ECE91C106 (void);
// 0x00000214 System.Void WorldNPC::.ctor()
extern void WorldNPC__ctor_m2FE39FE97859583BD1C0677FED187F6D72897E23 (void);
// 0x00000215 System.Void XNode.Examples.MathGraph::.ctor()
extern void MathGraph__ctor_m08F714216D2826AD4EE77B6E6CA0EE18C487034C (void);
// 0x00000216 System.Void XNode.Examples.StateGraph.StateNode::MoveNext()
extern void StateNode_MoveNext_mF38DF0A127D499DC6FCD3CD2EC2FFFD44CCE329C (void);
// 0x00000217 System.Void XNode.Examples.StateGraph.StateNode::OnEnter()
extern void StateNode_OnEnter_mD3B49F17ACC5833C7893AF00B0748905A885E274 (void);
// 0x00000218 System.Void XNode.Examples.StateGraph.StateNode::.ctor()
extern void StateNode__ctor_m4C751F717E323C2EB3A0C1AAC81AEC1511892674 (void);
// 0x00000219 System.Void XNode.Examples.StateGraph.StateNode/Empty::.ctor()
extern void Empty__ctor_mB3CFFBD34162B6ECDDB72EE7404D4CE10F8F6CE4 (void);
// 0x0000021A System.Void XNode.Examples.StateGraph.StateGraph::Continue()
extern void StateGraph_Continue_mAB5A6B3406F1AD7C5BD984E5C434B6FEEF6E6BB0 (void);
// 0x0000021B System.Void XNode.Examples.StateGraph.StateGraph::.ctor()
extern void StateGraph__ctor_mA29022A97B62DC51A73D18D0C20F894E50E5611B (void);
// 0x0000021C System.Void XNode.Examples.RuntimeMathNodes.Connection::SetPosition(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Connection_SetPosition_m0DE9B5BA8284A656F22A4FC41D68827DBD32EF3E (void);
// 0x0000021D System.Void XNode.Examples.RuntimeMathNodes.Connection::.ctor()
extern void Connection__ctor_m885969234D0CDC49BEC2C9EBE2F6E1EB9AF5A742 (void);
// 0x0000021E System.Void XNode.Examples.RuntimeMathNodes.NodeDrag::Awake()
extern void NodeDrag_Awake_mA27E486007B578DA6BBE766E6966A53EC3762231 (void);
// 0x0000021F System.Void XNode.Examples.RuntimeMathNodes.NodeDrag::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void NodeDrag_OnDrag_mC28D400062F09A29AA10C479212242164743E957 (void);
// 0x00000220 System.Void XNode.Examples.RuntimeMathNodes.NodeDrag::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void NodeDrag_OnBeginDrag_m06EF21377A6ED85C2B76498B75EFA34443F4DC43 (void);
// 0x00000221 System.Void XNode.Examples.RuntimeMathNodes.NodeDrag::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void NodeDrag_OnEndDrag_mF64FDE7FA151F9D74892CBCD5EA768F183E1815B (void);
// 0x00000222 System.Void XNode.Examples.RuntimeMathNodes.NodeDrag::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void NodeDrag_OnPointerClick_m61E783969FE13EC21DD1521BB496EE754C30930C (void);
// 0x00000223 System.Void XNode.Examples.RuntimeMathNodes.NodeDrag::.ctor()
extern void NodeDrag__ctor_m774BC055632E89033FF352CFEFBB3C9FE2A47E05 (void);
// 0x00000224 UnityEngine.UI.ScrollRect XNode.Examples.RuntimeMathNodes.RuntimeMathGraph::get_scrollRect()
extern void RuntimeMathGraph_get_scrollRect_m69128FFC7B3A2FCA86348549F47FD80E5CFEE579 (void);
// 0x00000225 System.Void XNode.Examples.RuntimeMathNodes.RuntimeMathGraph::set_scrollRect(UnityEngine.UI.ScrollRect)
extern void RuntimeMathGraph_set_scrollRect_m1986940E63F05E28544767835AB2A62B823F9163 (void);
// 0x00000226 System.Void XNode.Examples.RuntimeMathNodes.RuntimeMathGraph::Awake()
extern void RuntimeMathGraph_Awake_m0BC41A4604D8747F89FC44489AA601FAE12F5D5A (void);
// 0x00000227 System.Void XNode.Examples.RuntimeMathNodes.RuntimeMathGraph::Start()
extern void RuntimeMathGraph_Start_m37FADA97F393ECA30DCB7523146B4E4CD9523B95 (void);
// 0x00000228 System.Void XNode.Examples.RuntimeMathNodes.RuntimeMathGraph::Refresh()
extern void RuntimeMathGraph_Refresh_mAB35523776F6270C94B99A235D38C067E3EBD71F (void);
// 0x00000229 System.Void XNode.Examples.RuntimeMathNodes.RuntimeMathGraph::Clear()
extern void RuntimeMathGraph_Clear_m5315AEF3A710381876FC7EF568C50AB4F5C7D179 (void);
// 0x0000022A System.Void XNode.Examples.RuntimeMathNodes.RuntimeMathGraph::SpawnGraph()
extern void RuntimeMathGraph_SpawnGraph_m44240E4BC4A61614FA4289BDF3A2F985612BFA31 (void);
// 0x0000022B XNode.Examples.RuntimeMathNodes.UGUIMathBaseNode XNode.Examples.RuntimeMathNodes.RuntimeMathGraph::GetRuntimeNode(XNode.Node)
extern void RuntimeMathGraph_GetRuntimeNode_m12555443F553A33A9E498E5F57A737C532B68127 (void);
// 0x0000022C System.Void XNode.Examples.RuntimeMathNodes.RuntimeMathGraph::SpawnNode(System.Type,UnityEngine.Vector2)
extern void RuntimeMathGraph_SpawnNode_m18E97E41E618C18CF0F4D90E2E92EB03F344C413 (void);
// 0x0000022D System.Void XNode.Examples.RuntimeMathNodes.RuntimeMathGraph::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void RuntimeMathGraph_OnPointerClick_mFFB2097E7C639F4B1865E08B6CBED86CE931FB90 (void);
// 0x0000022E System.Void XNode.Examples.RuntimeMathNodes.RuntimeMathGraph::.ctor()
extern void RuntimeMathGraph__ctor_mE758CEC960AC34D4AF6415FD9E935D57013DE92F (void);
// 0x0000022F System.Void XNode.Examples.RuntimeMathNodes.UGUIDisplayValue::Update()
extern void UGUIDisplayValue_Update_m8F684BB8CA98C5A5875BFDA18E66EBF9400597B3 (void);
// 0x00000230 System.Void XNode.Examples.RuntimeMathNodes.UGUIDisplayValue::.ctor()
extern void UGUIDisplayValue__ctor_m49DCDC902B37CE58310FA86CEF9FAA7BB57730EE (void);
// 0x00000231 System.Void XNode.Examples.RuntimeMathNodes.UGUIMathBaseNode::Start()
extern void UGUIMathBaseNode_Start_mCC8230FAAD21E347905EAC8E43781F8F1C667C78 (void);
// 0x00000232 System.Void XNode.Examples.RuntimeMathNodes.UGUIMathBaseNode::UpdateGUI()
extern void UGUIMathBaseNode_UpdateGUI_m6E3FEDE70043BD6A020A4F4CAC2582F60AABBCBE (void);
// 0x00000233 System.Void XNode.Examples.RuntimeMathNodes.UGUIMathBaseNode::LateUpdate()
extern void UGUIMathBaseNode_LateUpdate_mD25DB9CC6BBB573B36A2FF7D4DB57E862B73BD70 (void);
// 0x00000234 XNode.Examples.RuntimeMathNodes.UGUIPort XNode.Examples.RuntimeMathNodes.UGUIMathBaseNode::GetPort(System.String)
extern void UGUIMathBaseNode_GetPort_m40640E74F1E11F6228B99EE4AEF755273912A228 (void);
// 0x00000235 System.Void XNode.Examples.RuntimeMathNodes.UGUIMathBaseNode::SetPosition(UnityEngine.Vector2)
extern void UGUIMathBaseNode_SetPosition_mED0CAC51A05BB47B57E4C22467C4C557A632197D (void);
// 0x00000236 System.Void XNode.Examples.RuntimeMathNodes.UGUIMathBaseNode::SetName(System.String)
extern void UGUIMathBaseNode_SetName_m7E4B927D9083D8339A8B7AE766041121B23F1E8D (void);
// 0x00000237 System.Void XNode.Examples.RuntimeMathNodes.UGUIMathBaseNode::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void UGUIMathBaseNode_OnDrag_mCA51DCF8AB74286AE291F17C769ABC116EDDF1C9 (void);
// 0x00000238 System.Void XNode.Examples.RuntimeMathNodes.UGUIMathBaseNode::.ctor()
extern void UGUIMathBaseNode__ctor_mFA4439A9A6BDF85DE3E54BD073FFFFA29B5B898B (void);
// 0x00000239 System.Void XNode.Examples.RuntimeMathNodes.UGUIMathNode::Start()
extern void UGUIMathNode_Start_m12F0F45407D6C2F9903B6D9A5442723F47886F73 (void);
// 0x0000023A System.Void XNode.Examples.RuntimeMathNodes.UGUIMathNode::UpdateGUI()
extern void UGUIMathNode_UpdateGUI_m8068871F046E2797C1054139F51DD9D9C3133C3F (void);
// 0x0000023B System.Void XNode.Examples.RuntimeMathNodes.UGUIMathNode::OnChangeValA(System.String)
extern void UGUIMathNode_OnChangeValA_mE43F3BEE4F1E987C9F4781CBBCAFF9FC93DCB3E2 (void);
// 0x0000023C System.Void XNode.Examples.RuntimeMathNodes.UGUIMathNode::OnChangeValB(System.String)
extern void UGUIMathNode_OnChangeValB_mD973F44CBA5355FF5A9E2E4CBCA57BEF74A99D4A (void);
// 0x0000023D System.Void XNode.Examples.RuntimeMathNodes.UGUIMathNode::OnChangeDropdown(System.Int32)
extern void UGUIMathNode_OnChangeDropdown_mA55CE87A28D662FC9A9B4C0D1FE2C156E3AE2F62 (void);
// 0x0000023E System.Void XNode.Examples.RuntimeMathNodes.UGUIMathNode::.ctor()
extern void UGUIMathNode__ctor_mE3040969A32BD31A360890C3C7828A1956F32419 (void);
// 0x0000023F System.Void XNode.Examples.RuntimeMathNodes.UGUIVector::Start()
extern void UGUIVector_Start_m0D32366FA45B76BDF3D815CA444D1FB3BBFBF2DA (void);
// 0x00000240 System.Void XNode.Examples.RuntimeMathNodes.UGUIVector::UpdateGUI()
extern void UGUIVector_UpdateGUI_m35127D5C240AAC71AE2CDC6E87291B1BBE3A82BA (void);
// 0x00000241 System.Void XNode.Examples.RuntimeMathNodes.UGUIVector::OnChangeValX(System.String)
extern void UGUIVector_OnChangeValX_m0D88CEC7A70BE205FAFB38118BCB6138E7C29BC0 (void);
// 0x00000242 System.Void XNode.Examples.RuntimeMathNodes.UGUIVector::OnChangeValY(System.String)
extern void UGUIVector_OnChangeValY_m322AFA104D56AD26A117D84FE51D44C9B89991F3 (void);
// 0x00000243 System.Void XNode.Examples.RuntimeMathNodes.UGUIVector::OnChangeValZ(System.String)
extern void UGUIVector_OnChangeValZ_m6DE56F27602B03A6503F03D155356C1C8B7E7299 (void);
// 0x00000244 System.Void XNode.Examples.RuntimeMathNodes.UGUIVector::.ctor()
extern void UGUIVector__ctor_m3B34111FC3E736C294B6BC5F34D9C6FA421D9626 (void);
// 0x00000245 System.Void XNode.Examples.RuntimeMathNodes.UGUIContextMenu::Start()
extern void UGUIContextMenu_Start_m5B4F01A8A8815C23DFB29B74948AF9740F9C4F46 (void);
// 0x00000246 System.Void XNode.Examples.RuntimeMathNodes.UGUIContextMenu::OpenAt(UnityEngine.Vector2)
extern void UGUIContextMenu_OpenAt_m234509E25996070CACA3ED6EC21859D7ECCA2CA5 (void);
// 0x00000247 System.Void XNode.Examples.RuntimeMathNodes.UGUIContextMenu::Close()
extern void UGUIContextMenu_Close_m0DA5264711DC3F5370AFE5DABCE6EF89A8DCC9E9 (void);
// 0x00000248 System.Void XNode.Examples.RuntimeMathNodes.UGUIContextMenu::SpawnMathNode()
extern void UGUIContextMenu_SpawnMathNode_m5A61225832760022A450FD77CE6971BAE8C7C4F7 (void);
// 0x00000249 System.Void XNode.Examples.RuntimeMathNodes.UGUIContextMenu::SpawnDisplayNode()
extern void UGUIContextMenu_SpawnDisplayNode_m0808BC446C93FA5A07415DD347EDD2C289BF2A07 (void);
// 0x0000024A System.Void XNode.Examples.RuntimeMathNodes.UGUIContextMenu::SpawnVectorNode()
extern void UGUIContextMenu_SpawnVectorNode_m52C2AE86073C370ABA1417A48A850780B2F44479 (void);
// 0x0000024B System.Void XNode.Examples.RuntimeMathNodes.UGUIContextMenu::SpawnNode(System.Type)
extern void UGUIContextMenu_SpawnNode_mB0111D2909BDF48F9982199E228DC5C7EA153127 (void);
// 0x0000024C System.Void XNode.Examples.RuntimeMathNodes.UGUIContextMenu::RemoveNode()
extern void UGUIContextMenu_RemoveNode_mCE90FA971477C7C2C75FF4BFC7CD9A050AB51607 (void);
// 0x0000024D System.Void XNode.Examples.RuntimeMathNodes.UGUIContextMenu::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void UGUIContextMenu_OnPointerExit_mF6AD7A860889506CFC4B5C3310653D49C0D7E132 (void);
// 0x0000024E System.Void XNode.Examples.RuntimeMathNodes.UGUIContextMenu::.ctor()
extern void UGUIContextMenu__ctor_m0C738118D07DEC5AB10678AA709A180EDC1D126E (void);
// 0x0000024F System.Void XNode.Examples.RuntimeMathNodes.UGUIPort::Start()
extern void UGUIPort_Start_m16A46CA9E227A0FA5A0E1C792845FECD76320CC8 (void);
// 0x00000250 System.Void XNode.Examples.RuntimeMathNodes.UGUIPort::Reset()
extern void UGUIPort_Reset_mFC4D288DC3B6328010EA302790BF40104FF688D9 (void);
// 0x00000251 System.Void XNode.Examples.RuntimeMathNodes.UGUIPort::OnDestroy()
extern void UGUIPort_OnDestroy_mA3B5DE56603381657E8F11190E81613F506216A4 (void);
// 0x00000252 System.Void XNode.Examples.RuntimeMathNodes.UGUIPort::UpdateConnectionTransforms()
extern void UGUIPort_UpdateConnectionTransforms_m93C7B78D7186733D9E905F96B65DC327C6A2033B (void);
// 0x00000253 System.Void XNode.Examples.RuntimeMathNodes.UGUIPort::AddConnection()
extern void UGUIPort_AddConnection_m377FF09BC188462247A157B3E824D2AF4775CA0F (void);
// 0x00000254 System.Void XNode.Examples.RuntimeMathNodes.UGUIPort::OnBeginDrag(UnityEngine.EventSystems.PointerEventData)
extern void UGUIPort_OnBeginDrag_m0497CE2770B738F135D0093A6110701F725E2690 (void);
// 0x00000255 System.Void XNode.Examples.RuntimeMathNodes.UGUIPort::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern void UGUIPort_OnDrag_mB2454F027A0BA53A5A9775618296F662596BA946 (void);
// 0x00000256 System.Void XNode.Examples.RuntimeMathNodes.UGUIPort::OnEndDrag(UnityEngine.EventSystems.PointerEventData)
extern void UGUIPort_OnEndDrag_mCFCCD3057FC385B8A42CCA24D52AA8F444DA0949 (void);
// 0x00000257 XNode.Examples.RuntimeMathNodes.UGUIPort XNode.Examples.RuntimeMathNodes.UGUIPort::FindPortInStack(System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void UGUIPort_FindPortInStack_mBF5E071151838E7B315E56D2EE6BA952073CA28F (void);
// 0x00000258 System.Void XNode.Examples.RuntimeMathNodes.UGUIPort::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void UGUIPort_OnPointerEnter_m794955F3173C621EB4C0872E891FBF973B9EC4B9 (void);
// 0x00000259 System.Void XNode.Examples.RuntimeMathNodes.UGUIPort::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void UGUIPort_OnPointerExit_m122D9372F12459338529164974AF3284C7268B9E (void);
// 0x0000025A System.Void XNode.Examples.RuntimeMathNodes.UGUIPort::.ctor()
extern void UGUIPort__ctor_m05B9EB0AC157ECF11A7E13A0F2BD22F6B7CD1F53 (void);
// 0x0000025B System.Void XNode.Examples.RuntimeMathNodes.UGUITooltip::Awake()
extern void UGUITooltip_Awake_m009D407FB9688A335943BF417611C1B98722070B (void);
// 0x0000025C System.Void XNode.Examples.RuntimeMathNodes.UGUITooltip::Start()
extern void UGUITooltip_Start_mE587ECE5B94354E0D7198C228D9DB141EA327029 (void);
// 0x0000025D System.Void XNode.Examples.RuntimeMathNodes.UGUITooltip::Update()
extern void UGUITooltip_Update_m60F17384E60DDCB53F4990F41DCAC22D7F3FA0B8 (void);
// 0x0000025E System.Void XNode.Examples.RuntimeMathNodes.UGUITooltip::Show()
extern void UGUITooltip_Show_mC452CE990806140D452CA0B1E60F21571F52392B (void);
// 0x0000025F System.Void XNode.Examples.RuntimeMathNodes.UGUITooltip::Hide()
extern void UGUITooltip_Hide_mB3965C14152968E2A1BB6378A163CD29FB7F14E1 (void);
// 0x00000260 System.Void XNode.Examples.RuntimeMathNodes.UGUITooltip::UpdatePosition()
extern void UGUITooltip_UpdatePosition_mF1611863E3F063990CB26F6AD8C6E0093C09D18E (void);
// 0x00000261 System.Void XNode.Examples.RuntimeMathNodes.UGUITooltip::.ctor()
extern void UGUITooltip__ctor_mA31943CA10B3DFF93ECA9AAE73E23EC50FCF8664 (void);
// 0x00000262 System.Object XNode.Examples.MathNodes.DisplayValue::GetValue()
extern void DisplayValue_GetValue_m821DEC54FA25E0DC1E29449D00EC6E84F1634A9F (void);
// 0x00000263 System.Void XNode.Examples.MathNodes.DisplayValue::.ctor()
extern void DisplayValue__ctor_m67F955616D3F180414CE5BF952CF919791EDEE37 (void);
// 0x00000264 System.Void XNode.Examples.MathNodes.DisplayValue/Anything::.ctor()
extern void Anything__ctor_mA360F49DFBA462EA29AC621068299A0FAB092615 (void);
// 0x00000265 System.Object XNode.Examples.MathNodes.MathNode::GetValue(XNode.NodePort)
extern void MathNode_GetValue_m6D83304B019EB129B4AA2AE722EFE1CBCD7B12DC (void);
// 0x00000266 System.Void XNode.Examples.MathNodes.MathNode::.ctor()
extern void MathNode__ctor_m7E0515C0027A4C5C5197160F3445EFF626CDD535 (void);
// 0x00000267 System.Object XNode.Examples.MathNodes.Vector::GetValue(XNode.NodePort)
extern void Vector_GetValue_m80A517E30C6CACB1A26481BF8ACE27AB86DCC14B (void);
// 0x00000268 System.Void XNode.Examples.MathNodes.Vector::.ctor()
extern void Vector__ctor_mDF3D9A96569F08C79CC5623BB9A1117637FF866C (void);
// 0x00000269 System.Void XNode.Examples.LogicToy.ITimerTick::Tick(System.Single)
// 0x0000026A System.Void XNode.Examples.LogicToy.LogicGraph::.ctor()
extern void LogicGraph__ctor_mAB6F8B727E2038C38130E7528D398552A709FC2E (void);
// 0x0000026B System.Boolean XNode.Examples.LogicToy.AndNode::get_led()
extern void AndNode_get_led_m07058D48B07684BEBF2D14C8933FE7E0D8B9B3DE (void);
// 0x0000026C System.Void XNode.Examples.LogicToy.AndNode::OnInputChanged()
extern void AndNode_OnInputChanged_m1EE0058B9F052F55885C2026D1E8C9EE22B25BBF (void);
// 0x0000026D System.Object XNode.Examples.LogicToy.AndNode::GetValue(XNode.NodePort)
extern void AndNode_GetValue_mF4ACC4AF67BEEE839673FB403938FBEE3775709C (void);
// 0x0000026E System.Void XNode.Examples.LogicToy.AndNode::.ctor()
extern void AndNode__ctor_mD884FBF670927323BBAF70872D6E2E8CF76EEAC6 (void);
// 0x0000026F System.Void XNode.Examples.LogicToy.AndNode/<>c::.cctor()
extern void U3CU3Ec__cctor_m43CB6CE6E61263B6925CD794A608A9FB7D2EF22C (void);
// 0x00000270 System.Void XNode.Examples.LogicToy.AndNode/<>c::.ctor()
extern void U3CU3Ec__ctor_m505A5863A0F2D60CBD1EA8266CA1DEF45BAD6402 (void);
// 0x00000271 System.Boolean XNode.Examples.LogicToy.AndNode/<>c::<OnInputChanged>b__4_0(System.Boolean)
extern void U3CU3Ec_U3COnInputChangedU3Eb__4_0_mC0473403A4ECC84E2059EB11636842525924C1C4 (void);
// 0x00000272 System.Boolean XNode.Examples.LogicToy.LogicNode::get_led()
// 0x00000273 System.Void XNode.Examples.LogicToy.LogicNode::SendSignal(XNode.NodePort)
extern void LogicNode_SendSignal_m3388469DD1139C817C13CF4C1BDD50ABC4B09656 (void);
// 0x00000274 System.Void XNode.Examples.LogicToy.LogicNode::OnInputChanged()
// 0x00000275 System.Void XNode.Examples.LogicToy.LogicNode::OnCreateConnection(XNode.NodePort,XNode.NodePort)
extern void LogicNode_OnCreateConnection_m396ACAE1EA68D7C16AE811A4F35F52C3F27A59EA (void);
// 0x00000276 System.Void XNode.Examples.LogicToy.LogicNode::.ctor()
extern void LogicNode__ctor_m36EB60F398F9141795EB8DD413874B7BD01785DE (void);
// 0x00000277 System.Boolean XNode.Examples.LogicToy.NotNode::get_led()
extern void NotNode_get_led_mCEC980A6562BB36D49D932437B8B62EE0CDC12A7 (void);
// 0x00000278 System.Void XNode.Examples.LogicToy.NotNode::OnInputChanged()
extern void NotNode_OnInputChanged_m2C326BAF912DF9D7285F29BA042A4DFB0C5850E4 (void);
// 0x00000279 System.Object XNode.Examples.LogicToy.NotNode::GetValue(XNode.NodePort)
extern void NotNode_GetValue_m1BF31296D764FDEC869EDFC3C5AEBE0A7CFEBFDE (void);
// 0x0000027A System.Void XNode.Examples.LogicToy.NotNode::.ctor()
extern void NotNode__ctor_m06B5A6182F54943E9839E47FE00A1C656CC7FA6C (void);
// 0x0000027B System.Void XNode.Examples.LogicToy.NotNode/<>c::.cctor()
extern void U3CU3Ec__cctor_mEA75EA0EC633FAE25B9C1735436BFFD2BC620D59 (void);
// 0x0000027C System.Void XNode.Examples.LogicToy.NotNode/<>c::.ctor()
extern void U3CU3Ec__ctor_m93BABCF9975FF7706553E79DDFBDA77FF1887827 (void);
// 0x0000027D System.Boolean XNode.Examples.LogicToy.NotNode/<>c::<OnInputChanged>b__4_0(System.Boolean)
extern void U3CU3Ec_U3COnInputChangedU3Eb__4_0_mD40C965A34673FF685383249FB878346A5F3ED89 (void);
// 0x0000027E System.Boolean XNode.Examples.LogicToy.PulseNode::get_led()
extern void PulseNode_get_led_mBC363F5F80159ED336A4D85600670C448E6E5F14 (void);
// 0x0000027F System.Void XNode.Examples.LogicToy.PulseNode::Tick(System.Single)
extern void PulseNode_Tick_m51D3DEBCE38B48D1C19541BBF5E6AE017AFBBA66 (void);
// 0x00000280 System.Void XNode.Examples.LogicToy.PulseNode::OnInputChanged()
extern void PulseNode_OnInputChanged_m8C94E1360CBBF65996C46E157F77F4D957FF6EF5 (void);
// 0x00000281 System.Object XNode.Examples.LogicToy.PulseNode::GetValue(XNode.NodePort)
extern void PulseNode_GetValue_m0ADF405D9AB631B157886DEAD787D2971BF6E8B5 (void);
// 0x00000282 System.Void XNode.Examples.LogicToy.PulseNode::.ctor()
extern void PulseNode__ctor_m4837A62F6A63AD2240009F45463D56AC1A560ECE (void);
// 0x00000283 System.Boolean XNode.Examples.LogicToy.ToggleNode::get_led()
extern void ToggleNode_get_led_m9B094C6B485A6ECD535230E4EA5C95F657078597 (void);
// 0x00000284 System.Void XNode.Examples.LogicToy.ToggleNode::OnInputChanged()
extern void ToggleNode_OnInputChanged_mBC94FB0893E6995296C3AC06830FD33A7BF3B4B7 (void);
// 0x00000285 System.Object XNode.Examples.LogicToy.ToggleNode::GetValue(XNode.NodePort)
extern void ToggleNode_GetValue_mC5B0977F82799E7E7881F7B60787C421A643B649 (void);
// 0x00000286 System.Void XNode.Examples.LogicToy.ToggleNode::.ctor()
extern void ToggleNode__ctor_m1077F1C1168F9947181F8CF087605EBC7E3CAA21 (void);
// 0x00000287 System.Void XNode.Examples.LogicToy.ToggleNode/<>c::.cctor()
extern void U3CU3Ec__cctor_m5302482C728DC301D359F3CF3F07B45A3EB94074 (void);
// 0x00000288 System.Void XNode.Examples.LogicToy.ToggleNode/<>c::.ctor()
extern void U3CU3Ec__ctor_m3EF3B40085A477D5DDC42C0160AE3EEBA71EAB18 (void);
// 0x00000289 System.Boolean XNode.Examples.LogicToy.ToggleNode/<>c::<OnInputChanged>b__4_0(System.Boolean)
extern void U3CU3Ec_U3COnInputChangedU3Eb__4_0_m6252E0DE67AAF790D8D3C21174A106E66D64B1B2 (void);
static Il2CppMethodPointer s_methodPointers[649] = 
{
	Input_master_get_asset_mB578FB991F84D0700B5FA9B4B34C7048BE5637EF,
	Input_master__ctor_m05570375675155D2756C12748D40E3A55E995118,
	Input_master_Dispose_m4E1AB80951FBD83DE2BBD7152B250AD5B62DD907,
	Input_master_get_bindingMask_mA351518215DD2A46E0AC3B43C4F21E07FF4D6DAB,
	Input_master_set_bindingMask_m47CCB6C56E9964B4F39B0C3D83AD6FBFBACCD5DA,
	Input_master_get_devices_mC144889E948D43D5E0BE4762C1897D94CACAEDE8,
	Input_master_set_devices_mCE7B8BD9B4FA936178C0A7A5012C01D6227BD8C9,
	Input_master_get_controlSchemes_mA89274C4467E476A0A54E36E5AE827B527453038,
	Input_master_Contains_m3B11EA2D2A14801BD2ECD5CE5FDF548B82EFFCC3,
	Input_master_GetEnumerator_m576439B219C6489068DF54CD3A888D911AF81BD4,
	Input_master_System_Collections_IEnumerable_GetEnumerator_m5C5C020C36FECC05CBA84F4112857A516B18059C,
	Input_master_Enable_mAA5A4A5A3B5048D37FE1BE9BE3E654439F40771D,
	Input_master_Disable_m375CAC17441BA7AEFDE01B73AC869E9974E00A6A,
	Input_master_get_Player_mC7F6E1D47AB6D2D60FC4C5397098EB159F4660B9,
	Input_master_get_UI_m7F8ED2828F6A385587139ED712AF2780A3E7D422,
	PlayerActions__ctor_m7481EA18F112C70293D3474392F06706C811E621,
	PlayerActions_get_Movement_mEA5BB53310B50C0F518A9F4F610423C3905E9207,
	PlayerActions_get_Interaction_m77608CD4C8D9A2A8B508BD745FAFA1622C5835C7,
	PlayerActions_get_Escape_mB334771D35D03DCB58855F20B5A151BEB95681DD,
	PlayerActions_get_LeftMouse_m5E5CAEA8DAC2A5065B4890005B37F0A63B2F8A4D,
	PlayerActions_get_LeftMouseReleash_m8B39D61E60E15BCD618823B459D8B35D0C729B29,
	PlayerActions_get_RightMouse_mC71D836809242B2B2931CDED4D47B659DB5CD223,
	PlayerActions_get_MousePosition_m12C40F11BB2A48AA6F4CBEAC2307BE06F6CADC26,
	PlayerActions_get_X_mC015291F0DD75D6C781276BADA2E2DE4325531BA,
	PlayerActions_get_C_mF777A74F1B446CDD7CE1FD7E151C89781B115492,
	PlayerActions_get_P_m04B5F31DBC4699181C495E9BC290D5FBC292C0EA,
	PlayerActions_get_O_mB7AF304DB8C29291B5D5A622F1D5898A370972FF,
	PlayerActions_get_ForwardDialogue_mE207E1B693F9DE4F991D8709CD8DD5D2B85BE6C2,
	PlayerActions_get_Tab_m338B414903369841F681082F90CD34C17E3CA12B,
	PlayerActions_get_Debugging_mE0B8A7A3F30DF25EFED5C27CD4F1088F0377456E,
	PlayerActions_Get_m9E302E68280071685925C8B03873ED42656775A8,
	PlayerActions_Enable_m00A9A57EDAC1A58DFB03DC10C4E266AC921B552C,
	PlayerActions_Disable_mB03301453FDA738D343E68B47B67493E74C6FEA9,
	PlayerActions_get_enabled_m0C8FBC0086D06E55C6C2FDCFBC7C53C1A06F15B7,
	PlayerActions_op_Implicit_m00773CF39CF596BDE1B46506EC4558388936D99B,
	PlayerActions_SetCallbacks_m0CAD45BB19CA2B72DB0BA6C127A5137CB5D458E7,
	UIActions__ctor_m944934E74521224F76E9F1FB5C83CB23EDC1D41B,
	UIActions_get_Navigate_m6C827DD1D55866AAB36A4E59CC372409621AC711,
	UIActions_Get_m65A7C87B82A96CBAD0B2662044414AB697A0266A,
	UIActions_Enable_m49364651EAEA42FB35D42CC6AE6E4305195295BA,
	UIActions_Disable_m90FE4E19AC2051688F7FE7D3F593BBCDEB65E913,
	UIActions_get_enabled_m19088D7D7D446891B6DD16C542B70E24A65CD4E8,
	UIActions_op_Implicit_mAFB6947101E88F51845A740D582B517DC13E86D2,
	UIActions_SetCallbacks_mF059380A033D1DCCBDEEF8FA931A92C7AFABEE2C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PlayerInputController_get_Instance_m1B5AA8013FFB4BA31511DB22E772E66D5738C01D,
	PlayerInputController_Awake_m334C96DC775AE5059ED1D52D3EA44ABDE69A8425,
	PlayerInputController_OnEnable_m50D74F6E26D85540E0A7E2EF2E44C19E7A98F763,
	PlayerInputController_OnDisable_m70AD87038071D9611A9C80FC2A6D4D600E8D140A,
	PlayerInputController_get_GetMovementValue_mEDEE3A0D2F095148251748F65A4BC79C4035B53F,
	PlayerInputController_GetMovement_m153DFF309E5A13FB63ACE059FE575356639E2782,
	PlayerInputController_get_MousePosition_m497FF44822EC8B4CAD48123AD8492ABBA8E5DC0F,
	PlayerInputController_LeftMouseClick_mFD99D5C6E414C3F2A62D92F4E85A960870F78B09,
	PlayerInputController_get_LeftMouseValue_m6D27F378E8D2CCEC15A703B73960861DAA6E9F28,
	PlayerInputController_get_LeftMouseReleashValue_mCA3DBAEDF421276E6AFD9611AC2C8D582846B273,
	PlayerInputController_Interact_m36E4EE7FF96BF81BAF51657427E2E2A91D83B064,
	PlayerInputController_SkipDialogue_mFAADE025BBF7457E39CE420D29FCF70F4C725B7C,
	PlayerInputController_Tab_mCFD7D19A1C1C2A455D17E3588D72333C089CC4DB,
	PlayerInputController_Escape_mC59CA988739B661ADC08977297BDAEE9A65014EA,
	PlayerInputController_Debugging_mAE6DBE0F9995592114EAF09BAE49AD5C2D72E83C,
	PlayerInputController__ctor_m4D6570072769159A1458FB4FB7CE42C3B82F4B81,
	DebuggingMenu_Start_mD454939A17A47E827ADC4D1EC50FFEBA9D6ED2FA,
	DebuggingMenu_Update_m66B7B618510B314720C4B84251891C3C9C121008,
	DebuggingMenu_OpenCloseMenuTab_m17BE4FA32A4E227AD0B15D87642098C6632DABE5,
	DebuggingMenu_CheckPlayerInformation_mA2B5DB4C7DF407CE00D472E7C8AF468B04C83A2F,
	DebuggingMenu_UpdateText_m5F172B900146036AF02FA22936569BD57D96AB8C,
	DebuggingMenu_IncreaseStat_mB1BA13A5B9995403442FB5B7F9EE26A5C769D1E0,
	DebuggingMenu_DecreaseStat_m98937ADC8C8B359D9C330FBC961B90F0A1BC188C,
	DebuggingMenu_OpenCloseSubTab_m6B092E0D4400844AD1EA438FBD8AC06E31011515,
	DebuggingMenu_CloseDebugMenu_m8CDBDAB32274BD0FACABEE4B2CC0FDD3E3506D33,
	DebuggingMenu_DebugRefillHealth_m7DEA276C28B1E09EF2B7C5CD51A2470907D8DE0A,
	DebuggingMenu_DebugResetStat_mE916784AB1693886E9A5C15E6054064D5AC62462,
	DebuggingMenu_DebugResetLevel_m84AB0A33CD01D14DB7B1C8DA42DA5CB632FF46B1,
	DebuggingMenu_DebugSetEnemyHPTo10_m3E4C54F84B452AA6CDC898268B86A154EB191BA7,
	DebuggingMenu__ctor_m57C26B55C2816EC65DC8F51EFEB44998E1C25D74,
	U3CU3Ec__DisplayClass26_0__ctor_m1BF762E0CA54B1731560D5297FF8B4717C409AC0,
	U3CU3Ec__DisplayClass26_0_U3CStartU3Eb__0_mA6D6329F4AF68DDE8291B415799548F539C3EFCA,
	U3CU3Ec__DisplayClass26_1__ctor_m7595C240A99180A8BF1D4EB9CF0974EB43DDA9D5,
	U3CU3Ec__DisplayClass26_1_U3CStartU3Eb__1_mFDCE13723B36FD230536AA2B8DEC4886446DCDED,
	DialogueChoice_OnClickUI_m1F9676DB3A743A1780B047F1E26C683D5139A312,
	DialogueChoice_OnCancelledUI_m3CDD4170785804A87502C4B89196BE4887B57024,
	DialogueChoice_OnHoverUI_m92C1C92E94F000D932746CD536DF652F736D97BC,
	DialogueChoice__ctor_m50CC4EE08F1A7F2C75AD0DDA41F00278F50760F4,
	NarrationCharacter_get_characterSprite_m8FD221AC65F3655C7F67B9BEA0264CF87617D9AB,
	NarrationCharacter_get_characterName_mD994EBC69F8DDC1ABB13EFFA36F57025063E6917,
	NarrationCharacter_ChangePlayerNarration_mF41EFC540EB3B7B1FF89B98EBE98E3509AFA22C5,
	NarrationCharacter__ctor_m4F3F981D43B57D24B0CC88DE448BDD4AFA8B5891,
	NarrationLine_get_speaker_m0430AE37C9502172CE5164D138D5DE76047F6C31,
	NarrationLine_get_text_m3A93671496008C6B99092A228F4F62AFDBCA6C71,
	NarrationLine__ctor_m98F31EB8B60C24586A2D7DFE6CB3CF528AB1ADFB,
	BaseNode_GetString_m64FC1E63D9CBE2DDD0829134F49D215623A528F4,
	BaseNode_GetStringArray_m85BE362542A58E253DF85F30C57B297873368F51,
	BaseNode_GetSprite_mBF37E86C6ACCBFED8F329E3CF54A0751A384E9FE,
	BaseNode_GetInt_m6EDF5180A2BE10C7187F47EC8834B79963BB8B8A,
	BaseNode_GetUnitProfile_m158C0FB7003FA6C9ACB8A51ECAF4DA89094CF327,
	BaseNode__ctor_mD6F065C5F0E972AFF7002AF9370B528C74CE96E2,
	BattleCutIn_GetString_mFAEB2E3D1BA2A4B1686EF68AD3C3C5FA18F5EDFA,
	BattleCutIn__ctor_m92A85EE28296A2FC2D31C528514EC57E4677D53D,
	BattleNode_GetString_m968C6F2A68030D9F1DECEAEBB33907F72CBB0970,
	BattleNode_GetUnitProfile_m4A230B5604191FCDC4491F9451B0A68F3A9A2056,
	BattleNode__ctor_mA2ED470E48C006CB7F5DC5DB4763C4E3CB9DCF4E,
	ChoiceNode_GetString_m5E4478EE466BD0DF9CD307095D2FD2D9DF10180D,
	ChoiceNode_GetStringArray_m15A3B5FE3F2D9ECC3BA99225B2024A93AF4B726F,
	ChoiceNode_GetSprite_m425F46293512CCC35FBC6AF66CF9B143197CC757,
	ChoiceNode_GetInt_mB702F05D34FC998C842C915722CD5D9F06A100A0,
	ChoiceNode__ctor_m1EFBC99899BDE6BD20E11BA41D871B05760EE57F,
	DialogueGraph__ctor_m8D2152FF1CB552124EFD6592BB66EE0585E7AA7F,
	DialogueNode_GetString_mA40ECA92C7C76D086A9B776FC306A34A916F6483,
	DialogueNode_GetSprite_m8BB6A6309D09A33D67CC2D0F675E712581EAD533,
	DialogueNode_GetInt_m307E06D26944654B04B0F3596073AE2E4FE8D152,
	DialogueNode__ctor_m2DBC6A16614EE3B3D7A7A84AAFB09DFDD5EB63DA,
	EndNode_GetString_mCD865F51677747DEDCF1A8E55C8AB9A97197554E,
	EndNode__ctor_mC991B7FCF10E8DC0B3B59F1FA8BF5DA3A8D46885,
	NodeReader_get_Instance_m79F918993194F446ADC18D96BEAB9F7BD14DA988,
	NodeReader_Awake_m160313717EC13BDE138A6C6C04270EAC121CCC90,
	NodeReader_Start_m74A53C7E51328EE7119612A5BC233AAD3BE22417,
	NodeReader_Update_m92329D9A3B8DAE21F78507C16FF6B599359A6F7B,
	NodeReader_StartDialogue_mFDC3B702A57B37F9E2C734F0633E4F3C9C23232F,
	NodeReader_CustomStartDialogue_mE1DDE55BB452FB6FB5F59E071977CDD4932A556E,
	NodeReader_ParserNode_m3EAB881B5ECB17F0B0BDD84C5CA93EEFF15B1E30,
	NodeReader_CustomDialogue_mDA2FFD006849C2851B90F8E19927B5E4E409D418,
	NodeReader_ParseChoice_mE64E48CC0F78A2DE7D4257483A4347453929C73D,
	NodeReader_NextNode_m43243890BFBB821826CCB331C2E51F2D0C37D89B,
	NodeReader_RunningDialogueButton_m7D87247FFFB4C18089B2E8236D702A12BAEBA269,
	NodeReader_RunningDialogue_m4C92C28AE90FEE3D502AD927CA208A29A6631DC5,
	NodeReader_SkipDialogueButton_m6F28D80E6334E9AA67EED744C8A00390976EE0C6,
	NodeReader_SkipDialogue_m497A40DAFFD871C7DE21300657A121B2F4056290,
	NodeReader_TypeSentence_mE846AFE4DD1D8B4E9B5E084E82823DC410D03762,
	NodeReader_EndNode_mFFBA7423C5E0F27395EAAB5301FB80B8B1980F6A,
	NodeReader_BattleCutIn_m4D9BCA0392E3B7698EFEB7A3B47C662D06C7A217,
	NodeReader_BattleNode_m5E11E8AAF6DF063C0046E68DF944BFDB8170C576,
	NodeReader_ChooseChoice_m8008E29FE55B22969C469DD413BF3103FC428DF9,
	NodeReader_get_CheckDialogueIsPlaying_mCAEF25BB9ECF4DA1723C34A84BEBEECB8E98D0D1,
	NodeReader_StartTypingSound_mF8FAD347562AC7A7E4DE41388DDC525803D98866,
	NodeReader__ctor_m4A93B76E13004A46F2923C82F037E13025CAB001,
	NodeReader_U3CParseChoiceU3Eb__32_0_mE9CB54451F924F79F3AC08623838E478D66D7A2C,
	NodeReader_U3CParseChoiceU3Eb__32_1_m967C24CA1384A386651E4401E8B1E3C850BBF394,
	U3CParserNodeU3Ed__30__ctor_m382DAC51BDF1174D3212B4AF0B0D9DDDD122A329,
	U3CParserNodeU3Ed__30_System_IDisposable_Dispose_m14EB0FDDF590E4BDFF1FD8AC581D9AE22B70729C,
	U3CParserNodeU3Ed__30_MoveNext_mF51EE850ABD4015241300EDF6CFC08FC90DE6CC1,
	U3CParserNodeU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE06E05FE75F35AB048964B3D36B60A3B74A7F33,
	U3CParserNodeU3Ed__30_System_Collections_IEnumerator_Reset_m0CDB444124BB417931E15B820C3C9D9ABE915800,
	U3CParserNodeU3Ed__30_System_Collections_IEnumerator_get_Current_m5599AFCFFBBF462A44F42C2F1DAA399D2082E1AB,
	U3CCustomDialogueU3Ed__31__ctor_m539D0D53D0C9852C2AA92851832BBA1F93FE5215,
	U3CCustomDialogueU3Ed__31_System_IDisposable_Dispose_m05AB132BB1C96C406CB4E6FB832AA9310CAF9159,
	U3CCustomDialogueU3Ed__31_MoveNext_m0FA30F52AE3CAB4A7D1E775F39C388780CC4A919,
	U3CCustomDialogueU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB652CBA767F5E7D4BF9661F5D60318DA0C3562DF,
	U3CCustomDialogueU3Ed__31_System_Collections_IEnumerator_Reset_m174F4229540442B55B3F7CCCDD051E832E908526,
	U3CCustomDialogueU3Ed__31_System_Collections_IEnumerator_get_Current_m039BFABC311EAD1031D0A8D7D1E90260AE943B8C,
	U3CU3Ec__DisplayClass32_0__ctor_m7BA21C65BDC489FACCEE4EA5B9B7D4AE5A5BC63B,
	U3CU3Ec__DisplayClass32_0_U3CParseChoiceU3Eb__2_mF660B8B93FEDF189BF6CC164D6895CA6068151E3,
	U3CParseChoiceU3Ed__32__ctor_m8CD7150757546CD2F3182C33BBAE91519AAB9000,
	U3CParseChoiceU3Ed__32_System_IDisposable_Dispose_mC62B6A94C7B7FEAA59D9B2BF082E33A1E28A1293,
	U3CParseChoiceU3Ed__32_MoveNext_m64C4B15D5CF4BA1BAD07F532BB3C423168B1F42B,
	U3CParseChoiceU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F3C38F7EFC22E10D93A71F5136ABE6343D8E287,
	U3CParseChoiceU3Ed__32_System_Collections_IEnumerator_Reset_m45250654797E7892C0D72ECDBDF79A12C230ABBF,
	U3CParseChoiceU3Ed__32_System_Collections_IEnumerator_get_Current_m34989581633799DDA06EE8408DA1FF9B977EED46,
	U3CTypeSentenceU3Ed__38__ctor_mD2CE5896F92C199BFA6B203DC27462B78C8D2FBB,
	U3CTypeSentenceU3Ed__38_System_IDisposable_Dispose_m3B08D4B564EB130E4D1160752C31C799AEBC00FB,
	U3CTypeSentenceU3Ed__38_MoveNext_m7761734731ABA6F19EEF515CBC3F0FFE960B7F9D,
	U3CTypeSentenceU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C44140AB27A4C7204C12F1564FC47CCFDAC8C69,
	U3CTypeSentenceU3Ed__38_System_Collections_IEnumerator_Reset_mA7053DC759C764A5E1E71EF04AED17866CA1575B,
	U3CTypeSentenceU3Ed__38_System_Collections_IEnumerator_get_Current_mE7624AC3A9AFB6B227EC4D69375762C83BB9FB64,
	StartNode_GetString_m7FFF9CCFF277A74935E7D4D72CED41694E9557D0,
	StartNode__ctor_mAD3D015ED7EA0249B4E522D686960203F6C6F2EA,
	GameManager_get_Instance_m758C0D98BBDEF0C2DCA163A5D60B22DBB5B07232,
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	NPC_Start_m20C2D17950CB234A91905C91CCEADA0870B913B9,
	NPC_Update_m385369B628E634D1BB4300956637169CBAFDDEAE,
	NPC_NPCState_m16FD7EE3093100F535540A8D62DE0664FB09603E,
	NPC_NPCNotificationBox_mEE0CE12E183C3233097308141C84936C509FFE20,
	NPC_Interaction_mBE822D12AE7FA329CB3C794B96F6D97EE0D974D3,
	NPC_PlayDialogue_m10A92A7DBBC4B6AE9AA9AD99A2549CDAD91391B6,
	NPC_MouseHovering_m8EEAD6599CBDCF66D9AA14CFBA343588F64F4CEA,
	NPC_MouseNotHovering_m3EB10E45423C5FA934E23800BC687B9EFBFAC7FD,
	NPC_ChangeProfile_mD759886C03028BEFE7802EF35C12FB4967DF5DAD,
	NPC__ctor_mC9AA4F3CBCBADCB1866559D68B31E231A879CACE,
	NPCTriggerCollider_Start_mC1DE92FF508FAAB938EB0E099635F237D7226294,
	NPCTriggerCollider_OnTriggerEnter2D_m7837A332B4E6F283D2BB5C70E0B78E2500A0E5FF,
	NPCTriggerCollider_OnTriggerExit2D_m6727010F3047E3B29869368708B34C31B248B5A4,
	NPCTriggerCollider__ctor_m4F1029058C03705CAE09423FD4524F46183FDFF8,
	ObjectiveInfoSO__ctor_m58026E32811CDA4D4DF6BF945C2B44D5345742E4,
	ObjectiveManager_StartObjective_m32FA1274D3D8175E652A28A67A05080BABC93329,
	ObjectiveManager__ctor_mB2CC8A6A9649D74D24754F48287BC90A8B839F63,
	PlayerObjective_AcceptingObjective_mA91A40E21D26A39B77DCBF346164B18FDC453BCD,
	PlayerObjective__ctor_m07D6C86D9A6E753D4DB5D5EC3CCAD4532742976E,
	RuntimeObjective__ctor_m394F248519C3B8ED3B5DBE076D431BED88F11E4D,
	NULL,
	RuntimeObjectiveTracker__ctor_mE96643297DC6452D03C85C39D339C733B0B10091,
	PlayerBrain_get_Instance_m9533B6327CD58859793AD9B47C22C51F37B9832F,
	PlayerBrain_Awake_m6408C44E8C560FDF2213D3821DE4157BF9225882,
	PlayerBrain__ctor_m8CAD32EA09F6ED25BF02F112D99F604DA309AA23,
	PlayerController_get_Instance_m8C373614456176CE37B49E24BA9B7E019C5A0440,
	PlayerController_Awake_m23059564DCFDD324EA9DB966473251896647CD23,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_Movement_m417CCA59FA36C6719013DF91B010673B5F7277F1,
	PlayerController_CanMove_m18B797FBC598549FE78766BD4912DC152A595241,
	PlayerController_PlayOneShot_m5C3AF5FEC890F309670510ED154A065A29480820,
	PlayerController_PlayWalkingSound_mBAEC5AD60DAED60971DAD54F795DF458D8BBEB77,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	PlayerModel_Start_m12F050E122E9E36024B7A6DF0D5B7E7FB82BD5FB,
	PlayerModel__ctor_m251F2D079FCA1F01DE72142D041B9BE652411BD6,
	PlayerMouseDetection_get_Instance_mDE07DF7A7DBBBAAB2AAFAABE5018583617B4AACB,
	PlayerMouseDetection_Awake_mA5B035F2C10AAD3AC3A1C3F576FAB78566FE04F0,
	PlayerMouseDetection_Update_m9A5A2213DFBEBC61503A3984D551A3F4CFD7740F,
	PlayerMouseDetection_checkifPressOnObjectBackUp_mD333437F81231A3337157CC52C52B5B46AB3CC40,
	PlayerMouseDetection_CheckMouseOnObject_m0DDB07EE04416C2DCF706DAAB4518CB45F1CE1D2,
	PlayerMouseDetection__ctor_m345244A82751DB65E3D1ED67DB23AB4766750043,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	AttackProfile_get_attackName_mC97A9FE870418F6C3362D62800732D804AF3EA6C,
	AttackProfile_get_attackDescription_m93DDDD8DF15875C93EA592647606DED08BA8F2D4,
	AttackProfile_get_attackIcon_mFE491EBE2F25AB0CBB6AAC783F0613F0E29AEF03,
	AttackProfile_get_attackAnim_mD579E33A1B52FFDBB8578F6AD437DB2BA72E3F70,
	AttackProfile_get_staminaCost_m91A47A6E567713F29BFA474EF43A2137F1D670D3,
	AttackProfile_get_ideaCost_mFB9C30CD4F58F45A1144A9625362BA28616FE038,
	AttackProfile_get_logicCost_m6D196B2FE9E3EAE42A4CC8CE6B483B3DEE4A6447,
	AttackProfile_get_damage_m1E71A63DEA10998B8447BD2FB1784FDFAF48F5A1,
	AttackProfile_get_critRate_mC1439D190AA3B686B64F7B878AD2509F15DE2310,
	AttackProfile_get_heal_mBB7EBA1FCD6D52A9F125AFBBF8AA0F5F2D15FBEC,
	AttackProfile_get_isDesignSkill_m3889BC71D3542F76BBBC62D565D4A47FC3BCB7A0,
	AttackProfile_get_isCodeSkill_mF5E38048ED6FAE858EF31620162E2DC7A1FB2470,
	AttackProfile_get_healRate_mAD9546C4E074D779488EA00EE60A1DBFF8FEE151,
	AttackProfile_get_healAmount_mC089C99644BE15D2EC0101D9FDCCF3613AF29D8F,
	AttackProfile__ctor_m7A187871CFA9B24459BB51C0D8B126B41466981A,
	UnitProfile_get_unitName_m53905CB500D1E07809ADCAB0271090DFAD182CAE,
	UnitProfile_get_unitSprite_mDAC5B1B56F0EC079C7AB0434EA89C60C21D79BB0,
	UnitProfile_get_stat_mCC53416A80E5B4D2498BD1F25846900793BE22A2,
	UnitProfile_get_attackProfile_mF74B7109FAEA6809DA5B7D5921F7C3FCC6704D68,
	UnitProfile_get_skillProfile_m81873147F6B3EA0A6ABBF5FABA56D2F794220B8D,
	UnitProfile_get_startingDialogue_mD3D96018A62DBC5FB8F2D7AE4859D9AD1F2B9030,
	UnitProfile_get_introBattle_mA5E48E94582D47BAA684F2AC83F336AF408A88F2,
	UnitProfile_get_reward_m4D6C9B4C79E7E49C51F676498EAD8B442A63DD7D,
	UnitProfile_ResetDefaultState_mAE94449F2BE5746CD6C94528C1BF7D083A4F3B12,
	UnitProfile_ResettingFightingStat_m8ACAC82FA2F0D84722A15EF6F9642A4D72306321,
	UnitProfile_DefaultStartingLevel_m4BFFDA1B009B2A2AFC96C61A77994A98F7A11FE8,
	UnitProfile_RestoreCharacter_mB341D0B068F9C89065FF79BF1FE6378FE0807382,
	UnitProfile_SetNewProfile_mB35E3BA67D46C8BD16A8DD2EE7E1C0D2BC5F9B46,
	UnitProfile_ChangeName_m1DFE7879F051DD211B33B9DF8AB528390063975B,
	UnitProfile_AddNewSkill_mAF952E70E7724E8655C4C74A9DCF8580844E5FC7,
	UnitProfile_ResetSkill_mE32E06E4948BB6C8F9BEC872CB94467350B504C0,
	UnitProfile__ctor_mDD0903476ACFC4AB1BBB6E48DF1BC47184A7E016,
	CharacterStat_GainEXP_m3A38088CA47955D129E82561E2DA3F9416E1384C,
	CharacterStat_LevelingUp_mFBFE6E0D6D81FBFCDFC9C2475C54C1B95CC78573,
	CharacterStat_LevelingStatCheck_m743BF6C8D2C6E3EEE78F98B12C5FF5C44618122E,
	CharacterStat_TakeDamage_m63406F2029F75095CEBFFC90CFA002C4F3081F65,
	CharacterStat_TakeSpecialDamage_m054B0EE229E9B21CD600EA2E6BD552F24A0AA689,
	CharacterStat_get_GetCurrentMaxHP_mCC925038DDDBE548B20160AD52DC8AE96429CF53,
	CharacterStat_get_GetCurrentIdea_mE67A36836A194AD697F022708E5ED3A7EBABC238,
	CharacterStat_get_GetCurrentLogic_mDCB673ABBC41FD04B8487174633FC91D58CEF971,
	CharacterStat_get_GetCurrentAttack_m612A622ACC3FC82C46C082CC27EB903C941D0A8E,
	CharacterStat_get_GetCurrentDefence_mBE06311CE3FF99F492251E61DC06CD13EFB9DE6C,
	CharacterStat__ctor_mE8453187755B24D6B71C4241AB73607BD8C0E8ED,
	Reward_get_rewardEXP_mA003AABC3AEFB472A595FCF17148F2D0F00F6ED8,
	Reward__ctor_mC71832CFC8A6C052EC4051BD5288B600B9587DF8,
	LevelingStat_get_increaseBaseHP_m82B3B4996A1F0103CB82BA4BFC97DA790AFE0AB2,
	LevelingStat_get_increaseBaseIdea_m65C073FE236242E61623F892F9369083B85EE4EA,
	LevelingStat_get_increaseBaseLogic_mE5B57D361629083B4C264AC7D955667BC37A4F6B,
	LevelingStat_get_increaseBaseAtk_mB8C3279B530EBDA97B3D7A6CE870D1780B2ED804,
	LevelingStat_get_increaseBaseDef_m3BFE123B6634480C6A5F9F2E555990A241EF7209,
	LevelingStat_get_skillPoint_mE3A2E3A63211FDB587708907D62EFDDDE2BEFB91,
	LevelingStat__ctor_mAB24A06E211DDE4F98AAB79CE811EFFDDC71C843,
	SoundManager_get_bgmSource_m513EAF47372F98B602C885AD31D38CCDC73D1757,
	SoundManager_get_onClick_m18B7A6DDF53EE9F37F5A62EBFAA82891F0685140,
	SoundManager_get_onCancelled_m857F1443D4263FCB8E6030D1B9091D6FE99D83FA,
	SoundManager_get_onHover_m51D38D885188E4EE9D266ECF340A440F6F19D338,
	SoundManager_Update_mDD188B65FF1E9B1DF1B8345A6290D149E70E657C,
	SoundManager_OnSceneLoaded_m77B3E475EE4B25348C20D0B125F9FF6690057132,
	SoundManager_CheckIfPlaying_m74F7C53D4283A6061B3A3516308D994AF281DC87,
	SoundManager_OnClickUI_m4AAC7AE59343DA3C3B8D08F0230159F3317F5DD3,
	SoundManager_OnCancelledUI_m314B21D5255A36BCFE8E8E7561FECD515DFFA948,
	SoundManager_OnHoverUI_mF2796CAFCC262451D8BF99918CBBF013B0AABA21,
	SoundManager_PlayOneShot_m7117DF0B0DB6E24B88955F905D8039516C647A99,
	SoundManager_PlayWorldBGM_m177F5F008721ABA09ECD4823B8D221661432C4C8,
	SoundManager_PauseWorldBGM_m1C56B7527DBC6C49FE5F0C593B73F54DB0DE4100,
	SoundManager_ResumeWorldBGM_mDDFF7A9C1A853E35FB8E2A8B1EFC94E0FAC5643F,
	SoundManager_ClearWorldBGM_mECB2BE0484A089CA62C7A21AA2D19839AAD1023D,
	SoundManager_PlayBattleBGM_m6E6A443F6B026CFB62F133FD3F0D462601787507,
	SoundManager_PlayRandomBattleFightSFX_m9866DC15D5B51A73F072E193A6F2671F3C4E9CCF,
	SoundManager_PlayRandomHealingSFX_m32886A48C052825CAA816F6E65159042D06B3C98,
	SoundManager_FadeToNewMusic_m92AC93DD3C3A9E8A3D66418DF6CFB092FE9C3A9C,
	SoundManager_TriggerFade_mC01BBBDBA38E40AAE19601091DC596C3FE5D5D86,
	SoundManager__ctor_m30D18BC25871BD3FF7FB195A1CAE50A2EE48FCAE,
	ChangeProfileEvent_Event_m957556E98776D0D09702CE223352E0C89007709B,
	ChangeProfileEvent__ctor_m982CE719834A13D88299A3147A63D76FA9BC428E,
	EventClass_Start_mB77D35247F4FE51053EA69569D0721B4E890BE2F,
	EventClass_Update_m7D43536DE39D104396A1822DCB829832334449D7,
	EventClass_Event_m871008C3DF478FC32B786A7C8A4179EF8E3C62B1,
	EventClass__ctor_mBABE26471514E06A3C30BC76565EF0098844B04C,
	EventTriggerClass_OnTriggerEnter2D_m31036183084267307731C3989654E5FDC70EBFA5,
	EventTriggerClass_Event_m92662E5A39701B3BAA25B555ABFD8A1889A99540,
	EventTriggerClass__ctor_m065F74D52CA7C1A8F60822B76656C4F43BB786D2,
	GettingNewSkillEvent_Start_m597303A6B8327A9577F7E1C8225AB1D1A3751A15,
	GettingNewSkillEvent_Event_m97B419231B963A6BC453D976B66479236DD36B98,
	GettingNewSkillEvent__ctor_m1EA90F4305DABEBB835F1DF73F85D452CF5A0C7C,
	HealBox_OnTriggerEnter2D_m9240FD1CDEE1662B8E2A1E090F51FE59EEC216C3,
	HealBox__ctor_mE820B1FEE45EA43430D083CA17F3D9A2B115AF5E,
	MoveEvent_Event_m797DA0E9DBF57058D2C392AA6103E797F2EB6BC1,
	MoveEvent__ctor_mDBAD24BBD72CD75398F7D3C023E2FB238007C8E1,
	TriggerDialogue_Event_m500342BB968BA39A9C63277293B7C80284AC034F,
	TriggerDialogue__ctor_m3BDA5E5F71BC525FFF15F7063A404CBAD50D8B4F,
	TriggerMusic_Update_m3D81E4170773A1DC48AAEFA5EF839FDC313FF565,
	TriggerMusic_OnTriggerEnter2D_mBBDB7F7767A26B8DC917670376CDAC4EBF1A665C,
	TriggerMusic_Event_m80AE3DB2CD83E22A176D6510F8DADF2CFEFBB6EC,
	TriggerMusic__ctor_m8080BD1615302632F3FD85AFFE20A9AFA99F695B,
	ActionButton_ButtonSetup_mE94F1B38707F2E7F0EF62D544BB3045D487A3208,
	ActionButton_ChangeAttackDescription_m76426CC654AB161E2D93407B9D56BB81D7611409,
	ActionButton_ResetAttackDescription_m73FC3C03B99C025FF50E090F57589554390F98FB,
	ActionButton_OnClickUI_m9894C8EF1E2D4911A070C8EBCE7734A7F537695D,
	ActionButton_OnCancelledUI_mD61186489A05491FB4AF7A664B0A9E7B080C963F,
	ActionButton_OnHoverUI_m4FCC381E3734596D42A64498FA2BA413FC71B771,
	ActionButton__ctor_m8919EB676AE8997E975D2B1280671B00E1E90FF9,
	ActionButton_U3CButtonSetupU3Eb__4_0_m08761867745A1895E5FC24E48212246605E32BA7,
	ActionButton_U3CButtonSetupU3Eb__4_1_m5FF477DF8FAFE5AEC5C37EAF80E03DF9C1E1F497,
	ActionButton_U3CButtonSetupU3Eb__4_2_m95AF25B868B387888CAE76BAA0C58A74D909B80A,
	ActionButton_U3CButtonSetupU3Eb__4_3_mD693DC64E13A8671086D2579FDE4F3CAA841305B,
	BattleDialogue_get_Instance_mD0C203A1C23ADF93551FE18C2669FC6A6F281A01,
	BattleDialogue_Awake_mB59F81F24B8D7016C035EC582AD1825EF4B915AA,
	BattleDialogue_Update_mB3E5A3299787A2FBC10682AD8D3CA233FE66F462,
	BattleDialogue_StartBattleDialogue_m0C9CB81C08435E0CF5D481A0DE771B8584539AEF,
	BattleDialogue_ChangeSentence_m7A50861150E21D1E918DB70EFDD0B69FBBF54D7B,
	BattleDialogue_TypeSentence_m03DBCF9A9051983E515A2A9D99379FFA2B616BE0,
	BattleDialogue_SkipDialogueButton_mB14C438B3C4CA11DA23CD0DD7B48A53D41243009,
	BattleDialogue_SkipDialogue_m6ABFE2A69F9197D674F77913BB87219C11AD494B,
	BattleDialogue_EndBattleDialogue_mD2DBA7C09E498F0DE6DDE30373B455A85D27F5BF,
	BattleDialogue__ctor_m2613E45EB3EAB65C795A472302EA508EFE44CC6F,
	BattleDialogue_U3CTypeSentenceU3Eb__15_0_m458E168B5080C9C7915C6C32DE32D5F131C05942,
	U3CU3Ec__cctor_mDAD38F28B51926BD0142F0852E560473208BE6EA,
	U3CU3Ec__ctor_mF26FE1D29F9CD75FC3C5F1D40F8E6E1C6D490BA1,
	U3CU3Ec_U3CTypeSentenceU3Eb__15_1_m669A9B245BC4C5673981C06453718715AB3028CF,
	U3CTypeSentenceU3Ed__15__ctor_m714D8743D196B43AECF81689E5212CEE2D292FA2,
	U3CTypeSentenceU3Ed__15_System_IDisposable_Dispose_m9FAED1ED91495E5D1C54C55022788CE9C1880A6F,
	U3CTypeSentenceU3Ed__15_MoveNext_m6F4090DC69FF856E2D8FC93959456EBABD9D62FE,
	U3CTypeSentenceU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BD268DFD0A34C14F5343375B8F74AE693EB2495,
	U3CTypeSentenceU3Ed__15_System_Collections_IEnumerator_Reset_mD678F10373F8B0D8D78BE91742776FF56EC4654F,
	U3CTypeSentenceU3Ed__15_System_Collections_IEnumerator_get_Current_m270C4F3E2E87C7235ED7F7457E25DEA8695791B5,
	TurnBasePlayer_Update_m407E0DF6C190A5809F7516BE8F7C91814B3E1557,
	TurnBasePlayer_SetUnitProfile_m2394130706ACB14F43C7D377CE61DCE4990B02D7,
	TurnBasePlayer_SetUnit_m8E9349191C58C0E2E106B921786CC3646A91B4DB,
	TurnBasePlayer_UpdateStatus_mB553802459C3E16BD07666672E8FD3A3D8957C62,
	TurnBasePlayer_DebugFullPlayerStatue_m3FD29EC404555DD300CDAC0DBBFE7F63B514F257,
	TurnBasePlayer__ctor_m2F938D8F16DD039005D079D9263082836439786B,
	TurnBaseSystem_get_enemyUnit_m51350F4B8790ADA3A6CBD2F73066529F13E22320,
	TurnBaseSystem_get_playerUnit_m8B8D68B355582068989616C845F11AF8391A9253,
	TurnBaseSystem_get_Instance_mF91629977DE4E454B34331DE163F5377D2169229,
	TurnBaseSystem_get_inbattle_m99F1923942FEEC348F29314A6FAE608D0E337700,
	TurnBaseSystem_Awake_m4352AAB3D281039297BF3CDD38B8C38477A89A8A,
	TurnBaseSystem_Start_m2D8F03F1DB3F5CC64C349E32EDE932102535EE14,
	TurnBaseSystem_Update_m8016CF36F5E4BF6B3B80D4C5BB3C0DC1FB7FA551,
	TurnBaseSystem_SetupBattle_mA8A505D87F206EAF8C6FF1BF7EA4EE030859E683,
	TurnBaseSystem_EndBattle_m04FE104E66DD5B7603A5E7E310019698B54FA902,
	TurnBaseSystem_CurrentNPCTarget_m5EA854441406654AF6E70ADCE564772AB7A2273D,
	TurnBaseSystem_EnterDialogue_mAF29D4AAE654BE261F650DAAF854564F22FFD4A5,
	TurnBaseSystem_OpenActionTab_m3B8581A9F45E7FBCF7271A2673BA2FB37ECB10F4,
	TurnBaseSystem_OpenAttackTab_m20421553E62709D797E58A4DC33C4B7946AC51A5,
	TurnBaseSystem_OpenSkilllTab_mD127B6EDB855E79650032200F02117893019EF77,
	TurnBaseSystem_PlayerAttackAction_mE08CD3B970472CED6EF0AE6A494CF5B496BFC40F,
	TurnBaseSystem_PlayerSkillAction_mD6903E2C33C9B5200E08D31765160EF7B1BEAC4B,
	TurnBaseSystem_EnemyAttackAction_m57DBEF969E3F6FA517DD353D7609A47200910FD9,
	TurnBaseSystem_PlayerTurn_m64279C163C3632B41265E29A38CE186760934439,
	TurnBaseSystem_PlayerHealTurn_mE022E32CFA0E9B5A2CDFA97F4F6F3C7C02031F7E,
	TurnBaseSystem_EnemyTurn_m953332AC41E491EA08D064B7566E08E5211C6076,
	TurnBaseSystem_Resting_m9F8D83DBCBCBA3032F60AC74509D2F1A8F94FE16,
	TurnBaseSystem_SkillHeal_mCD4C00815BE5D059CE8CCD645C032BF363BA05EC,
	TurnBaseSystem__ctor_m7C641C0BBE80EA6619F10A5271C76A985E5DF3B0,
	TurnBaseSystem_U3CPlayerTurnU3Eb__35_0_m71013D9FECDC342FF5202C42E881593D8CB8ECA2,
	TurnBaseSystem_U3CPlayerHealTurnU3Eb__36_0_m4F6A8805E871F063CB75F553296744F0FFD78D5F,
	TurnBaseSystem_U3CEnemyTurnU3Eb__37_0_m6587319AD0E3D72F409E682B319B063AAC5E1A6F,
	U3CPlayerTurnU3Ed__35__ctor_m8AB2530F00AAC23868A1014008E4510CC8A85E7B,
	U3CPlayerTurnU3Ed__35_System_IDisposable_Dispose_mBA54B120599E2137E5393768D6120030E95A336B,
	U3CPlayerTurnU3Ed__35_MoveNext_m6943F4F83434E0EB6AA8FBA38251229DB98EE141,
	U3CPlayerTurnU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61EACE996BC752C1364CCA92B5C2166D4123E2AC,
	U3CPlayerTurnU3Ed__35_System_Collections_IEnumerator_Reset_mE2DDF9EF573EE8506B67333FD381EC49558D4D04,
	U3CPlayerTurnU3Ed__35_System_Collections_IEnumerator_get_Current_m66E0053278E42810479BE9E520E40D27F246FAC3,
	U3CPlayerHealTurnU3Ed__36__ctor_m16FD053EDC1AA07B2FEFB29F67B4AA7000E791F2,
	U3CPlayerHealTurnU3Ed__36_System_IDisposable_Dispose_m93A030B3E7AF0DCF29F04DFA1A1047D57632DA42,
	U3CPlayerHealTurnU3Ed__36_MoveNext_mFA42B2A4EABBF0763C8926D529BF5964DAA5AD05,
	U3CPlayerHealTurnU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA71D3FD00986769C1CECB82B8711E700D8430D99,
	U3CPlayerHealTurnU3Ed__36_System_Collections_IEnumerator_Reset_mC26A5D24136DEF19D8186666D9610B2B419215E1,
	U3CPlayerHealTurnU3Ed__36_System_Collections_IEnumerator_get_Current_m0F726B46089D8ACB32B00F591BE0D9B9361BE751,
	U3CEnemyTurnU3Ed__37__ctor_m9E7906BD17A2D26440DDBE0432B224E44AF8DAFA,
	U3CEnemyTurnU3Ed__37_System_IDisposable_Dispose_m4DDADCDAB60E4E76F6BF71E63260855719A5B556,
	U3CEnemyTurnU3Ed__37_MoveNext_mB6A16F0679A1CE9A1AC81B7354CA9DA592FB6560,
	U3CEnemyTurnU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7E55AF6A8D378D610248C15AABB1DFB37EA4FF6,
	U3CEnemyTurnU3Ed__37_System_Collections_IEnumerator_Reset_m37EA51052F303CAEB8428D0A0BEA66936454B4DA,
	U3CEnemyTurnU3Ed__37_System_Collections_IEnumerator_get_Current_mBFE15F4A828D23A7AA24CF09039B4D93C3870896,
	TurnBaseUnit_get_attackProfile_m5EC8B0BB4333910F3F7A583C510AD1CBE9220C3A,
	TurnBaseUnit_Update_m3EE9F07A2BD9F457618C7DFECEA22526860FC77F,
	TurnBaseUnit_SetUnitProfile_m4ADA07FD6D13CBDC18941F0FB8835524793B1509,
	TurnBaseUnit_SetUnit_mD33BB1EAAC743E34C7BB891EF50722322135E0BB,
	TurnBaseUnit_UpdateStatus_m7BDB439E6F228B0C4C2E63D272FCF87DFDB97786,
	TurnBaseUnit_DebugSetHPtoLow_mCD17C7C107760924287F33E0AE7EEBC8BD18CBD7,
	TurnBaseUnit__ctor_mD5D120FAB64631026ABEAD1DC1DA043740DA5039,
	TurnBasedUIManager_get_Instance_m75EF5E7A730B20708AD7B3BBAFBA3966EBE9B995,
	TurnBasedUIManager_Awake_m9A6345BB0DCF94D18C0FC0C0DE6F3D9F8E22A70D,
	TurnBasedUIManager_Start_m5A425962A1586A14D316FB09B1C831B7D512C9A2,
	TurnBasedUIManager_GenerateAttackButton_m9E301967E44348C58855DFF3A6F01D5426DC4817,
	TurnBasedUIManager_ClearButton_m96C1D0D24209738D29C797EA2ECA2A88079A555C,
	TurnBasedUIManager_ChangeAttackDetailText_mBEF7EC8E397C87A011E6C953506015C7CA4E7DF7,
	TurnBasedUIManager_ChangeStaminaUsage_m81BF49A3A6CA5C9B1280DD51022027D123AA554B,
	TurnBasedUIManager_OpenActionTab_mA5D8D1B4753A96A8810A1E85E1F42A6D9BB95E0A,
	TurnBasedUIManager_OpenAttackTab_m901A501A97162735F0CDF4B263CB975010F6C860,
	TurnBasedUIManager_OpenSkillTab_m8E1C03C81CCB5CF67DCCAA7D270667BC6AF09BCD,
	TurnBasedUIManager_Resting_m4DF4A614BE945B9C50F59928733BAA2F87E4998B,
	TurnBasedUIManager_OpenResult_m3104EBA7FEAD2C6F8A4A2868D34C98CC286F8914,
	TurnBasedUIManager_EndBattle_m974B25B43D9169D06772125BEF2FE3F6B11FE03D,
	TurnBasedUIManager__ctor_m9F086F9214D6626F4ABAF1FD0C99A414EC036AEB,
	CameraManager_Start_m3A8F66B836B9A9BC419B62EF88451549BBC9CCD0,
	CameraManager_Update_m911D2029A80BF20148CD94D496452F232343FF60,
	CameraManager__ctor_m932EDBCBAE89115380AA929ED5BC9823B9371185,
	CharacterCreationMneu_Start_m32257EEE3887D9BB68471BBBC430B9DBFFD2DD2B,
	CharacterCreationMneu_ChangeCharacterImageRight_mD2AC61C9426B83974F7EF0CAA92DD9AEDB995535,
	CharacterCreationMneu_ChangeCharacterImageLeft_m6C4F38DE16F0FFE9E51D9F326CF3DC87DFBBC608,
	CharacterCreationMneu_ChangeCharacterName_m6C56DEE1A5A0EF30D1D57AE49B9635F3809EAA62,
	CharacterCreationMneu_SetImage_m3C44F5F44EB813DAC4766E4E212E683025EE3F48,
	CharacterCreationMneu_SetProfile_mAD53DC730D7899374015B10F670FF2DEDCC0B4C5,
	CharacterCreationMneu_ResetProfile_mDA5850DEBA57886979CFF5942A804B299C32853B,
	CharacterCreationMneu_IncreaseStat_m8C60F3155CC238235057C6C54242C09127CE9639,
	CharacterCreationMneu_DecreaseStat_m200A44D05270F427D37B4C690E096E0065DFF852,
	CharacterCreationMneu_SetButton_m2606BF7484D7B3A7FD5821C104CD37EF32374735,
	CharacterCreationMneu__ctor_m2692750A1A15113CCA5B92455B91D8619C73486F,
	U3CU3Ec__DisplayClass22_0__ctor_mC99D3FCF62DF88622051F92E708A3E83DD88329E,
	U3CU3Ec__DisplayClass22_0_U3CSetButtonU3Eb__0_mD85AD97BC66C9FB5E133C473C346D2ACD7A8E996,
	U3CU3Ec__DisplayClass22_0_U3CSetButtonU3Eb__1_m924BB2A7E1B4079762B8F406F54DE5805820160B,
	CustomCamera_Start_m8CB9ADCB516D51E8E5F4B33490945B2F32C08415,
	CustomCamera_Update_m25201A4AD9D0F6357B35E02FFBD85EE2B46ED701,
	CustomCamera_CameraMovement_mCFAE95ACAC4E135952C29C6E08D87CF55B02F73C,
	CustomCamera_get_CheckX_m048A9DD496A436806A907EBFC1F62183EAF20B06,
	CustomCamera_get_CheckY_mA51848E656CF5A4FFC6B01CFA17C7297F4F4363A,
	CustomCamera__ctor_m924280DC9D40E5DC1482DCCCDCD7E501F53E985D,
	MenuManager_Awake_mEFBAF3F8CBDEF5A033B3BAD9CA897801135B6463,
	MenuManager_Start_m4E4A5EF33C27448D7F34FD29B93589635F1B2EE2,
	MenuManager_OpenCharacterCreationMenu_mE2561068153DA0923C7F297F5873CF2EE71DB4F6,
	MenuManager_OpenMainMenu_mEEDFCE3B23480F6ECF1B092EC9042E1963001B43,
	MenuManager_OpenSetting_m31C6ADF97C8A7E0F331653D00A266A2A5DDE25D5,
	MenuManager_OpenCredit_m5366171F17422BDAAEF6A606A002BFAC491DD0AA,
	MenuManager_ExitGame_mC7F9385BFDAD357058DAD3C97268BC8993494F6B,
	MenuManager_CloseAll_m8DF0F562FD7E6BAE4145DCF6281DC8D5C2459B8E,
	MenuManager_loadScene_mB717D5E9676CEF035C842EF68CE71DC876F68F3A,
	MenuManager_RunStartupCheck_m3D1F693BFAAC94F6B6E4428EB1CF4A49763E12EE,
	MenuManager_WebCheck_m2537950FF57C182A25A718D43A14B8B5D2EBB01D,
	MenuManager_OnClickUI_mB9BAF3AF7CBEE08E0270322F7EE81FA86A3EF7E9,
	MenuManager_OnCancelledUI_m49919F4FD962BE8A25A136B788066E4C34E87A40,
	MenuManager_OnHoverUI_m4923418F5F93258590FB02C319219E56EF8B5E9A,
	MenuManager__ctor_m8F61CC885B72291B54C1C6EC368AE303EA856529,
	MenuTabManager_Start_mDA80B67CE9422788DFE0955438846C86433E1B54,
	MenuTabManager_Update_m5EEEB00FDF26482CA7A8BEB7B524CABA38BF6832,
	MenuTabManager_OpenCloseMenuTab_mBE2D6AE74A266E8661072C33948CBD72EA57A772,
	MenuTabManager_OpenCloseSubTab_m1F7C1C7913BA28CFA6ECECE1353BC72383FBAC83,
	MenuTabManager_CheckPlayerInformation_mE5FA62007FC29E5515FCB1DB00389EB06780578A,
	MenuTabManager_Pause_mE8E795AE9D476FD4CD349075E37873B8DBEF111F,
	MenuTabManager_Resume_mBC6F70080425BE49A79B45792DCE7DB71660C1B5,
	MenuTabManager_loadScene_m8C362A7505F8A97FBE52CCA1E364244C31FC11E5,
	MenuTabManager_IncreaseStat_mC55760452B1B4F86327F0A1A13177466B0AAE3C2,
	MenuTabManager_UpdateStatText_m8935AB46D891F76508F5F32FBADDC1103AD169CD,
	MenuTabManager_WebCheck_mAFC1BA7FA1DB43B4BF79A16672F1B0A3AC2C31CD,
	MenuTabManager_OnClickUI_mF237C0B938076A0D70C840155852C0878DA288B7,
	MenuTabManager_OnCancelledUI_mAB1B95B63F945B017899EB9DA7500148E74BAD25,
	MenuTabManager_OnHoverUI_mB1DA6C8180247BE70AA814BCC74760470F657E8F,
	MenuTabManager__ctor_m2CEA8B55C342FCEBA7824C34750C4036AD65DFC2,
	U3CU3Ec__DisplayClass30_0__ctor_m34E5A1AAB7275186461FA2FE7E89E73769F5937B,
	U3CU3Ec__DisplayClass30_0_U3CStartU3Eb__0_m0AAC18AFD777333DEFAE362E22BD087A711B275B,
	ResolutionSetting_SetResolution_m6CE1C2CA19F20697FFF1F93D0A4E7FD718D282C2,
	ResolutionSetting_SetFullscreen_m9A3ACC61D389703B6579FD109FCB33F77911A5A8,
	ResolutionSetting_dropDownCurrentStartResolution_m561E225092EB39044346CA7CF855022DEA3AD828,
	ResolutionSetting__ctor_mB0A19DB25DD45D37837224D84E80A95C1F0739AD,
	SceneLoader_get_Instance_m260F687667E99FC4E952DEDC6B3FC65CC788F0BD,
	SceneLoader_Awake_m0618C263DDE94B4607D9D5148F87247B506A337C,
	SceneLoader_startLoadLevel_m04754948AF7C6B19FB5FB11C8875F6F6B8D3EA25,
	SceneLoader_exitToDestop_mABAD45692C757391FE8C86289AC7128ABFECC8F3,
	SceneLoader_loadlevel_m04CF2C85AEFC903C21B789AD1825E90000E5DC97,
	SceneLoader_exitGame_m2924FA892A01B56DE948D8583EC159DCB0DD1D36,
	SceneLoader_TransitioningBegin_mEEC98108AAFE48A2386596D3F119D103ACF22B36,
	SceneLoader_TransitioningEnd_m2B0F39FE99C608DD3F7A70274CB480632E81D77B,
	SceneLoader__ctor_m8708D080848349110CEA260D8779F30BD5823912,
	U3CloadlevelU3Ed__10__ctor_mAE58B80B195BBFA7510F6E68EA9A1298361DBBA4,
	U3CloadlevelU3Ed__10_System_IDisposable_Dispose_m860B69D84C70B4F5896588A3D6F8064C6960C632,
	U3CloadlevelU3Ed__10_MoveNext_m2D8401AFBD77283234CC84DF6144D756A800B0D6,
	U3CloadlevelU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DC83A18DF8FF983CA0BF345C8B581996D42D402,
	U3CloadlevelU3Ed__10_System_Collections_IEnumerator_Reset_m51A5262162ECD04D7DE7A5F02E29160606469126,
	U3CloadlevelU3Ed__10_System_Collections_IEnumerator_get_Current_m3AD12E1478791BEAA33BB99964E9329A73384BD6,
	U3CexitGameU3Ed__11__ctor_mFF73E161755102B656F61250D9D576BE9A096C0F,
	U3CexitGameU3Ed__11_System_IDisposable_Dispose_mE0A07690D50D877F2029B4AAD833CBBDF8B5FAD8,
	U3CexitGameU3Ed__11_MoveNext_m0AFD3A8DEEF3C9FB4243BA7A6928D26A36AC59AC,
	U3CexitGameU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFEE4287F4E70EF27C575F348CABF960F5CEB4DCC,
	U3CexitGameU3Ed__11_System_Collections_IEnumerator_Reset_m42D8867845452E08B152564A3AD4191A22C79851,
	U3CexitGameU3Ed__11_System_Collections_IEnumerator_get_Current_mB57F50DA13B68471F8331091EEDAAC4788CFF4C3,
	VolumeMixer_Awake_m23F7C0578529E573B13C7A8FDC402A310BFD12B6,
	VolumeMixer_StartUpSet_m715236EB9A605ECDD39FB6794B4F933F8EAED155,
	VolumeMixer_SetVolume_mFE149DEB806A9D3FDAE5242829C35248A479D795,
	VolumeMixer_PlayEffectOnce_mC3AD134F86585FBA710D30736C38E412224012BD,
	VolumeMixer__ctor_m0026E479FD2F148D6067EF304C73ECDE1E915FCE,
	WinLoseManager_get_Instance_mC4D2B4CA667A9CDBC5B4E494174DE668F1E2344B,
	WinLoseManager_Awake_m41D60A8A03A24DF139CE8FB566EFFD8E009279E7,
	WinLoseManager_Start_m2E32930E3AF736B1D19D5F101559E63754AE4CE6,
	WinLoseManager_Update_mD99C4FF5969FC2B24E42809CCBFD4FCAA49FCBF6,
	WinLoseManager_WinCheck_m40489380BFE19E913EBF5B383A9169E0166C5938,
	WinLoseManager__ctor_m9BEA6A4A6A01273878129498C2CC4177230EDAC9,
	WorldNPC_Start_m63C1535B1E8237579549FCA2D997FA2D1114B3BE,
	WorldNPC_Update_m6A4D1A7E6E321DFC6136E20E55F00A629641271F,
	WorldNPC_NPCNotificationBox_m094F73AF08085D59D373FD77BF23D13CC9F9E2FC,
	WorldNPC_Interaction_mB686444EA3C37D5FECE2B7545FC1266DE45B5E95,
	WorldNPC_PlayDialogue_m05EF23E7667CCB555EFDF7400C3A9E62A1157B92,
	WorldNPC_MouseHovering_m133B22B864AE178D67BFFCED1C21370CCD4A40F7,
	WorldNPC_MouseNotHovering_mDB720C36AD4B56AA3DFA78F538E820825EF82108,
	WorldNPC_PlayOneShot_m231B1C37029CCFDB1605FEF3BF694D1ECE91C106,
	WorldNPC__ctor_m2FE39FE97859583BD1C0677FED187F6D72897E23,
	MathGraph__ctor_m08F714216D2826AD4EE77B6E6CA0EE18C487034C,
	StateNode_MoveNext_mF38DF0A127D499DC6FCD3CD2EC2FFFD44CCE329C,
	StateNode_OnEnter_mD3B49F17ACC5833C7893AF00B0748905A885E274,
	StateNode__ctor_m4C751F717E323C2EB3A0C1AAC81AEC1511892674,
	Empty__ctor_mB3CFFBD34162B6ECDDB72EE7404D4CE10F8F6CE4,
	StateGraph_Continue_mAB5A6B3406F1AD7C5BD984E5C434B6FEEF6E6BB0,
	StateGraph__ctor_mA29022A97B62DC51A73D18D0C20F894E50E5611B,
	Connection_SetPosition_m0DE9B5BA8284A656F22A4FC41D68827DBD32EF3E,
	Connection__ctor_m885969234D0CDC49BEC2C9EBE2F6E1EB9AF5A742,
	NodeDrag_Awake_mA27E486007B578DA6BBE766E6966A53EC3762231,
	NodeDrag_OnDrag_mC28D400062F09A29AA10C479212242164743E957,
	NodeDrag_OnBeginDrag_m06EF21377A6ED85C2B76498B75EFA34443F4DC43,
	NodeDrag_OnEndDrag_mF64FDE7FA151F9D74892CBCD5EA768F183E1815B,
	NodeDrag_OnPointerClick_m61E783969FE13EC21DD1521BB496EE754C30930C,
	NodeDrag__ctor_m774BC055632E89033FF352CFEFBB3C9FE2A47E05,
	RuntimeMathGraph_get_scrollRect_m69128FFC7B3A2FCA86348549F47FD80E5CFEE579,
	RuntimeMathGraph_set_scrollRect_m1986940E63F05E28544767835AB2A62B823F9163,
	RuntimeMathGraph_Awake_m0BC41A4604D8747F89FC44489AA601FAE12F5D5A,
	RuntimeMathGraph_Start_m37FADA97F393ECA30DCB7523146B4E4CD9523B95,
	RuntimeMathGraph_Refresh_mAB35523776F6270C94B99A235D38C067E3EBD71F,
	RuntimeMathGraph_Clear_m5315AEF3A710381876FC7EF568C50AB4F5C7D179,
	RuntimeMathGraph_SpawnGraph_m44240E4BC4A61614FA4289BDF3A2F985612BFA31,
	RuntimeMathGraph_GetRuntimeNode_m12555443F553A33A9E498E5F57A737C532B68127,
	RuntimeMathGraph_SpawnNode_m18E97E41E618C18CF0F4D90E2E92EB03F344C413,
	RuntimeMathGraph_OnPointerClick_mFFB2097E7C639F4B1865E08B6CBED86CE931FB90,
	RuntimeMathGraph__ctor_mE758CEC960AC34D4AF6415FD9E935D57013DE92F,
	UGUIDisplayValue_Update_m8F684BB8CA98C5A5875BFDA18E66EBF9400597B3,
	UGUIDisplayValue__ctor_m49DCDC902B37CE58310FA86CEF9FAA7BB57730EE,
	UGUIMathBaseNode_Start_mCC8230FAAD21E347905EAC8E43781F8F1C667C78,
	UGUIMathBaseNode_UpdateGUI_m6E3FEDE70043BD6A020A4F4CAC2582F60AABBCBE,
	UGUIMathBaseNode_LateUpdate_mD25DB9CC6BBB573B36A2FF7D4DB57E862B73BD70,
	UGUIMathBaseNode_GetPort_m40640E74F1E11F6228B99EE4AEF755273912A228,
	UGUIMathBaseNode_SetPosition_mED0CAC51A05BB47B57E4C22467C4C557A632197D,
	UGUIMathBaseNode_SetName_m7E4B927D9083D8339A8B7AE766041121B23F1E8D,
	UGUIMathBaseNode_OnDrag_mCA51DCF8AB74286AE291F17C769ABC116EDDF1C9,
	UGUIMathBaseNode__ctor_mFA4439A9A6BDF85DE3E54BD073FFFFA29B5B898B,
	UGUIMathNode_Start_m12F0F45407D6C2F9903B6D9A5442723F47886F73,
	UGUIMathNode_UpdateGUI_m8068871F046E2797C1054139F51DD9D9C3133C3F,
	UGUIMathNode_OnChangeValA_mE43F3BEE4F1E987C9F4781CBBCAFF9FC93DCB3E2,
	UGUIMathNode_OnChangeValB_mD973F44CBA5355FF5A9E2E4CBCA57BEF74A99D4A,
	UGUIMathNode_OnChangeDropdown_mA55CE87A28D662FC9A9B4C0D1FE2C156E3AE2F62,
	UGUIMathNode__ctor_mE3040969A32BD31A360890C3C7828A1956F32419,
	UGUIVector_Start_m0D32366FA45B76BDF3D815CA444D1FB3BBFBF2DA,
	UGUIVector_UpdateGUI_m35127D5C240AAC71AE2CDC6E87291B1BBE3A82BA,
	UGUIVector_OnChangeValX_m0D88CEC7A70BE205FAFB38118BCB6138E7C29BC0,
	UGUIVector_OnChangeValY_m322AFA104D56AD26A117D84FE51D44C9B89991F3,
	UGUIVector_OnChangeValZ_m6DE56F27602B03A6503F03D155356C1C8B7E7299,
	UGUIVector__ctor_m3B34111FC3E736C294B6BC5F34D9C6FA421D9626,
	UGUIContextMenu_Start_m5B4F01A8A8815C23DFB29B74948AF9740F9C4F46,
	UGUIContextMenu_OpenAt_m234509E25996070CACA3ED6EC21859D7ECCA2CA5,
	UGUIContextMenu_Close_m0DA5264711DC3F5370AFE5DABCE6EF89A8DCC9E9,
	UGUIContextMenu_SpawnMathNode_m5A61225832760022A450FD77CE6971BAE8C7C4F7,
	UGUIContextMenu_SpawnDisplayNode_m0808BC446C93FA5A07415DD347EDD2C289BF2A07,
	UGUIContextMenu_SpawnVectorNode_m52C2AE86073C370ABA1417A48A850780B2F44479,
	UGUIContextMenu_SpawnNode_mB0111D2909BDF48F9982199E228DC5C7EA153127,
	UGUIContextMenu_RemoveNode_mCE90FA971477C7C2C75FF4BFC7CD9A050AB51607,
	UGUIContextMenu_OnPointerExit_mF6AD7A860889506CFC4B5C3310653D49C0D7E132,
	UGUIContextMenu__ctor_m0C738118D07DEC5AB10678AA709A180EDC1D126E,
	UGUIPort_Start_m16A46CA9E227A0FA5A0E1C792845FECD76320CC8,
	UGUIPort_Reset_mFC4D288DC3B6328010EA302790BF40104FF688D9,
	UGUIPort_OnDestroy_mA3B5DE56603381657E8F11190E81613F506216A4,
	UGUIPort_UpdateConnectionTransforms_m93C7B78D7186733D9E905F96B65DC327C6A2033B,
	UGUIPort_AddConnection_m377FF09BC188462247A157B3E824D2AF4775CA0F,
	UGUIPort_OnBeginDrag_m0497CE2770B738F135D0093A6110701F725E2690,
	UGUIPort_OnDrag_mB2454F027A0BA53A5A9775618296F662596BA946,
	UGUIPort_OnEndDrag_mCFCCD3057FC385B8A42CCA24D52AA8F444DA0949,
	UGUIPort_FindPortInStack_mBF5E071151838E7B315E56D2EE6BA952073CA28F,
	UGUIPort_OnPointerEnter_m794955F3173C621EB4C0872E891FBF973B9EC4B9,
	UGUIPort_OnPointerExit_m122D9372F12459338529164974AF3284C7268B9E,
	UGUIPort__ctor_m05B9EB0AC157ECF11A7E13A0F2BD22F6B7CD1F53,
	UGUITooltip_Awake_m009D407FB9688A335943BF417611C1B98722070B,
	UGUITooltip_Start_mE587ECE5B94354E0D7198C228D9DB141EA327029,
	UGUITooltip_Update_m60F17384E60DDCB53F4990F41DCAC22D7F3FA0B8,
	UGUITooltip_Show_mC452CE990806140D452CA0B1E60F21571F52392B,
	UGUITooltip_Hide_mB3965C14152968E2A1BB6378A163CD29FB7F14E1,
	UGUITooltip_UpdatePosition_mF1611863E3F063990CB26F6AD8C6E0093C09D18E,
	UGUITooltip__ctor_mA31943CA10B3DFF93ECA9AAE73E23EC50FCF8664,
	DisplayValue_GetValue_m821DEC54FA25E0DC1E29449D00EC6E84F1634A9F,
	DisplayValue__ctor_m67F955616D3F180414CE5BF952CF919791EDEE37,
	Anything__ctor_mA360F49DFBA462EA29AC621068299A0FAB092615,
	MathNode_GetValue_m6D83304B019EB129B4AA2AE722EFE1CBCD7B12DC,
	MathNode__ctor_m7E0515C0027A4C5C5197160F3445EFF626CDD535,
	Vector_GetValue_m80A517E30C6CACB1A26481BF8ACE27AB86DCC14B,
	Vector__ctor_mDF3D9A96569F08C79CC5623BB9A1117637FF866C,
	NULL,
	LogicGraph__ctor_mAB6F8B727E2038C38130E7528D398552A709FC2E,
	AndNode_get_led_m07058D48B07684BEBF2D14C8933FE7E0D8B9B3DE,
	AndNode_OnInputChanged_m1EE0058B9F052F55885C2026D1E8C9EE22B25BBF,
	AndNode_GetValue_mF4ACC4AF67BEEE839673FB403938FBEE3775709C,
	AndNode__ctor_mD884FBF670927323BBAF70872D6E2E8CF76EEAC6,
	U3CU3Ec__cctor_m43CB6CE6E61263B6925CD794A608A9FB7D2EF22C,
	U3CU3Ec__ctor_m505A5863A0F2D60CBD1EA8266CA1DEF45BAD6402,
	U3CU3Ec_U3COnInputChangedU3Eb__4_0_mC0473403A4ECC84E2059EB11636842525924C1C4,
	NULL,
	LogicNode_SendSignal_m3388469DD1139C817C13CF4C1BDD50ABC4B09656,
	NULL,
	LogicNode_OnCreateConnection_m396ACAE1EA68D7C16AE811A4F35F52C3F27A59EA,
	LogicNode__ctor_m36EB60F398F9141795EB8DD413874B7BD01785DE,
	NotNode_get_led_mCEC980A6562BB36D49D932437B8B62EE0CDC12A7,
	NotNode_OnInputChanged_m2C326BAF912DF9D7285F29BA042A4DFB0C5850E4,
	NotNode_GetValue_m1BF31296D764FDEC869EDFC3C5AEBE0A7CFEBFDE,
	NotNode__ctor_m06B5A6182F54943E9839E47FE00A1C656CC7FA6C,
	U3CU3Ec__cctor_mEA75EA0EC633FAE25B9C1735436BFFD2BC620D59,
	U3CU3Ec__ctor_m93BABCF9975FF7706553E79DDFBDA77FF1887827,
	U3CU3Ec_U3COnInputChangedU3Eb__4_0_mD40C965A34673FF685383249FB878346A5F3ED89,
	PulseNode_get_led_mBC363F5F80159ED336A4D85600670C448E6E5F14,
	PulseNode_Tick_m51D3DEBCE38B48D1C19541BBF5E6AE017AFBBA66,
	PulseNode_OnInputChanged_m8C94E1360CBBF65996C46E157F77F4D957FF6EF5,
	PulseNode_GetValue_m0ADF405D9AB631B157886DEAD787D2971BF6E8B5,
	PulseNode__ctor_m4837A62F6A63AD2240009F45463D56AC1A560ECE,
	ToggleNode_get_led_m9B094C6B485A6ECD535230E4EA5C95F657078597,
	ToggleNode_OnInputChanged_mBC94FB0893E6995296C3AC06830FD33A7BF3B4B7,
	ToggleNode_GetValue_mC5B0977F82799E7E7881F7B60787C421A643B649,
	ToggleNode__ctor_m1077F1C1168F9947181F8CF087605EBC7E3CAA21,
	U3CU3Ec__cctor_m5302482C728DC301D359F3CF3F07B45A3EB94074,
	U3CU3Ec__ctor_m3EF3B40085A477D5DDC42C0160AE3EEBA71EAB18,
	U3CU3Ec_U3COnInputChangedU3Eb__4_0_m6252E0DE67AAF790D8D3C21174A106E66D64B1B2,
};
extern void PlayerActions__ctor_m7481EA18F112C70293D3474392F06706C811E621_AdjustorThunk (void);
extern void PlayerActions_get_Movement_mEA5BB53310B50C0F518A9F4F610423C3905E9207_AdjustorThunk (void);
extern void PlayerActions_get_Interaction_m77608CD4C8D9A2A8B508BD745FAFA1622C5835C7_AdjustorThunk (void);
extern void PlayerActions_get_Escape_mB334771D35D03DCB58855F20B5A151BEB95681DD_AdjustorThunk (void);
extern void PlayerActions_get_LeftMouse_m5E5CAEA8DAC2A5065B4890005B37F0A63B2F8A4D_AdjustorThunk (void);
extern void PlayerActions_get_LeftMouseReleash_m8B39D61E60E15BCD618823B459D8B35D0C729B29_AdjustorThunk (void);
extern void PlayerActions_get_RightMouse_mC71D836809242B2B2931CDED4D47B659DB5CD223_AdjustorThunk (void);
extern void PlayerActions_get_MousePosition_m12C40F11BB2A48AA6F4CBEAC2307BE06F6CADC26_AdjustorThunk (void);
extern void PlayerActions_get_X_mC015291F0DD75D6C781276BADA2E2DE4325531BA_AdjustorThunk (void);
extern void PlayerActions_get_C_mF777A74F1B446CDD7CE1FD7E151C89781B115492_AdjustorThunk (void);
extern void PlayerActions_get_P_m04B5F31DBC4699181C495E9BC290D5FBC292C0EA_AdjustorThunk (void);
extern void PlayerActions_get_O_mB7AF304DB8C29291B5D5A622F1D5898A370972FF_AdjustorThunk (void);
extern void PlayerActions_get_ForwardDialogue_mE207E1B693F9DE4F991D8709CD8DD5D2B85BE6C2_AdjustorThunk (void);
extern void PlayerActions_get_Tab_m338B414903369841F681082F90CD34C17E3CA12B_AdjustorThunk (void);
extern void PlayerActions_get_Debugging_mE0B8A7A3F30DF25EFED5C27CD4F1088F0377456E_AdjustorThunk (void);
extern void PlayerActions_Get_m9E302E68280071685925C8B03873ED42656775A8_AdjustorThunk (void);
extern void PlayerActions_Enable_m00A9A57EDAC1A58DFB03DC10C4E266AC921B552C_AdjustorThunk (void);
extern void PlayerActions_Disable_mB03301453FDA738D343E68B47B67493E74C6FEA9_AdjustorThunk (void);
extern void PlayerActions_get_enabled_m0C8FBC0086D06E55C6C2FDCFBC7C53C1A06F15B7_AdjustorThunk (void);
extern void PlayerActions_SetCallbacks_m0CAD45BB19CA2B72DB0BA6C127A5137CB5D458E7_AdjustorThunk (void);
extern void UIActions__ctor_m944934E74521224F76E9F1FB5C83CB23EDC1D41B_AdjustorThunk (void);
extern void UIActions_get_Navigate_m6C827DD1D55866AAB36A4E59CC372409621AC711_AdjustorThunk (void);
extern void UIActions_Get_m65A7C87B82A96CBAD0B2662044414AB697A0266A_AdjustorThunk (void);
extern void UIActions_Enable_m49364651EAEA42FB35D42CC6AE6E4305195295BA_AdjustorThunk (void);
extern void UIActions_Disable_m90FE4E19AC2051688F7FE7D3F593BBCDEB65E913_AdjustorThunk (void);
extern void UIActions_get_enabled_m19088D7D7D446891B6DD16C542B70E24A65CD4E8_AdjustorThunk (void);
extern void UIActions_SetCallbacks_mF059380A033D1DCCBDEEF8FA931A92C7AFABEE2C_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[27] = 
{
	{ 0x06000010, PlayerActions__ctor_m7481EA18F112C70293D3474392F06706C811E621_AdjustorThunk },
	{ 0x06000011, PlayerActions_get_Movement_mEA5BB53310B50C0F518A9F4F610423C3905E9207_AdjustorThunk },
	{ 0x06000012, PlayerActions_get_Interaction_m77608CD4C8D9A2A8B508BD745FAFA1622C5835C7_AdjustorThunk },
	{ 0x06000013, PlayerActions_get_Escape_mB334771D35D03DCB58855F20B5A151BEB95681DD_AdjustorThunk },
	{ 0x06000014, PlayerActions_get_LeftMouse_m5E5CAEA8DAC2A5065B4890005B37F0A63B2F8A4D_AdjustorThunk },
	{ 0x06000015, PlayerActions_get_LeftMouseReleash_m8B39D61E60E15BCD618823B459D8B35D0C729B29_AdjustorThunk },
	{ 0x06000016, PlayerActions_get_RightMouse_mC71D836809242B2B2931CDED4D47B659DB5CD223_AdjustorThunk },
	{ 0x06000017, PlayerActions_get_MousePosition_m12C40F11BB2A48AA6F4CBEAC2307BE06F6CADC26_AdjustorThunk },
	{ 0x06000018, PlayerActions_get_X_mC015291F0DD75D6C781276BADA2E2DE4325531BA_AdjustorThunk },
	{ 0x06000019, PlayerActions_get_C_mF777A74F1B446CDD7CE1FD7E151C89781B115492_AdjustorThunk },
	{ 0x0600001A, PlayerActions_get_P_m04B5F31DBC4699181C495E9BC290D5FBC292C0EA_AdjustorThunk },
	{ 0x0600001B, PlayerActions_get_O_mB7AF304DB8C29291B5D5A622F1D5898A370972FF_AdjustorThunk },
	{ 0x0600001C, PlayerActions_get_ForwardDialogue_mE207E1B693F9DE4F991D8709CD8DD5D2B85BE6C2_AdjustorThunk },
	{ 0x0600001D, PlayerActions_get_Tab_m338B414903369841F681082F90CD34C17E3CA12B_AdjustorThunk },
	{ 0x0600001E, PlayerActions_get_Debugging_mE0B8A7A3F30DF25EFED5C27CD4F1088F0377456E_AdjustorThunk },
	{ 0x0600001F, PlayerActions_Get_m9E302E68280071685925C8B03873ED42656775A8_AdjustorThunk },
	{ 0x06000020, PlayerActions_Enable_m00A9A57EDAC1A58DFB03DC10C4E266AC921B552C_AdjustorThunk },
	{ 0x06000021, PlayerActions_Disable_mB03301453FDA738D343E68B47B67493E74C6FEA9_AdjustorThunk },
	{ 0x06000022, PlayerActions_get_enabled_m0C8FBC0086D06E55C6C2FDCFBC7C53C1A06F15B7_AdjustorThunk },
	{ 0x06000024, PlayerActions_SetCallbacks_m0CAD45BB19CA2B72DB0BA6C127A5137CB5D458E7_AdjustorThunk },
	{ 0x06000025, UIActions__ctor_m944934E74521224F76E9F1FB5C83CB23EDC1D41B_AdjustorThunk },
	{ 0x06000026, UIActions_get_Navigate_m6C827DD1D55866AAB36A4E59CC372409621AC711_AdjustorThunk },
	{ 0x06000027, UIActions_Get_m65A7C87B82A96CBAD0B2662044414AB697A0266A_AdjustorThunk },
	{ 0x06000028, UIActions_Enable_m49364651EAEA42FB35D42CC6AE6E4305195295BA_AdjustorThunk },
	{ 0x06000029, UIActions_Disable_m90FE4E19AC2051688F7FE7D3F593BBCDEB65E913_AdjustorThunk },
	{ 0x0600002A, UIActions_get_enabled_m19088D7D7D446891B6DD16C542B70E24A65CD4E8_AdjustorThunk },
	{ 0x0600002C, UIActions_SetCallbacks_mF059380A033D1DCCBDEEF8FA931A92C7AFABEE2C_AdjustorThunk },
};
static const int32_t s_InvokerIndices[649] = 
{
	3009,
	3079,
	3079,
	2892,
	2376,
	2890,
	2374,
	2901,
	2096,
	3009,
	3009,
	3079,
	3079,
	3127,
	3128,
	2479,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3079,
	3079,
	3039,
	4705,
	2479,
	2479,
	3009,
	3009,
	3079,
	3079,
	3039,
	4706,
	2479,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	2568,
	4875,
	3079,
	3079,
	3079,
	3072,
	3039,
	3072,
	3039,
	3046,
	3046,
	3039,
	3039,
	3039,
	3039,
	3039,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	2462,
	2462,
	2462,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3009,
	3009,
	1399,
	3079,
	3009,
	3009,
	3079,
	3009,
	3009,
	3009,
	2991,
	3009,
	3079,
	3009,
	3079,
	3009,
	3009,
	3079,
	3009,
	3009,
	3009,
	2991,
	3079,
	3079,
	3009,
	3009,
	2991,
	3079,
	3009,
	3079,
	4875,
	3079,
	3079,
	3079,
	2479,
	2479,
	3009,
	3009,
	3009,
	2479,
	3079,
	3079,
	3079,
	3079,
	991,
	3079,
	3079,
	3079,
	2462,
	3039,
	3079,
	3079,
	3039,
	3039,
	2462,
	3079,
	3039,
	3009,
	3079,
	3009,
	2462,
	3079,
	3039,
	3009,
	3079,
	3009,
	3079,
	3079,
	2462,
	3079,
	3039,
	3009,
	3079,
	3009,
	2462,
	3079,
	3039,
	3009,
	3079,
	3009,
	3009,
	3079,
	4875,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	2479,
	3079,
	3079,
	2479,
	3079,
	3079,
	2479,
	2479,
	3079,
	3079,
	2479,
	3079,
	3079,
	3079,
	2479,
	3079,
	3079,
	4875,
	3079,
	3079,
	4875,
	3079,
	3079,
	3079,
	2160,
	2479,
	3079,
	3079,
	3079,
	3079,
	4875,
	3079,
	3079,
	3079,
	2096,
	3079,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	3009,
	3009,
	3009,
	3009,
	3046,
	3046,
	3046,
	3046,
	2991,
	3039,
	3039,
	3039,
	2991,
	2991,
	3079,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3009,
	3079,
	3079,
	3079,
	3079,
	79,
	2479,
	2479,
	2479,
	3079,
	2462,
	3079,
	3079,
	1432,
	571,
	2991,
	2991,
	2991,
	2991,
	2991,
	3079,
	2991,
	3079,
	2991,
	2991,
	2991,
	2991,
	2991,
	2991,
	3079,
	3009,
	3009,
	3009,
	3009,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	2479,
	2462,
	3079,
	3079,
	3079,
	2462,
	3079,
	3079,
	3079,
	2479,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	2479,
	3079,
	3079,
	3079,
	3079,
	3079,
	2479,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	2479,
	3079,
	3079,
	2479,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	4875,
	3079,
	3079,
	3079,
	1399,
	991,
	3079,
	3079,
	3079,
	3079,
	3039,
	4910,
	3079,
	3039,
	2462,
	3079,
	3039,
	3009,
	3079,
	3009,
	3079,
	2479,
	3079,
	3079,
	3079,
	3079,
	3009,
	3009,
	4875,
	3039,
	3079,
	3079,
	3079,
	2479,
	3079,
	2479,
	2479,
	3079,
	3079,
	3079,
	2479,
	1403,
	2479,
	1869,
	1869,
	3009,
	3079,
	2479,
	3079,
	3039,
	3039,
	3039,
	2462,
	3079,
	3039,
	3009,
	3079,
	3009,
	2462,
	3079,
	3039,
	3009,
	3079,
	3009,
	2462,
	3079,
	3039,
	3009,
	3079,
	3009,
	3009,
	3079,
	2479,
	3079,
	3079,
	3079,
	3079,
	4875,
	3079,
	3079,
	2479,
	3079,
	819,
	1432,
	3079,
	3079,
	3079,
	3079,
	2507,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	2479,
	3079,
	3079,
	2462,
	2462,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3039,
	3039,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	2462,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	2462,
	3079,
	3079,
	3079,
	2462,
	2462,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	2462,
	2507,
	3079,
	3079,
	4875,
	3079,
	2462,
	3079,
	1859,
	3009,
	3079,
	3079,
	3079,
	2462,
	3079,
	3039,
	3009,
	3079,
	3009,
	2462,
	3079,
	3039,
	3009,
	3079,
	3009,
	3079,
	3079,
	2514,
	3079,
	3079,
	4875,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	2479,
	3079,
	3079,
	2479,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	1441,
	3079,
	3079,
	2479,
	2479,
	2479,
	2479,
	3079,
	3009,
	2479,
	3079,
	3079,
	3079,
	3079,
	3079,
	1866,
	1406,
	2479,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	1866,
	2541,
	2479,
	2479,
	3079,
	3079,
	3079,
	2479,
	2479,
	2462,
	3079,
	3079,
	3079,
	2479,
	2479,
	2479,
	3079,
	3079,
	2541,
	3079,
	3079,
	3079,
	3079,
	2479,
	3079,
	2479,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	2479,
	2479,
	2479,
	1866,
	2479,
	2479,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3079,
	3009,
	3079,
	3079,
	1866,
	3079,
	1866,
	3079,
	2514,
	3079,
	3039,
	3079,
	1866,
	3079,
	4910,
	3079,
	2126,
	3039,
	2479,
	3079,
	1399,
	3079,
	3039,
	3079,
	1866,
	3079,
	4910,
	3079,
	2126,
	3039,
	2514,
	3079,
	1866,
	3079,
	3039,
	3079,
	1866,
	3079,
	4910,
	3079,
	2126,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x0200002A, { 0, 2 } },
	{ 0x0200002B, { 2, 2 } },
	{ 0x0200002C, { 4, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[6] = 
{
	{ (Il2CppRGCTXDataType)2, 3150 },
	{ (Il2CppRGCTXDataType)2, 540 },
	{ (Il2CppRGCTXDataType)2, 3145 },
	{ (Il2CppRGCTXDataType)2, 536 },
	{ (Il2CppRGCTXDataType)2, 3149 },
	{ (Il2CppRGCTXDataType)2, 538 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	649,
	s_methodPointers,
	27,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	6,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
