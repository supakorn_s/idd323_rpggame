﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861;
// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8;
// System.String
struct String_t;
// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// XNode.Node/InputAttribute
struct InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD;
// XNode.Node/NodeTintAttribute
struct NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99;
// XNode.Node/NodeWidthAttribute
struct NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0;
// XNode.Node/OutputAttribute
struct OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585;

IL2CPP_EXTERN_C const RuntimeType* U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.ContextMenu
struct ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.ContextMenu::menuItem
	String_t* ___menuItem_0;
	// System.Boolean UnityEngine.ContextMenu::validate
	bool ___validate_1;
	// System.Int32 UnityEngine.ContextMenu::priority
	int32_t ___priority_2;

public:
	inline static int32_t get_offset_of_menuItem_0() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___menuItem_0)); }
	inline String_t* get_menuItem_0() const { return ___menuItem_0; }
	inline String_t** get_address_of_menuItem_0() { return &___menuItem_0; }
	inline void set_menuItem_0(String_t* value)
	{
		___menuItem_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___menuItem_0), (void*)value);
	}

	inline static int32_t get_offset_of_validate_1() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___validate_1)); }
	inline bool get_validate_1() const { return ___validate_1; }
	inline bool* get_address_of_validate_1() { return &___validate_1; }
	inline void set_validate_1(bool value)
	{
		___validate_1 = value;
	}

	inline static int32_t get_offset_of_priority_2() { return static_cast<int32_t>(offsetof(ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861, ___priority_2)); }
	inline int32_t get_priority_2() const { return ___priority_2; }
	inline int32_t* get_address_of_priority_2() { return &___priority_2; }
	inline void set_priority_2(int32_t value)
	{
		___priority_2 = value;
	}
};


// UnityEngine.CreateAssetMenuAttribute
struct CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.CreateAssetMenuAttribute::<menuName>k__BackingField
	String_t* ___U3CmenuNameU3Ek__BackingField_0;
	// System.String UnityEngine.CreateAssetMenuAttribute::<fileName>k__BackingField
	String_t* ___U3CfileNameU3Ek__BackingField_1;
	// System.Int32 UnityEngine.CreateAssetMenuAttribute::<order>k__BackingField
	int32_t ___U3CorderU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CmenuNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CmenuNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CmenuNameU3Ek__BackingField_0() const { return ___U3CmenuNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CmenuNameU3Ek__BackingField_0() { return &___U3CmenuNameU3Ek__BackingField_0; }
	inline void set_U3CmenuNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CmenuNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CmenuNameU3Ek__BackingField_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfileNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CfileNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CfileNameU3Ek__BackingField_1() const { return ___U3CfileNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CfileNameU3Ek__BackingField_1() { return &___U3CfileNameU3Ek__BackingField_1; }
	inline void set_U3CfileNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CfileNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CfileNameU3Ek__BackingField_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CorderU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C, ___U3CorderU3Ek__BackingField_2)); }
	inline int32_t get_U3CorderU3Ek__BackingField_2() const { return ___U3CorderU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CorderU3Ek__BackingField_2() { return &___U3CorderU3Ek__BackingField_2; }
	inline void set_U3CorderU3Ek__BackingField_2(int32_t value)
	{
		___U3CorderU3Ek__BackingField_2 = value;
	}
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// XNode.Node/NodeWidthAttribute
struct NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 XNode.Node/NodeWidthAttribute::width
	int32_t ___width_0;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.MultilineAttribute
struct MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.MultilineAttribute::lines
	int32_t ___lines_0;

public:
	inline static int32_t get_offset_of_lines_0() { return static_cast<int32_t>(offsetof(MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291, ___lines_0)); }
	inline int32_t get_lines_0() const { return ___lines_0; }
	inline int32_t* get_address_of_lines_0() { return &___lines_0; }
	inline void set_lines_0(int32_t value)
	{
		___lines_0 = value;
	}
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.SpaceAttribute
struct SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.SpaceAttribute::height
	float ___height_0;

public:
	inline static int32_t get_offset_of_height_0() { return static_cast<int32_t>(offsetof(SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8, ___height_0)); }
	inline float get_height_0() const { return ___height_0; }
	inline float* get_address_of_height_0() { return &___height_0; }
	inline void set_height_0(float value)
	{
		___height_0 = value;
	}
};


// UnityEngine.TextAreaAttribute
struct TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Int32 UnityEngine.TextAreaAttribute::minLines
	int32_t ___minLines_0;
	// System.Int32 UnityEngine.TextAreaAttribute::maxLines
	int32_t ___maxLines_1;

public:
	inline static int32_t get_offset_of_minLines_0() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___minLines_0)); }
	inline int32_t get_minLines_0() const { return ___minLines_0; }
	inline int32_t* get_address_of_minLines_0() { return &___minLines_0; }
	inline void set_minLines_0(int32_t value)
	{
		___minLines_0 = value;
	}

	inline static int32_t get_offset_of_maxLines_1() { return static_cast<int32_t>(offsetof(TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B, ___maxLines_1)); }
	inline int32_t get_maxLines_1() const { return ___maxLines_1; }
	inline int32_t* get_address_of_maxLines_1() { return &___maxLines_1; }
	inline void set_maxLines_1(int32_t value)
	{
		___maxLines_1 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// XNode.Node/ConnectionType
struct ConnectionType_t358038D75191873A2DBDBD5C7C4BC6D3D2FC9B41 
{
public:
	// System.Int32 XNode.Node/ConnectionType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConnectionType_t358038D75191873A2DBDBD5C7C4BC6D3D2FC9B41, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// XNode.Node/NodeTintAttribute
struct NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// UnityEngine.Color XNode.Node/NodeTintAttribute::color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___color_0;

public:
	inline static int32_t get_offset_of_color_0() { return static_cast<int32_t>(offsetof(NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99, ___color_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_color_0() const { return ___color_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_color_0() { return &___color_0; }
	inline void set_color_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___color_0 = value;
	}
};


// XNode.Node/ShowBackingValue
struct ShowBackingValue_t39EB6A46698E21FA7FEAC466A96A0C633CEDF296 
{
public:
	// System.Int32 XNode.Node/ShowBackingValue::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ShowBackingValue_t39EB6A46698E21FA7FEAC466A96A0C633CEDF296, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// XNode.Node/TypeConstraint
struct TypeConstraint_t131F0C4500E3FBE3F6BDC2191BD581C471782ACF 
{
public:
	// System.Int32 XNode.Node/TypeConstraint::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeConstraint_t131F0C4500E3FBE3F6BDC2191BD581C471782ACF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};


// XNode.Node/InputAttribute
struct InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// XNode.Node/ShowBackingValue XNode.Node/InputAttribute::backingValue
	int32_t ___backingValue_0;
	// XNode.Node/ConnectionType XNode.Node/InputAttribute::connectionType
	int32_t ___connectionType_1;
	// System.Boolean XNode.Node/InputAttribute::dynamicPortList
	bool ___dynamicPortList_2;
	// XNode.Node/TypeConstraint XNode.Node/InputAttribute::typeConstraint
	int32_t ___typeConstraint_3;

public:
	inline static int32_t get_offset_of_backingValue_0() { return static_cast<int32_t>(offsetof(InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD, ___backingValue_0)); }
	inline int32_t get_backingValue_0() const { return ___backingValue_0; }
	inline int32_t* get_address_of_backingValue_0() { return &___backingValue_0; }
	inline void set_backingValue_0(int32_t value)
	{
		___backingValue_0 = value;
	}

	inline static int32_t get_offset_of_connectionType_1() { return static_cast<int32_t>(offsetof(InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD, ___connectionType_1)); }
	inline int32_t get_connectionType_1() const { return ___connectionType_1; }
	inline int32_t* get_address_of_connectionType_1() { return &___connectionType_1; }
	inline void set_connectionType_1(int32_t value)
	{
		___connectionType_1 = value;
	}

	inline static int32_t get_offset_of_dynamicPortList_2() { return static_cast<int32_t>(offsetof(InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD, ___dynamicPortList_2)); }
	inline bool get_dynamicPortList_2() const { return ___dynamicPortList_2; }
	inline bool* get_address_of_dynamicPortList_2() { return &___dynamicPortList_2; }
	inline void set_dynamicPortList_2(bool value)
	{
		___dynamicPortList_2 = value;
	}

	inline static int32_t get_offset_of_typeConstraint_3() { return static_cast<int32_t>(offsetof(InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD, ___typeConstraint_3)); }
	inline int32_t get_typeConstraint_3() const { return ___typeConstraint_3; }
	inline int32_t* get_address_of_typeConstraint_3() { return &___typeConstraint_3; }
	inline void set_typeConstraint_3(int32_t value)
	{
		___typeConstraint_3 = value;
	}
};


// XNode.Node/OutputAttribute
struct OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// XNode.Node/ShowBackingValue XNode.Node/OutputAttribute::backingValue
	int32_t ___backingValue_0;
	// XNode.Node/ConnectionType XNode.Node/OutputAttribute::connectionType
	int32_t ___connectionType_1;
	// System.Boolean XNode.Node/OutputAttribute::dynamicPortList
	bool ___dynamicPortList_2;
	// XNode.Node/TypeConstraint XNode.Node/OutputAttribute::typeConstraint
	int32_t ___typeConstraint_3;

public:
	inline static int32_t get_offset_of_backingValue_0() { return static_cast<int32_t>(offsetof(OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585, ___backingValue_0)); }
	inline int32_t get_backingValue_0() const { return ___backingValue_0; }
	inline int32_t* get_address_of_backingValue_0() { return &___backingValue_0; }
	inline void set_backingValue_0(int32_t value)
	{
		___backingValue_0 = value;
	}

	inline static int32_t get_offset_of_connectionType_1() { return static_cast<int32_t>(offsetof(OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585, ___connectionType_1)); }
	inline int32_t get_connectionType_1() const { return ___connectionType_1; }
	inline int32_t* get_address_of_connectionType_1() { return &___connectionType_1; }
	inline void set_connectionType_1(int32_t value)
	{
		___connectionType_1 = value;
	}

	inline static int32_t get_offset_of_dynamicPortList_2() { return static_cast<int32_t>(offsetof(OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585, ___dynamicPortList_2)); }
	inline bool get_dynamicPortList_2() const { return ___dynamicPortList_2; }
	inline bool* get_address_of_dynamicPortList_2() { return &___dynamicPortList_2; }
	inline void set_dynamicPortList_2(bool value)
	{
		___dynamicPortList_2 = value;
	}

	inline static int32_t get_offset_of_typeConstraint_3() { return static_cast<int32_t>(offsetof(OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585, ___typeConstraint_3)); }
	inline int32_t get_typeConstraint_3() const { return ___typeConstraint_3; }
	inline int32_t* get_address_of_typeConstraint_3() { return &___typeConstraint_3; }
	inline void set_typeConstraint_3(int32_t value)
	{
		___typeConstraint_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65 (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_fileName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_menuName(System.String)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.CreateAssetMenuAttribute::set_order(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MultilineAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MultilineAttribute__ctor_m10153ED887A12FCB49824481A8C7E7BD87554338 (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * __this, int32_t ___lines0, const RuntimeMethod* method);
// System.Void XNode.Node/InputAttribute::.ctor(XNode.Node/ShowBackingValue,XNode.Node/ConnectionType,XNode.Node/TypeConstraint,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15 (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * __this, int32_t ___backingValue0, int32_t ___connectionType1, int32_t ___typeConstraint2, bool ___dynamicPortList3, const RuntimeMethod* method);
// System.Void XNode.Node/OutputAttribute::.ctor(XNode.Node/ShowBackingValue,XNode.Node/ConnectionType,XNode.Node/TypeConstraint,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70 (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * __this, int32_t ___backingValue0, int32_t ___connectionType1, int32_t ___typeConstraint2, bool ___dynamicPortList3, const RuntimeMethod* method);
// System.Void UnityEngine.TextAreaAttribute::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042 (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * __this, int32_t ___minLines0, int32_t ___maxLines1, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TextAreaAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2 (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * __this, const RuntimeMethod* method);
// System.Void UnityEngine.ContextMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * __this, String_t* ___itemName0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void XNode.Node/NodeWidthAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NodeWidthAttribute__ctor_m16DDD2A682E4AC137EA63B96FD229DB129A2C661 (NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0 * __this, int32_t ___width0, const RuntimeMethod* method);
// System.Void XNode.Node/NodeTintAttribute::.ctor(System.Byte,System.Byte,System.Byte)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NodeTintAttribute__ctor_mD1F2FF56A82C04DFCFBF9517326CD1753FD0E938 (NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99 * __this, uint8_t ___r0, uint8_t ___g1, uint8_t ___b2, const RuntimeMethod* method);
// System.Void UnityEngine.SpaceAttribute::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44 (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * __this, float ___height0, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void Input_master_t9B5B284DF340547BAD045C91B46416A71BCB697A_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Input_master_t9B5B284DF340547BAD045C91B46416A71BCB697A_CustomAttributesCacheGenerator_Input_master_get_asset_mB578FB991F84D0700B5FA9B4B34C7048BE5637EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__debugTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__subtabList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__playerBattle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__enemyBattle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__decreaseButton(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x20\x45\x64\x69\x74\x6F\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__increaseButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__shownStat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__playerImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__playerName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__level(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__skillPoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__vitality(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__intelligence(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__design(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__code(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__defence(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__HP(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__currentHP(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__idea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__logic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__baseAttack(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__baseDefence(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__expNeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__expHave(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass26_0_tAC6175CBA9B5AACB71FE303225EF33E5D2DC140A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass26_1_tE55916711493150BA6439E31899F48C49C709263_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NarrationCharacter_tBA8729EA20561C739EC5E76A192BF2CBEFC17670_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x72\x61\x63\x74\x65\x72"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x72\x72\x61\x74\x69\x6F\x6E\x2F\x43\x68\x61\x72\x61\x63\x74\x65\x72"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 1LL, NULL);
	}
}
static void NarrationCharacter_tBA8729EA20561C739EC5E76A192BF2CBEFC17670_CustomAttributesCacheGenerator__characterSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NarrationCharacter_tBA8729EA20561C739EC5E76A192BF2CBEFC17670_CustomAttributesCacheGenerator__characterName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NarrationLine_t3935F28CA0A06164780E17468DD3C09F5C1F757C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x72\x72\x61\x74\x69\x6F\x6E\x20\x4C\x69\x6E\x65"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x72\x72\x61\x74\x69\x6F\x6E\x2F\x4C\x69\x6E\x65"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 0LL, NULL);
	}
}
static void NarrationLine_t3935F28CA0A06164780E17468DD3C09F5C1F757C_CustomAttributesCacheGenerator__speaker(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NarrationLine_t3935F28CA0A06164780E17468DD3C09F5C1F757C_CustomAttributesCacheGenerator__text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 * tmp = (MultilineAttribute_tA4DCA9D8519E0C1B05636F2EFF74F81335A7F291 *)cache->attributes[1];
		MultilineAttribute__ctor_m10153ED887A12FCB49824481A8C7E7BD87554338(tmp, 4LL, NULL);
	}
}
static void BattleCutIn_tFFB78E274031C78B90FDCC7C8D9C7B942F003C51_CustomAttributesCacheGenerator_entry(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void BattleNode_tD6C18FD0F24D20A2719714A093B17E0A27A11664_CustomAttributesCacheGenerator_entry(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator_entry(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator_exit(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
		tmp->set_dynamicPortList_2(true);
	}
}
static void ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator__character(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x61\x6C\x6F\x67\x75\x65\x20\x49\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator__dialogueLine(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 2LL, 15LL, NULL);
	}
}
static void ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator__choiceLine(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 1LL, 5LL, NULL);
	}
}
static void ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator_nextOutputPriority(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void DialogueGraph_tC8F98D469C95CBDA498A9A1FA38AA1170467EC61_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
	}
}
static void DialogueNode_tCB0329F5EBE20CA1236E0D713DEB7A4C5EAF292B_CustomAttributesCacheGenerator_entry(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void DialogueNode_tCB0329F5EBE20CA1236E0D713DEB7A4C5EAF292B_CustomAttributesCacheGenerator_exit(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
		tmp->set_dynamicPortList_2(true);
	}
}
static void DialogueNode_tCB0329F5EBE20CA1236E0D713DEB7A4C5EAF292B_CustomAttributesCacheGenerator__character(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x61\x6C\x6F\x67\x75\x65\x20\x49\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void DialogueNode_tCB0329F5EBE20CA1236E0D713DEB7A4C5EAF292B_CustomAttributesCacheGenerator__dialogueLine(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mE6205039C7C59B1F274B18D33E5CD9C22C18B042(tmp, 5LL, 20LL, NULL);
	}
}
static void DialogueNode_tCB0329F5EBE20CA1236E0D713DEB7A4C5EAF292B_CustomAttributesCacheGenerator_nextOutputPriority(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void EndNode_t3993962364AE73BE3E7DB6DA2B5A34911C2D0D05_CustomAttributesCacheGenerator_entry(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_graph(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x75\x72\x72\x65\x6E\x74\x6C\x79\x20\x70\x6C\x61\x79\x65\x64\x20\x64\x69\x61\x6C\x6F\x67\x75\x65"), NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__dialogueCanvas(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__interactionCanvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__choiceCanvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__choiceSpawnPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__dialogueChoicePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__dialogueAnim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__playerController(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__typingAudioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F"), NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__typingSound(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_ParserNode_m3EAB881B5ECB17F0B0BDD84C5CA93EEFF15B1E30(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_0_0_0_var), NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_CustomDialogue_mDA2FFD006849C2851B90F8E19927B5E4E409D418(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_0_0_0_var), NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_ParseChoice_mE64E48CC0F78A2DE7D4257483A4347453929C73D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_0_0_0_var), NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_TypeSentence_mE846AFE4DD1D8B4E9B5E084E82823DC410D03762(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_0_0_0_var), NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_U3CParseChoiceU3Eb__32_0_mE9CB54451F924F79F3AC08623838E478D66D7A2C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_U3CParseChoiceU3Eb__32_1_m967C24CA1384A386651E4401E8B1E3C850BBF394(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator_U3CParserNodeU3Ed__30__ctor_m382DAC51BDF1174D3212B4AF0B0D9DDDD122A329(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator_U3CParserNodeU3Ed__30_System_IDisposable_Dispose_m14EB0FDDF590E4BDFF1FD8AC581D9AE22B70729C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator_U3CParserNodeU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE06E05FE75F35AB048964B3D36B60A3B74A7F33(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator_U3CParserNodeU3Ed__30_System_Collections_IEnumerator_Reset_m0CDB444124BB417931E15B820C3C9D9ABE915800(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator_U3CParserNodeU3Ed__30_System_Collections_IEnumerator_get_Current_m5599AFCFFBBF462A44F42C2F1DAA399D2082E1AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator_U3CCustomDialogueU3Ed__31__ctor_m539D0D53D0C9852C2AA92851832BBA1F93FE5215(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator_U3CCustomDialogueU3Ed__31_System_IDisposable_Dispose_m05AB132BB1C96C406CB4E6FB832AA9310CAF9159(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator_U3CCustomDialogueU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB652CBA767F5E7D4BF9661F5D60318DA0C3562DF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator_U3CCustomDialogueU3Ed__31_System_Collections_IEnumerator_Reset_m174F4229540442B55B3F7CCCDD051E832E908526(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator_U3CCustomDialogueU3Ed__31_System_Collections_IEnumerator_get_Current_m039BFABC311EAD1031D0A8D7D1E90260AE943B8C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass32_0_tBC908EE6071787695F1125EB4CB7995011986FC3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator_U3CParseChoiceU3Ed__32__ctor_m8CD7150757546CD2F3182C33BBAE91519AAB9000(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator_U3CParseChoiceU3Ed__32_System_IDisposable_Dispose_mC62B6A94C7B7FEAA59D9B2BF082E33A1E28A1293(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator_U3CParseChoiceU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F3C38F7EFC22E10D93A71F5136ABE6343D8E287(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator_U3CParseChoiceU3Ed__32_System_Collections_IEnumerator_Reset_m45250654797E7892C0D72ECDBDF79A12C230ABBF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator_U3CParseChoiceU3Ed__32_System_Collections_IEnumerator_get_Current_m34989581633799DDA06EE8408DA1FF9B977EED46(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__38__ctor_mD2CE5896F92C199BFA6B203DC27462B78C8D2FBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__38_System_IDisposable_Dispose_m3B08D4B564EB130E4D1160752C31C799AEBC00FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C44140AB27A4C7204C12F1564FC47CCFDAC8C69(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__38_System_Collections_IEnumerator_Reset_mA7053DC759C764A5E1E71EF04AED17866CA1575B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__38_System_Collections_IEnumerator_get_Current_mE7624AC3A9AFB6B227EC4D69375762C83BB9FB64(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void StartNode_tA0992E833D804E15A011293A0FBB5DE9277B0FD8_CustomAttributesCacheGenerator_exit(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
}
static void NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator_state(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x20\x53\x74\x61\x74\x65"), NULL);
	}
}
static void NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator__unitProfile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74\x20\x50\x72\x6F\x66\x69\x6C\x65"), NULL);
	}
}
static void NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator__name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
}
static void NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator__unitSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator__npcName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator__instruction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator_inInteractRange(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator_isDefeated(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x41\x54\x54\x4C\x45"), NULL);
	}
}
static void ObjectiveInfoSO_tAD54B01D79A962AFFE6BB90ED5E1FC33B7E62BE9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x6A\x65\x63\x74\x69\x76\x65\x20\x50\x72\x6F\x66\x69\x6C\x65"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x4F\x2F\x4F\x62\x6A\x65\x63\x74\x69\x76\x65\x2F\x53\x69\x6D\x70\x6C\x65\x20\x4F\x62\x6A\x65\x63\x74\x69\x76\x65"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 0LL, NULL);
	}
}
static void ObjectiveInfoSO_tAD54B01D79A962AFFE6BB90ED5E1FC33B7E62BE9_CustomAttributesCacheGenerator__objectiveId(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectiveInfoSO_tAD54B01D79A962AFFE6BB90ED5E1FC33B7E62BE9_CustomAttributesCacheGenerator_targetNPC(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectiveInfoSO_tAD54B01D79A962AFFE6BB90ED5E1FC33B7E62BE9_CustomAttributesCacheGenerator_objectiveName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ObjectiveInfoSO_tAD54B01D79A962AFFE6BB90ED5E1FC33B7E62BE9_CustomAttributesCacheGenerator_objectiveDescription(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[1];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
}
static void ObjectiveManager_t325E44CD23ED56F232807D8E46C81BBA3656ADF8_CustomAttributesCacheGenerator__debugObjectiveList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RuntimeObjective_t6EC87A02C01725E1F309E101A31BAB602EB164DE_CustomAttributesCacheGenerator__info(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerBrain_t8E1560D2B1590AF0ABC355B852E846F3FC4A0A47_CustomAttributesCacheGenerator_playerProfile(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72\x20\x43\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x4C\x69\x73\x74\x73"), NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_state(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72\x20\x53\x74\x61\x74\x65"), NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72\x20\x53\x74\x61\x74"), NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__movementDelay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__nextMovePoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__animator(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__idleTrigger(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__walking(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playingAnimation(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__audioSource(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__walkingSound(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__groundTilemap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6C\x65\x6D\x61\x70\x20\x61\x6E\x64\x20\x43\x6F\x6C\x6C\x69\x73\x69\x6F\x6E\x20\x43\x68\x65\x63\x6B\x65\x72"), NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__collisionTilemap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__collisionObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerModel_t1E6011AEEA89127DBFA99274CA47CEE13F7E7340_CustomAttributesCacheGenerator__playerSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerMouseDetection_t538CCC17BB18B68A59D32B4E6A0BB9443DEF165A_CustomAttributesCacheGenerator__selectable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PlayerMouseDetection_t538CCC17BB18B68A59D32B4E6A0BB9443DEF165A_CustomAttributesCacheGenerator__poitedObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x41\x74\x74\x61\x63\x6B\x5F\x6E\x61\x6D\x65\x5F\x61\x74\x74\x61\x63\x6B"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x4F\x2F\x50\x72\x6F\x66\x69\x6C\x65\x2F\x41\x74\x74\x61\x63\x6B"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 0LL, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__attackName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x6C\x20\x49\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__attackDescription(CustomAttributesCache* cache)
{
	{
		TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B * tmp = (TextAreaAttribute_t22F900CF759A0162A0C51120E646C11E10586A9B *)cache->attributes[0];
		TextAreaAttribute__ctor_mAA125EAD5301EB23E2BDC1E1E2244D1F9F9763F2(tmp, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__attackIcon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__attackAnim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__staminaCost(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__ideaCost(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__logicCost(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__damage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__critRate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__heal(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__isDesignSkill(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__isCodeSkill(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__healRate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__healAmount(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x6F\x66\x69\x6C\x65\x5F\x4E\x61\x6D\x65"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x53\x4F\x2F\x50\x72\x6F\x66\x69\x6C\x65\x2F\x55\x6E\x69\x74"), NULL);
		CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline(tmp, 0LL, NULL);
	}
}
static void UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__unitName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x6C\x20\x49\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__unitSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__stat(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x72\x61\x63\x74\x65\x72\x20\x53\x74\x61\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__attackProfile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x74\x74\x61\x63\x6B\x20\x50\x72\x6F\x66\x69\x6C\x65"), NULL);
	}
}
static void UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__skillProfile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__startingDialogue(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x69\x6E\x67\x20\x44\x69\x61\x6C\x6F\x67\x75\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__introBattle(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x61\x74\x74\x6C\x65\x20\x44\x69\x61\x6C\x6F\x67\x75\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__reward(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x28\x45\x6E\x65\x6D\x79\x29\x20\x52\x65\x77\x61\x72\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator_UnitProfile_ResettingFightingStat_m8ACAC82FA2F0D84722A15EF6F9642A4D72306321(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x74\x69\x6E\x67\x20\x46\x69\x67\x68\x74\x69\x6E\x67\x20\x53\x74\x61\x74\x20\x54\x65\x6D\x70\x6F\x72\x61\x72\x79\x20\x53\x74\x61\x74"), NULL);
	}
}
static void UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator_UnitProfile_DefaultStartingLevel_m4BFFDA1B009B2A2AFC96C61A77994A98F7A11FE8(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x65\x74\x20\x4C\x65\x76\x65\x6C"), NULL);
	}
}
static void CharacterStat_t80447BD100798673C855508DC8ED1D045CBF7099_CustomAttributesCacheGenerator_currentLevel(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x72\x61\x63\x74\x65\x72\x20\x53\x74\x61\x74"), NULL);
	}
}
static void CharacterStat_t80447BD100798673C855508DC8ED1D045CBF7099_CustomAttributesCacheGenerator_skillPoint(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x4B\x49\x4C\x4C\x20\x50\x4F\x49\x4E\x54\x53"), NULL);
	}
}
static void CharacterStat_t80447BD100798673C855508DC8ED1D045CBF7099_CustomAttributesCacheGenerator__levelingstat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x76\x65\x6C\x20\x75\x70\x20\x65\x78\x74\x72\x61"), NULL);
	}
}
static void Reward_t8DCC1AF39C20757EFE52BB3FE94BCD345F815EE0_CustomAttributesCacheGenerator__rewardEXP(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__increaseBaseHP(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__increaseBaseIdea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__increaseBaseLogic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__increaseBaseAtk(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__increaseBaseDef(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__skillPoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmSource(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x53\x6F\x75\x72\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__sfxSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__onClick(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49\x20\x53\x6F\x75\x6E\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__onCancelled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__onHover(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__battleBGMSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x57\x6F\x72\x6C\x64\x20\x53\x6F\x75\x6E\x64"), NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__worldBGM(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__battleBGM(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__battleSFX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__healingSFX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__fadingSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x64\x69\x6E\x67"), NULL);
	}
}
static void SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator_fading(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ChangeProfileEvent_tC482FC50729070C875B58E7FB002744948AE1C7B_CustomAttributesCacheGenerator__newDialogue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GettingNewSkillEvent_t2FA3772BD6D1C8E440355A51C208B8390CC27037_CustomAttributesCacheGenerator__dialogueToPlay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GettingNewSkillEvent_t2FA3772BD6D1C8E440355A51C208B8390CC27037_CustomAttributesCacheGenerator__newSkill(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HealBox_tEC2FB6743B667A64563259C8802FD7B6D83AD304_CustomAttributesCacheGenerator__resetEverything(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveEvent_tD585745BA89C8BA14E4BEBF6B63C7D02FEC85809_CustomAttributesCacheGenerator__targetLocation(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TriggerDialogue_t1A321E3FB424BB0738882500D4DAE4B27EF938A5_CustomAttributesCacheGenerator__dialogueToPlay(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TriggerMusic_t8714EB0F65F7A5025D4F8B72C478E5FB66B97544_CustomAttributesCacheGenerator__newBGM(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator__attackIcon(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator__attackName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator__attackButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator_ActionButton_U3CButtonSetupU3Eb__4_0_m08761867745A1895E5FC24E48212246605E32BA7(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator_ActionButton_U3CButtonSetupU3Eb__4_1_m5FF477DF8FAFE5AEC5C37EAF80E03DF9C1E1F497(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator_ActionButton_U3CButtonSetupU3Eb__4_2_m95AF25B868B387888CAE76BAA0C58A74D909B80A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator_ActionButton_U3CButtonSetupU3Eb__4_3_mD693DC64E13A8671086D2579FDE4F3CAA841305B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BattleDialogue_t0B717574AC7850D2D3F212FC5EFE5E914CC1CD68_CustomAttributesCacheGenerator__dialogueCanvas(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleDialogue_t0B717574AC7850D2D3F212FC5EFE5E914CC1CD68_CustomAttributesCacheGenerator__dialogueAnim(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BattleDialogue_t0B717574AC7850D2D3F212FC5EFE5E914CC1CD68_CustomAttributesCacheGenerator_BattleDialogue_TypeSentence_m03DBCF9A9051983E515A2A9D99379FFA2B616BE0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_0_0_0_var), NULL);
	}
}
static void BattleDialogue_t0B717574AC7850D2D3F212FC5EFE5E914CC1CD68_CustomAttributesCacheGenerator_BattleDialogue_U3CTypeSentenceU3Eb__15_0_m458E168B5080C9C7915C6C32DE32D5F131C05942(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tA6E8292C2B3E475DA239A7AA3EA5A22543F7BD37_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__15__ctor_m714D8743D196B43AECF81689E5212CEE2D292FA2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__15_System_IDisposable_Dispose_m9FAED1ED91495E5D1C54C55022788CE9C1880A6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BD268DFD0A34C14F5343375B8F74AE693EB2495(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__15_System_Collections_IEnumerator_Reset_mD678F10373F8B0D8D78BE91742776FF56EC4654F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__15_System_Collections_IEnumerator_get_Current_m270C4F3E2E87C7235ED7F7457E25DEA8695791B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator_currentProfile(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x6C\x20\x49\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__unitName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__unitLevel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__maxHP(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__maxIdea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__maxLogic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__unitProfile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__attackProfile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__level(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__hp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__idea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__logic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__unitImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__HPSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__ideaSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__logicSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_state(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x75\x72\x6E\x20\x53\x74\x61\x74\x65"), NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__enemyUnit(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x69\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__playerUnit(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__currentTargetNPC(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__battleCanvas(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6E\x76\x61\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__actionTab(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72\x20\x48\x75\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__attackTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__resultTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__playerController(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72"), NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__animator(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x69\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_SetupBattle_mA8A505D87F206EAF8C6FF1BF7EA4EE030859E683(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x75\x70\x42\x61\x74\x74\x6C\x65"), NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_EndBattle_m04FE104E66DD5B7603A5E7E310019698B54FA902(CustomAttributesCache* cache)
{
	{
		ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 * tmp = (ContextMenu_tA743E775BCF043B77AB6D4872E90FC4D7AE8E861 *)cache->attributes[0];
		ContextMenu__ctor_m8A50A6FFBB330E0DB6F6587FBAAA7850D8DBDA7B(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x64\x42\x61\x74\x74\x6C\x65"), NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_PlayerTurn_m64279C163C3632B41265E29A38CE186760934439(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_0_0_0_var), NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_PlayerHealTurn_mE022E32CFA0E9B5A2CDFA97F4F6F3C7C02031F7E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_0_0_0_var), NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_EnemyTurn_m953332AC41E491EA08D064B7566E08E5211C6076(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_0_0_0_var), NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_U3CPlayerTurnU3Eb__35_0_m71013D9FECDC342FF5202C42E881593D8CB8ECA2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_U3CPlayerHealTurnU3Eb__36_0_m4F6A8805E871F063CB75F553296744F0FFD78D5F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_U3CEnemyTurnU3Eb__37_0_m6587319AD0E3D72F409E682B319B063AAC5E1A6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator_U3CPlayerTurnU3Ed__35__ctor_m8AB2530F00AAC23868A1014008E4510CC8A85E7B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator_U3CPlayerTurnU3Ed__35_System_IDisposable_Dispose_mBA54B120599E2137E5393768D6120030E95A336B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator_U3CPlayerTurnU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61EACE996BC752C1364CCA92B5C2166D4123E2AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator_U3CPlayerTurnU3Ed__35_System_Collections_IEnumerator_Reset_mE2DDF9EF573EE8506B67333FD381EC49558D4D04(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator_U3CPlayerTurnU3Ed__35_System_Collections_IEnumerator_get_Current_m66E0053278E42810479BE9E520E40D27F246FAC3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator_U3CPlayerHealTurnU3Ed__36__ctor_m16FD053EDC1AA07B2FEFB29F67B4AA7000E791F2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator_U3CPlayerHealTurnU3Ed__36_System_IDisposable_Dispose_m93A030B3E7AF0DCF29F04DFA1A1047D57632DA42(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator_U3CPlayerHealTurnU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA71D3FD00986769C1CECB82B8711E700D8430D99(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator_U3CPlayerHealTurnU3Ed__36_System_Collections_IEnumerator_Reset_mC26A5D24136DEF19D8186666D9610B2B419215E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator_U3CPlayerHealTurnU3Ed__36_System_Collections_IEnumerator_get_Current_m0F726B46089D8ACB32B00F591BE0D9B9361BE751(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator_U3CEnemyTurnU3Ed__37__ctor_m9E7906BD17A2D26440DDBE0432B224E44AF8DAFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator_U3CEnemyTurnU3Ed__37_System_IDisposable_Dispose_m4DDADCDAB60E4E76F6BF71E63260855719A5B556(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator_U3CEnemyTurnU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7E55AF6A8D378D610248C15AABB1DFB37EA4FF6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator_U3CEnemyTurnU3Ed__37_System_Collections_IEnumerator_Reset_m37EA51052F303CAEB8428D0A0BEA66936454B4DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator_U3CEnemyTurnU3Ed__37_System_Collections_IEnumerator_get_Current_mBFE15F4A828D23A7AA24CF09039B4D93C3870896(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator_currentProfile(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x6C\x20\x49\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__unitName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__unitLevel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__maxHP(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__maxStamina(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__unitProfile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__attackProfile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__level(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__hp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__unitImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__HPSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__actionTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72\x20\x48\x75\x64"), NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__resultTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackTab(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x74\x74\x61\x63\x6B\x20\x44\x65\x74\x61\x69\x6C\x20\x54\x61\x62"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackDescription(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__ideaUsage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__logicUsage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackProfile(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__buttonSpawnPosition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__resultingTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x75\x6C\x74\x20\x54\x61\x62"), NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__expGain(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__currentLevel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__defeatEXPFactor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_CustomAttributesCacheGenerator_panSpeed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61\x20\x53\x65\x74\x74\x69\x6E\x67"), NULL);
	}
}
static void CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_CustomAttributesCacheGenerator_scrollLimitMin(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 50.0f, NULL);
	}
}
static void CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_CustomAttributesCacheGenerator_scrollLimitMax(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 5.0f, 50.0f, NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__basePlayerProfile(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x61\x76\x65\x20\x73\x6C\x6F\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__playerProfileSlot(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__characterName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x72\x61\x63\x74\x65\x72\x20\x63\x72\x65\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__characterImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__characterSprite(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__startingSkillPoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x72\x61\x63\x74\x65\x72\x20\x53\x6B\x69\x6C\x6C\x20\x61\x6E\x64\x20\x53\x74\x61\x74"), NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__currentSkillPoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__totalSkillPoints(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__numberOfStat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__statNumber(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__increaseButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__decreaseButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass22_0_tC229D66EB8530744B31CF7710EF5ED9D26152CA9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__startingPosition(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x6D\x65\x72\x61\x20\x53\x65\x74\x74\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__margin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__smooth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__minBorder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__maxBorder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__targetToFollow(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__mainMenu(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x65\x6E\x75\x20\x4C\x69\x73\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__characterCreationTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__settingMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__creditMenu(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__specialObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__resolutionSetting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__isWebBuild(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x55\x49\x4C\x44\x2F\x57\x45\x42"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__tabList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__specialObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__header(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__MenuTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__characterTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__characterStatTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__SettingTab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__resolutionSetting(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__isWebBuild(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x55\x49\x4C\x44\x2F\x57\x45\x42"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__playerImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__currentStat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__skillPoint(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__playerName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__vitality(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__intelligence(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__design(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__code(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__defence(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__level(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__HP(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__currentHP(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__idea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__logic(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__baseAttack(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__baseDefence(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__expNeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__expHave(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__increaseButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__shownStat(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass30_0_t8167D88381A49B98670A289746758B696C3D6350_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ResolutionSetting_t65491F1B4FCB2E7328CED93748384ADF87E72663_CustomAttributesCacheGenerator_resolutionDropDown(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x73\x6F\x6C\x75\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_transition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_image(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_transitiontime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_SceneLoader_loadlevel_m04CF2C85AEFC903C21B789AD1825E90000E5DC97(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_0_0_0_var), NULL);
	}
}
static void SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_SceneLoader_exitGame_m2924FA892A01B56DE948D8583EC159DCB0DD1D36(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_0_0_0_var), NULL);
	}
}
static void U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__10__ctor_mAE58B80B195BBFA7510F6E68EA9A1298361DBBA4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__10_System_IDisposable_Dispose_m860B69D84C70B4F5896588A3D6F8064C6960C632(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DC83A18DF8FF983CA0BF345C8B581996D42D402(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__10_System_Collections_IEnumerator_Reset_m51A5262162ECD04D7DE7A5F02E29160606469126(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__10_System_Collections_IEnumerator_get_Current_m3AD12E1478791BEAA33BB99964E9329A73384BD6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator_U3CexitGameU3Ed__11__ctor_mFF73E161755102B656F61250D9D576BE9A096C0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator_U3CexitGameU3Ed__11_System_IDisposable_Dispose_mE0A07690D50D877F2029B4AAD833CBBDF8B5FAD8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator_U3CexitGameU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFEE4287F4E70EF27C575F348CABF960F5CEB4DCC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator_U3CexitGameU3Ed__11_System_Collections_IEnumerator_Reset_m42D8867845452E08B152564A3AD4191A22C79851(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator_U3CexitGameU3Ed__11_System_Collections_IEnumerator_get_Current_mB57F50DA13B68471F8331091EEDAAC4788CFF4C3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator_soundForce(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x56\x6F\x6C\x75\x6D\x65\x20\x43\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__audioMixer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__slider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__ExposerString(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__audioSource(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x73\x74\x69\x6E\x67\x20\x53\x6F\x75\x6E\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__testSound(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__NPCToWin(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__winCanvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__objectName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x6C\x20\x49\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__objectInstruction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__instructionText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__npcName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__instruction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator_inInteractRange(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__containDialogue(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x6A\x65\x63\x74\x20\x49\x6E\x66\x6F\x72\x6D\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__isDoor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__isSceneTeleporter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__healPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__loadScene(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__playDialogue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__playerPointer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__destination(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__audioSource(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F"), NULL);
	}
}
static void WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__sound(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MathGraph_tF8675A1D5FFF6B80976105FDB4B5D5690A6A36D4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x4D\x61\x74\x68\x20\x47\x72\x61\x70\x68"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x78\x4E\x6F\x64\x65\x20\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4D\x61\x74\x68\x20\x47\x72\x61\x70\x68"), NULL);
	}
}
static void StateNode_t67B70094FF4EC074C8D00857773B6B38F074C576_CustomAttributesCacheGenerator_enter(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void StateNode_t67B70094FF4EC074C8D00857773B6B38F074C576_CustomAttributesCacheGenerator_exit(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
}
static void StateGraph_tDA7B8CAF1B24A0DA9C18A5F878B7CCEC2B676AF0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x53\x74\x61\x74\x65\x20\x47\x72\x61\x70\x68"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x78\x4E\x6F\x64\x65\x20\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x53\x74\x61\x74\x65\x20\x47\x72\x61\x70\x68"), NULL);
	}
}
static void RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_graph(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x72\x61\x70\x68"), NULL);
	}
}
static void RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_runtimeMathNodePrefab(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62\x73"), NULL);
	}
}
static void RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_graphContextMenu(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x73"), NULL);
	}
}
static void RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_U3CscrollRectU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_RuntimeMathGraph_get_scrollRect_m69128FFC7B3A2FCA86348549F47FD80E5CFEE579(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_RuntimeMathGraph_set_scrollRect_m1986940E63F05E28544767835AB2A62B823F9163(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UGUIMathBaseNode_t1151FCC3BCCC1BBFB86F553282380873605D559F_CustomAttributesCacheGenerator_node(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void UGUIMathBaseNode_t1151FCC3BCCC1BBFB86F553282380873605D559F_CustomAttributesCacheGenerator_graph(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void UGUIContextMenu_t1C011C797890A8ADA77F88D2621FE1C7157ED5B0_CustomAttributesCacheGenerator_selectedNode(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void UGUIPort_t7C023A7AAB1E206E3A9D516BC97926EDDDE3641A_CustomAttributesCacheGenerator_node(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void DisplayValue_tE369A7D7747D59F07948A207CC43A67D4F51CFEF_CustomAttributesCacheGenerator_input(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 0LL, 1LL, 0LL, false, NULL);
	}
}
static void MathNode_t6D322F810C43F9E03A41614FE98C1C9BA7F3292D_CustomAttributesCacheGenerator_a(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void MathNode_t6D322F810C43F9E03A41614FE98C1C9BA7F3292D_CustomAttributesCacheGenerator_b(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void MathNode_t6D322F810C43F9E03A41614FE98C1C9BA7F3292D_CustomAttributesCacheGenerator_result(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
}
static void Vector_t1B855FE635007476062AFB77108E1E0BD925DD4F_CustomAttributesCacheGenerator_x(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void Vector_t1B855FE635007476062AFB77108E1E0BD925DD4F_CustomAttributesCacheGenerator_y(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void Vector_t1B855FE635007476062AFB77108E1E0BD925DD4F_CustomAttributesCacheGenerator_z(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void Vector_t1B855FE635007476062AFB77108E1E0BD925DD4F_CustomAttributesCacheGenerator_vector(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
}
static void LogicGraph_t400DF7927DCC5A722713C073E9C361DD8A154482_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * tmp = (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C *)cache->attributes[0];
		CreateAssetMenuAttribute__ctor_mF4754D0F74BACF7BB7DAC67F46690A69256D2D65(tmp, NULL);
		CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x65\x77\x20\x4C\x6F\x67\x69\x63\x54\x6F\x79\x20\x47\x72\x61\x70\x68"), NULL);
		CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline(tmp, il2cpp_codegen_string_new_wrapper("\x78\x4E\x6F\x64\x65\x20\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4C\x6F\x67\x69\x63\x54\x6F\x79\x20\x47\x72\x61\x70\x68"), NULL);
	}
}
static void AndNode_t45427CEB277C73612D296E8FDC94DB6875C6EFDE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0 * tmp = (NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0 *)cache->attributes[0];
		NodeWidthAttribute__ctor_m16DDD2A682E4AC137EA63B96FD229DB129A2C661(tmp, 140LL, NULL);
	}
	{
		NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99 * tmp = (NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99 *)cache->attributes[1];
		NodeTintAttribute__ctor_mD1F2FF56A82C04DFCFBF9517326CD1753FD0E938(tmp, 100, 70, 70, NULL);
	}
}
static void AndNode_t45427CEB277C73612D296E8FDC94DB6875C6EFDE_CustomAttributesCacheGenerator_input(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void AndNode_t45427CEB277C73612D296E8FDC94DB6875C6EFDE_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void U3CU3Ec_t8CD327AFCB76160BD9E200AE99A7F59CB6553003_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void NotNode_t8677C6444D93F5FA1C24EE3E8F630028F746DBE4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99 * tmp = (NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99 *)cache->attributes[0];
		NodeTintAttribute__ctor_mD1F2FF56A82C04DFCFBF9517326CD1753FD0E938(tmp, 100, 100, 50, NULL);
	}
	{
		NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0 * tmp = (NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0 *)cache->attributes[1];
		NodeWidthAttribute__ctor_m16DDD2A682E4AC137EA63B96FD229DB129A2C661(tmp, 140LL, NULL);
	}
}
static void NotNode_t8677C6444D93F5FA1C24EE3E8F630028F746DBE4_CustomAttributesCacheGenerator_input(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[1];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
}
static void NotNode_t8677C6444D93F5FA1C24EE3E8F630028F746DBE4_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void U3CU3Ec_t59F94964D9E49C644617EB30DCF690E357A68185_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void PulseNode_tC3392F83E66498449EEF618E923473CD3E072D7C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99 * tmp = (NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99 *)cache->attributes[0];
		NodeTintAttribute__ctor_mD1F2FF56A82C04DFCFBF9517326CD1753FD0E938(tmp, 70, 100, 70, NULL);
	}
	{
		NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0 * tmp = (NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0 *)cache->attributes[1];
		NodeWidthAttribute__ctor_m16DDD2A682E4AC137EA63B96FD229DB129A2C661(tmp, 140LL, NULL);
	}
}
static void PulseNode_tC3392F83E66498449EEF618E923473CD3E072D7C_CustomAttributesCacheGenerator_interval(CustomAttributesCache* cache)
{
	{
		SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 * tmp = (SpaceAttribute_t041FADA1DC4DD39BBDEBC47F445290D7EE4BBCC8 *)cache->attributes[0];
		SpaceAttribute__ctor_m765D137779D8FB95279BCE4A90BAB4EA409C9C44(tmp, -18.0f, NULL);
	}
}
static void PulseNode_tC3392F83E66498449EEF618E923473CD3E072D7C_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[1];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
}
static void ToggleNode_t97CF16C1B842EF6751427563423F1D8696265F97_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99 * tmp = (NodeTintAttribute_t70F71E7C9B2A138F21DA6A711032AD151D1B9C99 *)cache->attributes[0];
		NodeTintAttribute__ctor_mD1F2FF56A82C04DFCFBF9517326CD1753FD0E938(tmp, 70, 70, 100, NULL);
	}
	{
		NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0 * tmp = (NodeWidthAttribute_t7D540CC1B04E216A0B4E32CC93415D55E6B026E0 *)cache->attributes[1];
		NodeWidthAttribute__ctor_m16DDD2A682E4AC137EA63B96FD229DB129A2C661(tmp, 140LL, NULL);
	}
}
static void ToggleNode_t97CF16C1B842EF6751427563423F1D8696265F97_CustomAttributesCacheGenerator_input(CustomAttributesCache* cache)
{
	{
		InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD * tmp = (InputAttribute_tCE409E3D7140204886F20327F4DE485B09D1F3CD *)cache->attributes[0];
		InputAttribute__ctor_m2EFB8BECAB99883FB2F131CC23428A4CF4215E15(tmp, 1LL, 0LL, 0LL, false, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ToggleNode_t97CF16C1B842EF6751427563423F1D8696265F97_CustomAttributesCacheGenerator_output(CustomAttributesCache* cache)
{
	{
		OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 * tmp = (OutputAttribute_tC7C182284F49419854EA33B72026DA7C8EE2F585 *)cache->attributes[0];
		OutputAttribute__ctor_m7E09A669AF62162B96A8E820241E9B22A90FDE70(tmp, 0LL, 0LL, 0LL, false, NULL);
	}
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[1];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void U3CU3Ec_t64C3A155ED3F081F5024F3FACD3C433AB8ECC964_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[417] = 
{
	U3CU3Ec__DisplayClass26_0_tAC6175CBA9B5AACB71FE303225EF33E5D2DC140A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass26_1_tE55916711493150BA6439E31899F48C49C709263_CustomAttributesCacheGenerator,
	NarrationCharacter_tBA8729EA20561C739EC5E76A192BF2CBEFC17670_CustomAttributesCacheGenerator,
	NarrationLine_t3935F28CA0A06164780E17468DD3C09F5C1F757C_CustomAttributesCacheGenerator,
	DialogueGraph_tC8F98D469C95CBDA498A9A1FA38AA1170467EC61_CustomAttributesCacheGenerator,
	U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator,
	U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass32_0_tBC908EE6071787695F1125EB4CB7995011986FC3_CustomAttributesCacheGenerator,
	U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator,
	U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator,
	ObjectiveInfoSO_tAD54B01D79A962AFFE6BB90ED5E1FC33B7E62BE9_CustomAttributesCacheGenerator,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator,
	UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator,
	U3CU3Ec_tA6E8292C2B3E475DA239A7AA3EA5A22543F7BD37_CustomAttributesCacheGenerator,
	U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator,
	U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator,
	U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator,
	U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass22_0_tC229D66EB8530744B31CF7710EF5ED9D26152CA9_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass30_0_t8167D88381A49B98670A289746758B696C3D6350_CustomAttributesCacheGenerator,
	U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator,
	U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator,
	MathGraph_tF8675A1D5FFF6B80976105FDB4B5D5690A6A36D4_CustomAttributesCacheGenerator,
	StateGraph_tDA7B8CAF1B24A0DA9C18A5F878B7CCEC2B676AF0_CustomAttributesCacheGenerator,
	LogicGraph_t400DF7927DCC5A722713C073E9C361DD8A154482_CustomAttributesCacheGenerator,
	AndNode_t45427CEB277C73612D296E8FDC94DB6875C6EFDE_CustomAttributesCacheGenerator,
	U3CU3Ec_t8CD327AFCB76160BD9E200AE99A7F59CB6553003_CustomAttributesCacheGenerator,
	NotNode_t8677C6444D93F5FA1C24EE3E8F630028F746DBE4_CustomAttributesCacheGenerator,
	U3CU3Ec_t59F94964D9E49C644617EB30DCF690E357A68185_CustomAttributesCacheGenerator,
	PulseNode_tC3392F83E66498449EEF618E923473CD3E072D7C_CustomAttributesCacheGenerator,
	ToggleNode_t97CF16C1B842EF6751427563423F1D8696265F97_CustomAttributesCacheGenerator,
	U3CU3Ec_t64C3A155ED3F081F5024F3FACD3C433AB8ECC964_CustomAttributesCacheGenerator,
	Input_master_t9B5B284DF340547BAD045C91B46416A71BCB697A_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__debugTab,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__subtabList,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__playerBattle,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__enemyBattle,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__decreaseButton,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__increaseButton,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__shownStat,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__playerImage,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__playerName,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__level,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__skillPoint,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__vitality,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__intelligence,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__design,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__code,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__defence,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__HP,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__currentHP,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__idea,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__logic,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__baseAttack,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__baseDefence,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__expNeed,
	DebuggingMenu_t517BBE1AF762531645B5E8C78E925A8B055494B0_CustomAttributesCacheGenerator__expHave,
	NarrationCharacter_tBA8729EA20561C739EC5E76A192BF2CBEFC17670_CustomAttributesCacheGenerator__characterSprite,
	NarrationCharacter_tBA8729EA20561C739EC5E76A192BF2CBEFC17670_CustomAttributesCacheGenerator__characterName,
	NarrationLine_t3935F28CA0A06164780E17468DD3C09F5C1F757C_CustomAttributesCacheGenerator__speaker,
	NarrationLine_t3935F28CA0A06164780E17468DD3C09F5C1F757C_CustomAttributesCacheGenerator__text,
	BattleCutIn_tFFB78E274031C78B90FDCC7C8D9C7B942F003C51_CustomAttributesCacheGenerator_entry,
	BattleNode_tD6C18FD0F24D20A2719714A093B17E0A27A11664_CustomAttributesCacheGenerator_entry,
	ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator_entry,
	ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator_exit,
	ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator__character,
	ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator__dialogueLine,
	ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator__choiceLine,
	ChoiceNode_tEC1F60FDF2D582419B544D74A9FE31267A4CAA11_CustomAttributesCacheGenerator_nextOutputPriority,
	DialogueNode_tCB0329F5EBE20CA1236E0D713DEB7A4C5EAF292B_CustomAttributesCacheGenerator_entry,
	DialogueNode_tCB0329F5EBE20CA1236E0D713DEB7A4C5EAF292B_CustomAttributesCacheGenerator_exit,
	DialogueNode_tCB0329F5EBE20CA1236E0D713DEB7A4C5EAF292B_CustomAttributesCacheGenerator__character,
	DialogueNode_tCB0329F5EBE20CA1236E0D713DEB7A4C5EAF292B_CustomAttributesCacheGenerator__dialogueLine,
	DialogueNode_tCB0329F5EBE20CA1236E0D713DEB7A4C5EAF292B_CustomAttributesCacheGenerator_nextOutputPriority,
	EndNode_t3993962364AE73BE3E7DB6DA2B5A34911C2D0D05_CustomAttributesCacheGenerator_entry,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_graph,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__dialogueCanvas,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__interactionCanvas,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__choiceCanvas,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__choiceSpawnPosition,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__dialogueChoicePrefab,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__dialogueAnim,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__playerController,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__typingAudioSource,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator__typingSound,
	StartNode_tA0992E833D804E15A011293A0FBB5DE9277B0FD8_CustomAttributesCacheGenerator_exit,
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator_state,
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator__unitProfile,
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator__name,
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator__unitSprite,
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator__npcName,
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator__instruction,
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator_inInteractRange,
	NPC_tC1B80DAFC03710A57E14A5AA80EED2CD89A35AE6_CustomAttributesCacheGenerator_isDefeated,
	ObjectiveInfoSO_tAD54B01D79A962AFFE6BB90ED5E1FC33B7E62BE9_CustomAttributesCacheGenerator__objectiveId,
	ObjectiveInfoSO_tAD54B01D79A962AFFE6BB90ED5E1FC33B7E62BE9_CustomAttributesCacheGenerator_targetNPC,
	ObjectiveInfoSO_tAD54B01D79A962AFFE6BB90ED5E1FC33B7E62BE9_CustomAttributesCacheGenerator_objectiveName,
	ObjectiveInfoSO_tAD54B01D79A962AFFE6BB90ED5E1FC33B7E62BE9_CustomAttributesCacheGenerator_objectiveDescription,
	ObjectiveManager_t325E44CD23ED56F232807D8E46C81BBA3656ADF8_CustomAttributesCacheGenerator__debugObjectiveList,
	RuntimeObjective_t6EC87A02C01725E1F309E101A31BAB602EB164DE_CustomAttributesCacheGenerator__info,
	PlayerBrain_t8E1560D2B1590AF0ABC355B852E846F3FC4A0A47_CustomAttributesCacheGenerator_playerProfile,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_state,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__speed,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__movementDelay,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__nextMovePoint,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__animator,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__idleTrigger,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__walking,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator_playingAnimation,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__audioSource,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__walkingSound,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__groundTilemap,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__collisionTilemap,
	PlayerController_tDFE946D0FE1CB988FC3DC902E05737A2A62CA3D9_CustomAttributesCacheGenerator__collisionObject,
	PlayerModel_t1E6011AEEA89127DBFA99274CA47CEE13F7E7340_CustomAttributesCacheGenerator__playerSprite,
	PlayerMouseDetection_t538CCC17BB18B68A59D32B4E6A0BB9443DEF165A_CustomAttributesCacheGenerator__selectable,
	PlayerMouseDetection_t538CCC17BB18B68A59D32B4E6A0BB9443DEF165A_CustomAttributesCacheGenerator__poitedObject,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__attackName,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__attackDescription,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__attackIcon,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__attackAnim,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__staminaCost,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__ideaCost,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__logicCost,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__damage,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__critRate,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__heal,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__isDesignSkill,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__isCodeSkill,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__healRate,
	AttackProfile_t8DF822E6441498D04E51BF0462E9440180872EA0_CustomAttributesCacheGenerator__healAmount,
	UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__unitName,
	UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__unitSprite,
	UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__stat,
	UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__attackProfile,
	UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__skillProfile,
	UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__startingDialogue,
	UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__introBattle,
	UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator__reward,
	CharacterStat_t80447BD100798673C855508DC8ED1D045CBF7099_CustomAttributesCacheGenerator_currentLevel,
	CharacterStat_t80447BD100798673C855508DC8ED1D045CBF7099_CustomAttributesCacheGenerator_skillPoint,
	CharacterStat_t80447BD100798673C855508DC8ED1D045CBF7099_CustomAttributesCacheGenerator__levelingstat,
	Reward_t8DCC1AF39C20757EFE52BB3FE94BCD345F815EE0_CustomAttributesCacheGenerator__rewardEXP,
	LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__increaseBaseHP,
	LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__increaseBaseIdea,
	LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__increaseBaseLogic,
	LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__increaseBaseAtk,
	LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__increaseBaseDef,
	LevelingStat_tE9DD868A9F4EDF9A23251A5438B621AA1AC61E50_CustomAttributesCacheGenerator__skillPoint,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__bgmSource,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__sfxSource,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__onClick,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__onCancelled,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__onHover,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__battleBGMSource,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__worldBGM,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__battleBGM,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__battleSFX,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__healingSFX,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator__fadingSpeed,
	SoundManager_t55BBC5661BB88EBEFF99C27CBD475CF774902D62_CustomAttributesCacheGenerator_fading,
	ChangeProfileEvent_tC482FC50729070C875B58E7FB002744948AE1C7B_CustomAttributesCacheGenerator__newDialogue,
	GettingNewSkillEvent_t2FA3772BD6D1C8E440355A51C208B8390CC27037_CustomAttributesCacheGenerator__dialogueToPlay,
	GettingNewSkillEvent_t2FA3772BD6D1C8E440355A51C208B8390CC27037_CustomAttributesCacheGenerator__newSkill,
	HealBox_tEC2FB6743B667A64563259C8802FD7B6D83AD304_CustomAttributesCacheGenerator__resetEverything,
	MoveEvent_tD585745BA89C8BA14E4BEBF6B63C7D02FEC85809_CustomAttributesCacheGenerator__targetLocation,
	TriggerDialogue_t1A321E3FB424BB0738882500D4DAE4B27EF938A5_CustomAttributesCacheGenerator__dialogueToPlay,
	TriggerMusic_t8714EB0F65F7A5025D4F8B72C478E5FB66B97544_CustomAttributesCacheGenerator__newBGM,
	ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator__attackIcon,
	ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator__attackName,
	ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator__attackButton,
	BattleDialogue_t0B717574AC7850D2D3F212FC5EFE5E914CC1CD68_CustomAttributesCacheGenerator__dialogueCanvas,
	BattleDialogue_t0B717574AC7850D2D3F212FC5EFE5E914CC1CD68_CustomAttributesCacheGenerator__dialogueAnim,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator_currentProfile,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__unitName,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__unitLevel,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__maxHP,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__maxIdea,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__maxLogic,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__unitProfile,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__attackProfile,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__name,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__level,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__hp,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__idea,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__logic,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__unitImage,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__HPSlider,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__ideaSlider,
	TurnBasePlayer_tD64496B8B5F430148C56DF6417355702D28CDA2A_CustomAttributesCacheGenerator__logicSlider,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_state,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__enemyUnit,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__playerUnit,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__currentTargetNPC,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__battleCanvas,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__actionTab,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__attackTab,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__resultTab,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__playerController,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator__animator,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator_currentProfile,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__unitName,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__unitLevel,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__maxHP,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__maxStamina,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__unitProfile,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__attackProfile,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__name,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__level,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__hp,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__unitImage,
	TurnBaseUnit_t3A0EA76065D56C07835A0AC885B265E37604D279_CustomAttributesCacheGenerator__HPSlider,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__actionTab,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__resultTab,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackTab,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackImage,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackName,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackDescription,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__ideaUsage,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__logicUsage,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackProfile,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__attackButton,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__buttonSpawnPosition,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__resultingTab,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__expGain,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__currentLevel,
	TurnBasedUIManager_tFF6201FD486D8836A5C4AF4890F3DF0C651C3B16_CustomAttributesCacheGenerator__defeatEXPFactor,
	CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_CustomAttributesCacheGenerator_panSpeed,
	CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_CustomAttributesCacheGenerator_scrollLimitMin,
	CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_CustomAttributesCacheGenerator_scrollLimitMax,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__basePlayerProfile,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__playerProfileSlot,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__characterName,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__characterImage,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__characterSprite,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__startingSkillPoint,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__currentSkillPoint,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__totalSkillPoints,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__numberOfStat,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__statNumber,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__increaseButton,
	CharacterCreationMneu_tFFB9282FFBB2CCB2B3EBFA810CDAEA23E4B4CF43_CustomAttributesCacheGenerator__decreaseButton,
	CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__startingPosition,
	CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__margin,
	CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__smooth,
	CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__minBorder,
	CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__maxBorder,
	CustomCamera_t384C218A375CA13C26CFFF7F3913F65B8139F024_CustomAttributesCacheGenerator__targetToFollow,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__mainMenu,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__characterCreationTab,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__settingMenu,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__creditMenu,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__specialObject,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__resolutionSetting,
	MenuManager_tD52BB657312ED53913E423A985CACC86F900124C_CustomAttributesCacheGenerator__isWebBuild,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__tabList,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__specialObject,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__header,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__MenuTab,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__characterTab,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__characterStatTab,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__SettingTab,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__resolutionSetting,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__isWebBuild,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__playerImage,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__currentStat,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__skillPoint,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__playerName,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__vitality,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__intelligence,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__design,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__code,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__defence,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__level,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__HP,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__currentHP,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__idea,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__logic,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__baseAttack,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__baseDefence,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__expNeed,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__expHave,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__increaseButton,
	MenuTabManager_t510B3791F505DC5BE1CFBDCF007D57868A2F0D11_CustomAttributesCacheGenerator__shownStat,
	ResolutionSetting_t65491F1B4FCB2E7328CED93748384ADF87E72663_CustomAttributesCacheGenerator_resolutionDropDown,
	SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_transition,
	SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_image,
	SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_transitiontime,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator_soundForce,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__audioMixer,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__slider,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__ExposerString,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__audioSource,
	VolumeMixer_t9074BFEB051ED5FF17020EB0123287BCC075DAAF_CustomAttributesCacheGenerator__testSound,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__NPCToWin,
	WinLoseManager_t40F697D7B7A80D262F42990D844024929623DAA5_CustomAttributesCacheGenerator__winCanvas,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__objectName,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__objectInstruction,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__name,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__instructionText,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__npcName,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__instruction,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator_inInteractRange,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__containDialogue,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__isDoor,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__isSceneTeleporter,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__healPlayer,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__loadScene,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__playDialogue,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__playerPointer,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__destination,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__audioSource,
	WorldNPC_t3FEC4BC3A5ABA91EE4501B37F6EE4D90EDAFF71C_CustomAttributesCacheGenerator__sound,
	StateNode_t67B70094FF4EC074C8D00857773B6B38F074C576_CustomAttributesCacheGenerator_enter,
	StateNode_t67B70094FF4EC074C8D00857773B6B38F074C576_CustomAttributesCacheGenerator_exit,
	RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_graph,
	RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_runtimeMathNodePrefab,
	RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_graphContextMenu,
	RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_U3CscrollRectU3Ek__BackingField,
	UGUIMathBaseNode_t1151FCC3BCCC1BBFB86F553282380873605D559F_CustomAttributesCacheGenerator_node,
	UGUIMathBaseNode_t1151FCC3BCCC1BBFB86F553282380873605D559F_CustomAttributesCacheGenerator_graph,
	UGUIContextMenu_t1C011C797890A8ADA77F88D2621FE1C7157ED5B0_CustomAttributesCacheGenerator_selectedNode,
	UGUIPort_t7C023A7AAB1E206E3A9D516BC97926EDDDE3641A_CustomAttributesCacheGenerator_node,
	DisplayValue_tE369A7D7747D59F07948A207CC43A67D4F51CFEF_CustomAttributesCacheGenerator_input,
	MathNode_t6D322F810C43F9E03A41614FE98C1C9BA7F3292D_CustomAttributesCacheGenerator_a,
	MathNode_t6D322F810C43F9E03A41614FE98C1C9BA7F3292D_CustomAttributesCacheGenerator_b,
	MathNode_t6D322F810C43F9E03A41614FE98C1C9BA7F3292D_CustomAttributesCacheGenerator_result,
	Vector_t1B855FE635007476062AFB77108E1E0BD925DD4F_CustomAttributesCacheGenerator_x,
	Vector_t1B855FE635007476062AFB77108E1E0BD925DD4F_CustomAttributesCacheGenerator_y,
	Vector_t1B855FE635007476062AFB77108E1E0BD925DD4F_CustomAttributesCacheGenerator_z,
	Vector_t1B855FE635007476062AFB77108E1E0BD925DD4F_CustomAttributesCacheGenerator_vector,
	AndNode_t45427CEB277C73612D296E8FDC94DB6875C6EFDE_CustomAttributesCacheGenerator_input,
	AndNode_t45427CEB277C73612D296E8FDC94DB6875C6EFDE_CustomAttributesCacheGenerator_output,
	NotNode_t8677C6444D93F5FA1C24EE3E8F630028F746DBE4_CustomAttributesCacheGenerator_input,
	NotNode_t8677C6444D93F5FA1C24EE3E8F630028F746DBE4_CustomAttributesCacheGenerator_output,
	PulseNode_tC3392F83E66498449EEF618E923473CD3E072D7C_CustomAttributesCacheGenerator_interval,
	PulseNode_tC3392F83E66498449EEF618E923473CD3E072D7C_CustomAttributesCacheGenerator_output,
	ToggleNode_t97CF16C1B842EF6751427563423F1D8696265F97_CustomAttributesCacheGenerator_input,
	ToggleNode_t97CF16C1B842EF6751427563423F1D8696265F97_CustomAttributesCacheGenerator_output,
	Input_master_t9B5B284DF340547BAD045C91B46416A71BCB697A_CustomAttributesCacheGenerator_Input_master_get_asset_mB578FB991F84D0700B5FA9B4B34C7048BE5637EF,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_ParserNode_m3EAB881B5ECB17F0B0BDD84C5CA93EEFF15B1E30,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_CustomDialogue_mDA2FFD006849C2851B90F8E19927B5E4E409D418,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_ParseChoice_mE64E48CC0F78A2DE7D4257483A4347453929C73D,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_TypeSentence_mE846AFE4DD1D8B4E9B5E084E82823DC410D03762,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_U3CParseChoiceU3Eb__32_0_mE9CB54451F924F79F3AC08623838E478D66D7A2C,
	NodeReader_tEE4D62816766DBD637C0405735EAFD5653B5117D_CustomAttributesCacheGenerator_NodeReader_U3CParseChoiceU3Eb__32_1_m967C24CA1384A386651E4401E8B1E3C850BBF394,
	U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator_U3CParserNodeU3Ed__30__ctor_m382DAC51BDF1174D3212B4AF0B0D9DDDD122A329,
	U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator_U3CParserNodeU3Ed__30_System_IDisposable_Dispose_m14EB0FDDF590E4BDFF1FD8AC581D9AE22B70729C,
	U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator_U3CParserNodeU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE06E05FE75F35AB048964B3D36B60A3B74A7F33,
	U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator_U3CParserNodeU3Ed__30_System_Collections_IEnumerator_Reset_m0CDB444124BB417931E15B820C3C9D9ABE915800,
	U3CParserNodeU3Ed__30_t2B534EF97AD968A2C09D5AFCF033A32E261D67D0_CustomAttributesCacheGenerator_U3CParserNodeU3Ed__30_System_Collections_IEnumerator_get_Current_m5599AFCFFBBF462A44F42C2F1DAA399D2082E1AB,
	U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator_U3CCustomDialogueU3Ed__31__ctor_m539D0D53D0C9852C2AA92851832BBA1F93FE5215,
	U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator_U3CCustomDialogueU3Ed__31_System_IDisposable_Dispose_m05AB132BB1C96C406CB4E6FB832AA9310CAF9159,
	U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator_U3CCustomDialogueU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB652CBA767F5E7D4BF9661F5D60318DA0C3562DF,
	U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator_U3CCustomDialogueU3Ed__31_System_Collections_IEnumerator_Reset_m174F4229540442B55B3F7CCCDD051E832E908526,
	U3CCustomDialogueU3Ed__31_tF826BB6A7EA4D862E82837FD7E6F3CFFEAB25513_CustomAttributesCacheGenerator_U3CCustomDialogueU3Ed__31_System_Collections_IEnumerator_get_Current_m039BFABC311EAD1031D0A8D7D1E90260AE943B8C,
	U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator_U3CParseChoiceU3Ed__32__ctor_m8CD7150757546CD2F3182C33BBAE91519AAB9000,
	U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator_U3CParseChoiceU3Ed__32_System_IDisposable_Dispose_mC62B6A94C7B7FEAA59D9B2BF082E33A1E28A1293,
	U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator_U3CParseChoiceU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F3C38F7EFC22E10D93A71F5136ABE6343D8E287,
	U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator_U3CParseChoiceU3Ed__32_System_Collections_IEnumerator_Reset_m45250654797E7892C0D72ECDBDF79A12C230ABBF,
	U3CParseChoiceU3Ed__32_tE230D65B7B7F21CBBD66BE850E2A6AF3C6A2F60B_CustomAttributesCacheGenerator_U3CParseChoiceU3Ed__32_System_Collections_IEnumerator_get_Current_m34989581633799DDA06EE8408DA1FF9B977EED46,
	U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__38__ctor_mD2CE5896F92C199BFA6B203DC27462B78C8D2FBB,
	U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__38_System_IDisposable_Dispose_m3B08D4B564EB130E4D1160752C31C799AEBC00FB,
	U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C44140AB27A4C7204C12F1564FC47CCFDAC8C69,
	U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__38_System_Collections_IEnumerator_Reset_mA7053DC759C764A5E1E71EF04AED17866CA1575B,
	U3CTypeSentenceU3Ed__38_tA720458677AD28E8F090A91BC8863A3C8C9ADF85_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__38_System_Collections_IEnumerator_get_Current_mE7624AC3A9AFB6B227EC4D69375762C83BB9FB64,
	UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator_UnitProfile_ResettingFightingStat_m8ACAC82FA2F0D84722A15EF6F9642A4D72306321,
	UnitProfile_t3F69A4462E9DF8283E42D045D8EBB1EA36BE2D70_CustomAttributesCacheGenerator_UnitProfile_DefaultStartingLevel_m4BFFDA1B009B2A2AFC96C61A77994A98F7A11FE8,
	ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator_ActionButton_U3CButtonSetupU3Eb__4_0_m08761867745A1895E5FC24E48212246605E32BA7,
	ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator_ActionButton_U3CButtonSetupU3Eb__4_1_m5FF477DF8FAFE5AEC5C37EAF80E03DF9C1E1F497,
	ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator_ActionButton_U3CButtonSetupU3Eb__4_2_m95AF25B868B387888CAE76BAA0C58A74D909B80A,
	ActionButton_t1EE89A77688985062ED895A5D800B09287A29362_CustomAttributesCacheGenerator_ActionButton_U3CButtonSetupU3Eb__4_3_mD693DC64E13A8671086D2579FDE4F3CAA841305B,
	BattleDialogue_t0B717574AC7850D2D3F212FC5EFE5E914CC1CD68_CustomAttributesCacheGenerator_BattleDialogue_TypeSentence_m03DBCF9A9051983E515A2A9D99379FFA2B616BE0,
	BattleDialogue_t0B717574AC7850D2D3F212FC5EFE5E914CC1CD68_CustomAttributesCacheGenerator_BattleDialogue_U3CTypeSentenceU3Eb__15_0_m458E168B5080C9C7915C6C32DE32D5F131C05942,
	U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__15__ctor_m714D8743D196B43AECF81689E5212CEE2D292FA2,
	U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__15_System_IDisposable_Dispose_m9FAED1ED91495E5D1C54C55022788CE9C1880A6F,
	U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9BD268DFD0A34C14F5343375B8F74AE693EB2495,
	U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__15_System_Collections_IEnumerator_Reset_mD678F10373F8B0D8D78BE91742776FF56EC4654F,
	U3CTypeSentenceU3Ed__15_t299EBB26F8F72A1FBA1A3E490FEE0EAE120FC6D4_CustomAttributesCacheGenerator_U3CTypeSentenceU3Ed__15_System_Collections_IEnumerator_get_Current_m270C4F3E2E87C7235ED7F7457E25DEA8695791B5,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_SetupBattle_mA8A505D87F206EAF8C6FF1BF7EA4EE030859E683,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_EndBattle_m04FE104E66DD5B7603A5E7E310019698B54FA902,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_PlayerTurn_m64279C163C3632B41265E29A38CE186760934439,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_PlayerHealTurn_mE022E32CFA0E9B5A2CDFA97F4F6F3C7C02031F7E,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_EnemyTurn_m953332AC41E491EA08D064B7566E08E5211C6076,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_U3CPlayerTurnU3Eb__35_0_m71013D9FECDC342FF5202C42E881593D8CB8ECA2,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_U3CPlayerHealTurnU3Eb__36_0_m4F6A8805E871F063CB75F553296744F0FFD78D5F,
	TurnBaseSystem_tBBCEB8F291775B619A0FBB713B7EF82ACFF70073_CustomAttributesCacheGenerator_TurnBaseSystem_U3CEnemyTurnU3Eb__37_0_m6587319AD0E3D72F409E682B319B063AAC5E1A6F,
	U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator_U3CPlayerTurnU3Ed__35__ctor_m8AB2530F00AAC23868A1014008E4510CC8A85E7B,
	U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator_U3CPlayerTurnU3Ed__35_System_IDisposable_Dispose_mBA54B120599E2137E5393768D6120030E95A336B,
	U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator_U3CPlayerTurnU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m61EACE996BC752C1364CCA92B5C2166D4123E2AC,
	U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator_U3CPlayerTurnU3Ed__35_System_Collections_IEnumerator_Reset_mE2DDF9EF573EE8506B67333FD381EC49558D4D04,
	U3CPlayerTurnU3Ed__35_t093A48A599E6EE3AAC6C86F2544F1C56D7F4D0F6_CustomAttributesCacheGenerator_U3CPlayerTurnU3Ed__35_System_Collections_IEnumerator_get_Current_m66E0053278E42810479BE9E520E40D27F246FAC3,
	U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator_U3CPlayerHealTurnU3Ed__36__ctor_m16FD053EDC1AA07B2FEFB29F67B4AA7000E791F2,
	U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator_U3CPlayerHealTurnU3Ed__36_System_IDisposable_Dispose_m93A030B3E7AF0DCF29F04DFA1A1047D57632DA42,
	U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator_U3CPlayerHealTurnU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA71D3FD00986769C1CECB82B8711E700D8430D99,
	U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator_U3CPlayerHealTurnU3Ed__36_System_Collections_IEnumerator_Reset_mC26A5D24136DEF19D8186666D9610B2B419215E1,
	U3CPlayerHealTurnU3Ed__36_tEF071DFC8B90166379B4B1F980BC72E7F2C112EA_CustomAttributesCacheGenerator_U3CPlayerHealTurnU3Ed__36_System_Collections_IEnumerator_get_Current_m0F726B46089D8ACB32B00F591BE0D9B9361BE751,
	U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator_U3CEnemyTurnU3Ed__37__ctor_m9E7906BD17A2D26440DDBE0432B224E44AF8DAFA,
	U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator_U3CEnemyTurnU3Ed__37_System_IDisposable_Dispose_m4DDADCDAB60E4E76F6BF71E63260855719A5B556,
	U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator_U3CEnemyTurnU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB7E55AF6A8D378D610248C15AABB1DFB37EA4FF6,
	U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator_U3CEnemyTurnU3Ed__37_System_Collections_IEnumerator_Reset_m37EA51052F303CAEB8428D0A0BEA66936454B4DA,
	U3CEnemyTurnU3Ed__37_t66B9BEC3BD270C00E6A9AFF61E17C367E1F16968_CustomAttributesCacheGenerator_U3CEnemyTurnU3Ed__37_System_Collections_IEnumerator_get_Current_mBFE15F4A828D23A7AA24CF09039B4D93C3870896,
	SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_SceneLoader_loadlevel_m04CF2C85AEFC903C21B789AD1825E90000E5DC97,
	SceneLoader_t3266812523B184D5AEDF27CA310452129C676B0D_CustomAttributesCacheGenerator_SceneLoader_exitGame_m2924FA892A01B56DE948D8583EC159DCB0DD1D36,
	U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__10__ctor_mAE58B80B195BBFA7510F6E68EA9A1298361DBBA4,
	U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__10_System_IDisposable_Dispose_m860B69D84C70B4F5896588A3D6F8064C6960C632,
	U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8DC83A18DF8FF983CA0BF345C8B581996D42D402,
	U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__10_System_Collections_IEnumerator_Reset_m51A5262162ECD04D7DE7A5F02E29160606469126,
	U3CloadlevelU3Ed__10_t0BAE87B2D79C71FFFAC71F73480A1FA61A8C3ECD_CustomAttributesCacheGenerator_U3CloadlevelU3Ed__10_System_Collections_IEnumerator_get_Current_m3AD12E1478791BEAA33BB99964E9329A73384BD6,
	U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator_U3CexitGameU3Ed__11__ctor_mFF73E161755102B656F61250D9D576BE9A096C0F,
	U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator_U3CexitGameU3Ed__11_System_IDisposable_Dispose_mE0A07690D50D877F2029B4AAD833CBBDF8B5FAD8,
	U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator_U3CexitGameU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFEE4287F4E70EF27C575F348CABF960F5CEB4DCC,
	U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator_U3CexitGameU3Ed__11_System_Collections_IEnumerator_Reset_m42D8867845452E08B152564A3AD4191A22C79851,
	U3CexitGameU3Ed__11_t37CA377265E2F4520EB23862693CCE56FFCB681F_CustomAttributesCacheGenerator_U3CexitGameU3Ed__11_System_Collections_IEnumerator_get_Current_mB57F50DA13B68471F8331091EEDAAC4788CFF4C3,
	RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_RuntimeMathGraph_get_scrollRect_m69128FFC7B3A2FCA86348549F47FD80E5CFEE579,
	RuntimeMathGraph_t66C527408B43DB8DD5BA48C8628B0DECAC419E32_CustomAttributesCacheGenerator_RuntimeMathGraph_set_scrollRect_m1986940E63F05E28544767835AB2A62B823F9163,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_fileName_m14EBC66F19AEC15751EC9822478CDDBA9DE8D2F6_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CfileNameU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_menuName_m27C32BCE71E6A5ED185212F083DFB23201B514F1_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		String_t* L_0 = ___value0;
		__this->set_U3CmenuNameU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void CreateAssetMenuAttribute_set_order_m4E3BBB75DCF82E2ADCACE57685420FA3F8D71C1B_inline (CreateAssetMenuAttribute_t79F6BDD595B569A2D16681BDD571D1AE6E782D0C * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CorderU3Ek__BackingField_2(L_0);
		return;
	}
}
