using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputController : MonoBehaviour
{
    private static PlayerInputController _instance;
    public static PlayerInputController Instance => _instance;

    public Input_master _inputMaster;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        _inputMaster = new Input_master();
    }
    private void OnEnable()
    {
        _inputMaster.Enable();
    }
    private void OnDisable()
    {
        _inputMaster.Disable();
    }

    public Vector2 GetMovementValue
    {
        get
        {
            return _inputMaster.Player.Movement.ReadValue<Vector2>();
        }
    }
    public bool GetMovement()
    {
        //return click;
        return _inputMaster.Player.Movement.triggered;
    }
    public Vector2 MousePosition
    {
        get
        {
            return _inputMaster.Player.MousePosition.ReadValue<Vector2>();
        }
    }
    public bool LeftMouseClick()
    {
        return _inputMaster.Player.LeftMouse.triggered;
    }
    public float LeftMouseValue
    {
        get
        {
            return _inputMaster.Player.LeftMouse.ReadValue<float>();
        }
    }
    public float LeftMouseReleashValue
    {
        get
        {
            return _inputMaster.Player.LeftMouseReleash.ReadValue<float>();
        }
    }
    public bool Interact()
    {
        return _inputMaster.Player.Interaction.triggered;
    }
    public bool SkipDialogue()
    {
        return _inputMaster.Player.ForwardDialogue.triggered;
    }
    public bool Tab()
    {
        return _inputMaster.Player.Tab.triggered;
    }
    public bool Escape()
    {
        return _inputMaster.Player.Escape.triggered;
    }
    public bool Debugging()
    {
        return _inputMaster.Player.Debugging.triggered;
    }
}
