using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerModel : MonoBehaviour
{
    [SerializeField] private SpriteRenderer _playerSprite;

    private void Start()
    {
        _playerSprite.sprite = PlayerBrain.Instance.playerProfile.unitSprite;
    }
}
