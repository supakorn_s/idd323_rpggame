using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerBrain : MonoBehaviour
{
    private static PlayerBrain _instance;
    public static PlayerBrain Instance => _instance;

    [Header("Player Component Lists")]
    #region Player Component
    public UnitProfile playerProfile;
    public NarrationCharacter playerNarration;
    public PlayerInputController _playerInput;
    public PlayerController _playerController;
    public PlayerMouseDetection _playerMouseDetection;
    public PlayerModel _playerModel;
    #endregion
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        if (_playerInput == null) GetComponent<PlayerInputController>();
        if (_playerController == null) GetComponent<PlayerController>();
        if (_playerMouseDetection == null) GetComponent<PlayerMouseDetection>();
        if (_playerModel == null) GetComponent<PlayerModel>();

        playerNarration.ChangePlayerNarration(playerProfile.unitSprite, playerProfile.unitName);
    }
}
