using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class WorldNPC : MonoBehaviour
{
    [Header("General Information")]
    [SerializeField] private string _objectName;
    [SerializeField] private string _objectInstruction = "Interact";
    [SerializeField] private TextMeshProUGUI _name;
    [SerializeField] private TextMeshProUGUI _instructionText;
    [SerializeField] private GameObject _npcName;
    [SerializeField] private GameObject _instruction;
    [HideInInspector] public bool inInteractRange = false;
    private bool _hovering = false;

    [Header("Object Information")]
    [SerializeField] private bool _containDialogue = true;
    [SerializeField] private bool _isDoor = false;
    [SerializeField] private bool _isSceneTeleporter = false;
    [SerializeField] private bool _healPlayer = false;
    [SerializeField] private int _loadScene = 0;
    [SerializeField] private DialogueGraph _playDialogue;
    private GameObject _player;
    private UnitProfile _playerProfile;
    [SerializeField] private GameObject _playerPointer;
    [SerializeField] private GameObject _destination;

    [Header("Audio")]
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private AudioClip _sound;


    private void Start()
    {
        _name.text = _objectName;
        _instructionText.text = _objectInstruction;
        _instruction.SetActive(false);
        _npcName.SetActive(false);
        _player = GameObject.FindGameObjectWithTag("Player");
        _playerProfile = PlayerBrain.Instance.playerProfile;
        _playerPointer = GameObject.FindGameObjectWithTag("PlayerPointer");
    }
    private void Update()
    {
        MouseHovering();
        NPCNotificationBox();
        Interaction();
    }
    public void NPCNotificationBox()
    {
        switch (inInteractRange)
        {
            case true:
                _instruction.SetActive(true);
                _npcName.SetActive(true);
                break;
            case false:
                if (!_hovering)
                {
                    _instruction.SetActive(false);
                    _npcName.SetActive(false);
                }
                else
                {
                    _instruction.SetActive(false);
                }
                break;
        }
    }
    private void Interaction()
    {
        if (inInteractRange && PlayerController.Instance.state == playerState.MOVEABLE && !PlayerController.Instance.playingAnimation)
        {
            if (PlayerInputController.Instance.Interact())
            {
                if (_isDoor)
                {
                    PlayOneShot(_sound);
                    _playerPointer.transform.position = _destination.transform.position;
                    _player.transform.position = _destination.transform.position;
                    return;
                }
                if (_isSceneTeleporter)
                {
                    PlayOneShot(_sound);
                    SceneLoader.Instance.startLoadLevel(_loadScene);
                    return;
                }
                if (_healPlayer)
                {
                    PlayOneShot(_sound);
                    _playerProfile.RestoreCharacter();
                }
                if (_containDialogue)
                {
                    PlayDialogue(_playDialogue);
                }
            }
            if (PlayerMouseDetection.Instance.CheckMouseOnObject(this.gameObject))
            {
                if (PlayerInputController.Instance.LeftMouseClick())
                {
                    if (_isDoor)
                    {
                        PlayOneShot(_sound);
                        _playerPointer.transform.position = _destination.transform.position;
                        _player.transform.position = _destination.transform.position;
                        return;
                    }
                    if (_isSceneTeleporter)
                    {
                        PlayOneShot(_sound);
                        SceneLoader.Instance.startLoadLevel(_loadScene);
                        return;
                    }
                    if (_healPlayer)
                    {
                        PlayOneShot(_sound);
                        _playerProfile.RestoreCharacter();
                    }
                    if (_containDialogue)
                    {
                        PlayDialogue(_playDialogue);
                    }
                }
            }
        }
    }
    private void PlayDialogue(DialogueGraph graph)
    {
        NodeReader.Instance.StartDialogue(graph);
    }

    public void MouseHovering()
    {
        //_hovering = true;
        //_npcName.SetActive(true);
        if (PlayerMouseDetection.Instance.CheckMouseOnObject(this.gameObject) && !GameManager.Instance.isPaused)
        {
            _hovering = true;
            _npcName.SetActive(true);
        }
        else
        {
            _hovering = false;
            _npcName.SetActive(false);
        }
    }
    public void MouseNotHovering()
    {
        _hovering = false;
        _npcName.SetActive(false);
    }

    private void PlayOneShot(AudioClip audioClip)
    {
        if (audioClip != null)
        {
            _audioSource.PlayOneShot(audioClip);
        }
    }
}
