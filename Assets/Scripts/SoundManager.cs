using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;


public class SoundManager : SingletonClass<SoundManager>
{
    [Header("Audio Source")]
    [SerializeField] private AudioSource _bgmSource;
    [SerializeField] private AudioSource _sfxSource;

    [Header("UI Sound")]
    [SerializeField] private AudioClip _onClick;
    [SerializeField] private AudioClip _onCancelled;
    [SerializeField] private AudioClip _onHover;

    [Header("World Sound")]
    [SerializeField] private AudioSource _battleBGMSource;
    [SerializeField] private List<AudioClip> _worldBGM;
    [SerializeField] private List<AudioClip> _battleBGM;
    [SerializeField] private List<AudioClip> _battleSFX;
    [SerializeField] private List<AudioClip> _healingSFX;

    [Header("Fading")]
    [SerializeField] private float _fadingSpeed = 1.5f;
    [HideInInspector] public bool fading = false;
    private AudioClip _waitingClip;

    #region Getter
    public AudioSource bgmSource => _bgmSource;
    public AudioClip onClick => _onClick;
    public AudioClip onCancelled => _onCancelled;
    public AudioClip onHover => _onHover;
    #endregion

    private void Update()
    {
        FadeToNewMusic();
    }

    public void OnSceneLoaded()
    {
        int scene = SceneManager.GetActiveScene().buildIndex;
        switch (scene)
        {
            case 0:
                PlayWorldBGM(0);
                break;
            case 1:
                PlayWorldBGM(1);
                break;
            case 2:
                PlayWorldBGM(0);
                break;
        }

    }
    private void CheckIfPlaying()
    {
        if (!_bgmSource.isPlaying && _bgmSource.clip != null)
        {
            _bgmSource.Play();
        }
    }
    public void OnClickUI()
    {
        PlayOneShot(_onClick);
    }
    public void OnCancelledUI()
    {
        PlayOneShot(_onCancelled);
    }
    public void OnHoverUI()
    {
        PlayOneShot(_onHover);
    }
    private void PlayOneShot(AudioClip audioClip)
    {
        _sfxSource.PlayOneShot(audioClip);
    }
    public void PlayWorldBGM(int bgmIndex)
    {
        _bgmSource.clip = _worldBGM[bgmIndex];
        _bgmSource.Play();
    }
    public void PauseWorldBGM()
    {
        _bgmSource.Pause();
    }
    public void ResumeWorldBGM()
    {
        _battleBGMSource.Pause();
        _bgmSource.Play();
    }
    public void ClearWorldBGM()
    {
        _bgmSource.clip = null;
    }
    public void PlayBattleBGM(int bgmIndex)
    {
        _bgmSource.Pause();
        _battleBGMSource.clip = _battleBGM[bgmIndex];
        _battleBGMSource.Play();
    }
    public void PlayRandomBattleFightSFX()
    {
        int battleSFX = Random.Range(0, _battleSFX.Count);
        PlayOneShot(_battleSFX[battleSFX]);
    }
    public void PlayRandomHealingSFX()
    {
        int healingSFX = Random.Range(0, _healingSFX.Count);
        PlayOneShot(_healingSFX[healingSFX]);
    }
    public void FadeToNewMusic()
    {
        if (fading)
        {
            _bgmSource.volume -= Time.deltaTime * _fadingSpeed;
            if (_bgmSource.volume <= 0)
            {
                fading = false;
                _bgmSource.clip = _waitingClip;
                _waitingClip = null;
                _bgmSource.volume = 1;
                _bgmSource.Play();
            }
        }
    }
    public void TriggerFade(AudioClip newClip)
    {
        _waitingClip = newClip;
        fading = true;
    }
}
