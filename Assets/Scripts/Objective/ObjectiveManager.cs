using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectiveManager : SingletonClass<ObjectiveManager>
{

    [SerializeField] private List<RuntimeObjective> _debugObjectiveList = new List<RuntimeObjective>();
    private Dictionary<ObjectiveInfoSO, RuntimeObjective> _startedObjective = new Dictionary<ObjectiveInfoSO, RuntimeObjective>();

    public void StartObjective(ObjectiveInfoSO questToStart)
    {
        if (_startedObjective.ContainsKey(questToStart))
        {
            Debug.LogWarning($"The quest {questToStart.name} has already been started");
            return;
        }
        RuntimeObjective questInstance = new RuntimeObjective(questToStart);
        _startedObjective[questToStart] = questInstance;
        _debugObjectiveList.Add(questInstance);
    }
}
