using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Objective Profile", menuName = "SO/Objective/Simple Objective", order = 0)]
public class ObjectiveInfoSO : ScriptableObject
{
    //public abstract RuntimeObjective CreateRunTimeObjective;

    [SerializeField] private int _objectiveId;
    [SerializeField] private GameObject targetNPC;
    [SerializeField] private string objectiveName;
    [TextArea] [SerializeField] private string objectiveDescription;
}
