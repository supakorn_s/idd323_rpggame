using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class RuntimeObjective
{
    [SerializeField] private ObjectiveInfoSO _info;
    public RuntimeObjective(ObjectiveInfoSO originInfo)
    {
        _info = originInfo;
    }
}
