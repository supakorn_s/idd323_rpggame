using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DebuggingMenu : MonoBehaviour
{
    [SerializeField] private GameObject _debugTab;
    [SerializeField] private List<GameObject> _subtabList;
    [SerializeField] private TurnBasePlayer _playerBattle;
    [SerializeField] private TurnBaseUnit _enemyBattle;
    private UnitProfile _playerProfile;
    private bool _debugOpen = false;

    #region Stat Editor
    [Header("Stat Editor")]
    [SerializeField] private List<Button> _decreaseButton;
    [SerializeField] private List<Button> _increaseButton;
    [SerializeField] private List<int> _shownStat;

    [SerializeField] private Image _playerImage;
    [SerializeField] private TextMeshProUGUI _playerName;
    [SerializeField] private TextMeshProUGUI _level;
    [SerializeField] private TextMeshProUGUI _skillPoint;
    [SerializeField] private TextMeshProUGUI _vitality;
    [SerializeField] private TextMeshProUGUI _intelligence;
    [SerializeField] private TextMeshProUGUI _design;
    [SerializeField] private TextMeshProUGUI _code;
    [SerializeField] private TextMeshProUGUI _defence;
    [SerializeField] private TextMeshProUGUI _HP;
    [SerializeField] private TextMeshProUGUI _currentHP;
    [SerializeField] private TextMeshProUGUI _idea;
    [SerializeField] private TextMeshProUGUI _logic;
    [SerializeField] private TextMeshProUGUI _baseAttack;
    [SerializeField] private TextMeshProUGUI _baseDefence;
    [SerializeField] private TextMeshProUGUI _expNeed;
    [SerializeField] private TextMeshProUGUI _expHave;
    #endregion

    private void Start()
    {
        _debugTab.SetActive(true);
        for (int i = 0; i < _decreaseButton.Count; i++)
        {
            int decreaseSlot = i;
            _decreaseButton[i].onClick.AddListener(delegate { DecreaseStat(decreaseSlot); });
        }
        for (int i = 0; i < _increaseButton.Count; i++)
        {
            int increaseSlot = i;
            _increaseButton[i].onClick.AddListener(delegate { IncreaseStat(increaseSlot); });
        }
        for (int i = 0; i < _subtabList.Count; i++)
        {
            _subtabList[i].SetActive(false);
        }
        _debugTab.SetActive(false);
        _playerProfile = PlayerBrain.Instance.playerProfile;
    }
    void Update()
    {
        OpenCloseMenuTab();
    }
    private void OpenCloseMenuTab()
    {
        if (SceneLoader.Instance.transitioning == false)
        {
            if (PlayerInputController.Instance.Debugging())
            {
                if (_debugOpen)
                {
                    _debugTab.SetActive(false);
                }
                else
                {
                    _debugTab.SetActive(true);
                    CheckPlayerInformation();
                }
                _debugOpen = !_debugOpen;
            }
        }
    }
    private void CheckPlayerInformation()
    {
        _shownStat.Clear();
        _shownStat.Add(_playerProfile.stat.vitality);
        _shownStat.Add(_playerProfile.stat.intelligence);
        _shownStat.Add(_playerProfile.stat.design);
        _shownStat.Add(_playerProfile.stat.code);
        _shownStat.Add(_playerProfile.stat.defence);

        _playerName.text = _playerProfile.unitName;
        _playerImage.sprite = _playerProfile.unitSprite;
        UpdateText();
    }
    private void UpdateText()
    {
        _level.text = _playerProfile.stat.currentLevel.ToString();
        _skillPoint.text = _playerProfile.stat.skillPoint.ToString();
        _vitality.text = _playerProfile.stat.vitality.ToString("F0");
        _intelligence.text = _playerProfile.stat.intelligence.ToString("F0");
        _design.text = _playerProfile.stat.design.ToString("F0");
        _code.text = _playerProfile.stat.code.ToString("F0");
        _defence.text = _playerProfile.stat.defence.ToString("F0");

        _HP.text = _playerProfile.stat.GetCurrentMaxHP.ToString("F0");
        _currentHP.text = _playerProfile.stat.currentHP.ToString("F0");
        _idea.text = _playerProfile.stat.GetCurrentIdea.ToString("F0");
        _logic.text = _playerProfile.stat.GetCurrentLogic.ToString("F0");
        _baseAttack.text = _playerProfile.stat.GetCurrentAttack.ToString("F0");
        _baseDefence.text = _playerProfile.stat.GetCurrentDefence.ToString("F0");
        _expHave.text = _playerProfile.stat.currentEXP.ToString("F0");
        _expNeed.text = _playerProfile.stat.requireEXP.ToString("F0");
    }
    public void IncreaseStat(int statNumber)
    {
        _shownStat[statNumber] += 1;

        switch (statNumber)
        {
            case 0:
                _vitality.text = _shownStat[statNumber].ToString("F0");
                _playerProfile.stat.vitality += 1;
                break;
            case 1:
                _intelligence.text = _shownStat[statNumber].ToString("F0");
                _playerProfile.stat.intelligence += 1;
                break;
            case 2:
                _design.text = _shownStat[statNumber].ToString("F0");
                _playerProfile.stat.design += 1;
                break;
            case 3:
                _code.text = _shownStat[statNumber].ToString("F0");
                _playerProfile.stat.code += 1;
                break;
            case 4:
                _defence.text = _shownStat[statNumber].ToString("F0");
                _playerProfile.stat.defence += 1;
                break;
        }
        UpdateText();
        Debug.Log(statNumber);
    }
    public void DecreaseStat(int statNumber)
    {
        _shownStat[statNumber] += 1;

        switch (statNumber)
        {
            case 0:
                _vitality.text = _shownStat[statNumber].ToString("F0");
                _playerProfile.stat.vitality -= 1;
                break;
            case 1:
                _intelligence.text = _shownStat[statNumber].ToString("F0");
                _playerProfile.stat.intelligence -= 1;
                break;
            case 2:
                _design.text = _shownStat[statNumber].ToString("F0");
                _playerProfile.stat.design -= 1;
                break;
            case 3:
                _code.text = _shownStat[statNumber].ToString("F0");
                _playerProfile.stat.code -= 1;
                break;
            case 4:
                _defence.text = _shownStat[statNumber].ToString("F0");
                _playerProfile.stat.defence -= 1;
                break;
        }
        UpdateText();
    }
    public void OpenCloseSubTab(int tabNumber)
    {
        for (int i = 0; i < _subtabList.Count; i++)
        {
            _subtabList[i].SetActive(false);
        }
        _subtabList[tabNumber].SetActive(true);
    }
    public void CloseDebugMenu()
    {
        _debugTab.SetActive(false);
        _debugOpen = !_debugOpen;
    }
    public void DebugRefillHealth()
    {
        _playerProfile.RestoreCharacter();
        if (_playerBattle.currentProfile != null)
        {
            _playerBattle.DebugFullPlayerStatue();
        }
    }
    public void DebugResetStat()
    {
        _playerProfile.ResettingFightingStat();
    }
    public void DebugResetLevel()
    {
        _playerProfile.DefaultStartingLevel();
    }
    public void DebugSetEnemyHPTo10()
    {
        if (_enemyBattle.currentProfile != null)
        {
            _enemyBattle.DebugSetHPtoLow();
        }
    }
}
