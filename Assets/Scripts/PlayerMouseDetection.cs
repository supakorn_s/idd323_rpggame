using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerMouseDetection : MonoBehaviour
{
    private static PlayerMouseDetection _instance;
    public static PlayerMouseDetection Instance => _instance;

    [SerializeField] private LayerMask _selectable;
    [SerializeField] private GameObject _poitedObject;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    private void Update()
    {
       
    }

    private void checkifPressOnObjectBackUp()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(PlayerInputController.Instance.MousePosition);
        RaycastHit2D hitObject = Physics2D.Raycast(mousePosition, Vector2.zero, 0f, _selectable);
        if (hitObject.collider != null)
        {
            _poitedObject = hitObject.collider.gameObject;
            if (_poitedObject.TryGetComponent(out NPC npcScript))
            {
                npcScript.MouseHovering();
            }
        }
        else
        {
            if (_poitedObject != null)
            {
                if (_poitedObject.TryGetComponent(out NPC npcScript))
                {
                    npcScript.MouseNotHovering();
                }

                _poitedObject = null;
            }
        }
    }
    public bool CheckMouseOnObject(GameObject CollidedObject)
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(PlayerInputController.Instance.MousePosition);
        RaycastHit2D hitObject = Physics2D.Raycast(mousePosition, Vector2.zero, 1000f, _selectable);

        if (hitObject.collider != null)
        {
            if (hitObject.collider.gameObject == CollidedObject)
            {
                _poitedObject = CollidedObject;
                return true;
            }
            else
            {
                _poitedObject = null;
                return false;
            }
        }
        else
        {
            _poitedObject = null;
            return false;
        }
    }
}
