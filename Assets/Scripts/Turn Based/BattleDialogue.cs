using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class BattleDialogue : MonoBehaviour
{
    private static BattleDialogue _instance;
    public static BattleDialogue Instance => _instance;


    [Header("UI")]
    [SerializeField] private GameObject _dialogueCanvas;
    [SerializeField] private Animator _dialogueAnim;
    public TextMeshProUGUI speaker;
    public TextMeshProUGUI dialogue;
    public Image characterImage;

    private Coroutine _dialogue;
    private bool _dialogueComplete = false;
    private bool _typing = false;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    private void Update()
    {
        SkipDialogue();
    }
    public void StartBattleDialogue()
    {
        _dialogueCanvas.SetActive(true);
        _dialogueAnim.SetTrigger("in");
    }
    public void ChangeSentence(UnitProfile profile, string sentence)
    {
        StartCoroutine(TypeSentence(speaker, profile.unitName));
        _dialogue = StartCoroutine(TypeSentence(dialogue, sentence));
        characterImage.sprite = profile.unitSprite;
    }

    IEnumerator TypeSentence(TextMeshProUGUI textBox, string sentence)
    {
        _typing = true;
        textBox.text = "";

        // Problem apply Rich Text here

        foreach (char letter in sentence.ToCharArray())
        {
            textBox.text += letter;

            yield return new WaitForSeconds(0.025f);
            if (_dialogueComplete)
            {
                break;
            }
        }

        _dialogueComplete = true;
        _typing = false;

        yield return new WaitUntil(() => _dialogueComplete);
        yield return new WaitUntil(() => PlayerInputController.Instance.LeftMouseClick() || PlayerInputController.Instance.SkipDialogue());
        EndBattleDialogue();
    }


    public void SkipDialogueButton()
    {
        if (_typing)
        {
            _dialogueComplete = true;
        }
    }
    private void SkipDialogue()
    {
        if (_typing)
        {
            if (PlayerInputController.Instance.SkipDialogue())
            {
                _dialogueComplete = true;
            }

        }
    }   // Spacebar
    private void EndBattleDialogue()
    {
        _dialogueAnim.SetTrigger("out");
    }
}
