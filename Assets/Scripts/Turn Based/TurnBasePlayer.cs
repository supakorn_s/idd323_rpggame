using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TurnBasePlayer : MonoBehaviour
{
    [Header("General Information")]
    public UnitProfile currentProfile;

    [SerializeField] private string _unitName = "Unit Name";
    [SerializeField] private int _unitLevel;

    [SerializeField] private float _maxHP;
    public float currentHP;

    [SerializeField] private float _maxIdea;
    public float currentIdea;
    [SerializeField] private float _maxLogic;
    public float currentLogic;

    [SerializeField] private UnitProfile _unitProfile;
    [SerializeField] private List<AttackProfile> _attackProfile;

    [Header("UI")]
    [SerializeField] private TextMeshProUGUI _name;
    [SerializeField] private TextMeshProUGUI _level;
    [SerializeField] private TextMeshProUGUI _hp;
    [SerializeField] private TextMeshProUGUI _idea;
    [SerializeField] private TextMeshProUGUI _logic;
    [SerializeField] private Image _unitImage;
    [SerializeField] private Slider _HPSlider;
    [SerializeField] private Slider _ideaSlider;
    [SerializeField] private Slider _logicSlider;
    public bool updateStatus = false;

    private void Update()
    {
        UpdateStatus();
    }
    public void SetUnitProfile(UnitProfile newProfile)
    {
        _unitProfile = newProfile;
        currentProfile = _unitProfile;
    }
    public void SetUnit()
    {
        if (currentProfile == null) return;
        _unitName = currentProfile.unitName;
        _unitLevel = currentProfile.stat.currentLevel;
        _maxHP = currentProfile.stat.GetCurrentMaxHP;
        currentHP = currentProfile.stat.currentHP;
        if (currentHP > _maxHP) currentHP = _maxHP;
        _maxIdea = currentProfile.stat.GetCurrentIdea;
        currentIdea = currentProfile.stat.currentIdea;
        _maxLogic = currentProfile.stat.GetCurrentLogic;
        currentLogic = currentProfile.stat.currentLogic;
        _attackProfile = currentProfile.attackProfile;

        //Set UI
        _name.text = _unitName;
        _level.text = _unitLevel.ToString();
        _unitImage.sprite = currentProfile.unitSprite;
        _HPSlider.maxValue = _maxHP;
        _HPSlider.value = currentHP;
        _ideaSlider.maxValue = _maxIdea;
        _ideaSlider.value = currentIdea;
        _logicSlider.maxValue = _maxLogic;
        _logicSlider.value = currentLogic;

        _hp.text = currentHP.ToString("F1");
        _idea.text = _maxIdea.ToString("F1");
        _logic.text = _maxLogic.ToString("F1");
    }

    public void UpdateStatus()
    {
        if (updateStatus)
        {
            float targetHP = Mathf.MoveTowards(_HPSlider.value, currentHP, Time.deltaTime * 50);
            float targetidea = Mathf.MoveTowards(_ideaSlider.value, currentIdea, Time.deltaTime * 50);
            float targetlogic = Mathf.MoveTowards(_logicSlider.value, currentLogic, Time.deltaTime * 50);
            _HPSlider.value = targetHP;
            _ideaSlider.value = targetidea;
            _logicSlider.value = targetlogic;
            _hp.text = targetHP.ToString("F1");
            _idea.text = targetidea.ToString("F1");
            _logic.text = targetlogic.ToString("F1");

            if (_HPSlider.value <= 0)
            {
                _HPSlider.value = 0;
                updateStatus = false;
                TurnBaseSystem.Instance.state = turnState.LOSE;
                //TurnBaseSystem.Instance.EndBattle();
                TurnBasedUIManager.Instance.OpenResult(false);
            }
            if (_HPSlider.value > _HPSlider.maxValue)
            {
                _HPSlider.value = _HPSlider.maxValue;
            }
            if (_HPSlider.value == currentHP && targetidea == currentIdea && targetlogic == currentLogic)
            {
                updateStatus = false;
                //TurnBaseSystem.Instance.state = turnState.PLAYER_TURN;
            }
        }
    }

    public void DebugFullPlayerStatue()
    {
        currentHP = _maxHP;
        currentIdea = _maxIdea;
        currentLogic = _maxLogic;
        _hp.text = currentHP.ToString("F1");
        _idea.text = _maxIdea.ToString("F1");
        _logic.text = _maxLogic.ToString("F1");
    }
}
