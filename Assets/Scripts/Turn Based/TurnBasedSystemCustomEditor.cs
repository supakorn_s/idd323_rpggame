#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TurnBaseSystem))]
public class TurnBasedSystemCustomEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        TurnBaseSystem _turnBaseSystem = (TurnBaseSystem)target;
        /*if (GUILayout.Button("Run Battle"))
        {
            _turnBaseSystem.SetupBattle();
        }*/
        if (GUILayout.Button("End Battle"))
        {
            _turnBaseSystem.EndBattle();
        }


    }
}
#endif