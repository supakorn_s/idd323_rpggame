using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ActionButton : MonoBehaviour
{
    [SerializeField] private Image _attackIcon;
    [SerializeField] private TextMeshProUGUI _attackName;
    [SerializeField] private Button _attackButton;
    private AttackProfile _attackProfile;


    public void ButtonSetup(AttackProfile attack)
    {
        _attackIcon.sprite = attack.attackIcon;
        _attackName.text = attack.attackName;
        _attackProfile = attack;
        if (_attackProfile.isDesignSkill)
        {
            _attackButton.onClick.AddListener(delegate { TurnBaseSystem.Instance.PlayerSkillAction(_attackProfile, true); });
            return;
        }
        if (_attackProfile.isCodeSkill)
        {
            _attackButton.onClick.AddListener(delegate { TurnBaseSystem.Instance.PlayerSkillAction(_attackProfile, false); });
            return;
        }
        if (_attackProfile.heal)
        {
            _attackButton.onClick.AddListener(delegate { TurnBaseSystem.Instance.SkillHeal(_attackProfile); });
            return;
        }
        else
        {
            _attackButton.onClick.AddListener(delegate { TurnBaseSystem.Instance.PlayerAttackAction(_attackProfile); });
        }

    }

    public void ChangeAttackDescription()
    {
        TurnBasedUIManager.Instance.ChangeAttackDetailText(_attackProfile.attackName, _attackProfile.attackDescription, _attackProfile.attackIcon);
        TurnBasedUIManager.Instance.ChangeStaminaUsage(_attackProfile.ideaCost, _attackProfile.logicCost);
    }
    public void ResetAttackDescription()
    {
        TurnBasedUIManager.Instance.ChangeAttackDetailText(null, null, null);
        TurnBasedUIManager.Instance.ChangeStaminaUsage(0, 0);
    }

    public void OnClickUI()
    {
        SoundManager.Instance.OnClickUI();
    }
    public void OnCancelledUI()
    {
        SoundManager.Instance.OnCancelledUI();
    }
    public void OnHoverUI()
    {
        SoundManager.Instance.OnHoverUI();
    }
}
