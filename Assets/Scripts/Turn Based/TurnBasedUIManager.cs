using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TurnBasedUIManager : MonoBehaviour
{
    [Header("Player Hud")]
    [SerializeField] private GameObject _actionTab;
    [SerializeField] private GameObject _resultTab;


    [Header("Attack Detail Tab")]
    [SerializeField] private GameObject _attackTab;
    [SerializeField] private Image _attackImage;
    [SerializeField] private TextMeshProUGUI _attackName;
    [SerializeField] private TextMeshProUGUI _attackDescription;
    [SerializeField] private TextMeshProUGUI _ideaUsage;
    [SerializeField] private TextMeshProUGUI _logicUsage;
    [SerializeField] private List<AttackProfile> _attackProfile;
    [SerializeField] private GameObject _attackButton;
    [SerializeField] private Transform _buttonSpawnPosition;

    [Header("Result Tab")]
    [SerializeField] private GameObject _resultingTab;
    [SerializeField] private TextMeshProUGUI _expGain;
    [SerializeField] private TextMeshProUGUI _currentLevel;
    [SerializeField] private float _defeatEXPFactor = 0.25f;


    private UnitProfile _playerProfile;
    private TurnBaseSystem _turnBaseSystem;
    private PlayerBrain _playerBrain;
    private static TurnBasedUIManager _instance;
    public static TurnBasedUIManager Instance => _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    void Start()
    {
        _turnBaseSystem = TurnBaseSystem.Instance;
        _playerBrain = PlayerBrain.Instance;
        _playerProfile = _playerBrain.playerProfile;
        _resultingTab.SetActive(false);
    }

    public void GenerateAttackButton(List<AttackProfile> attackProfile)
    {
        _attackProfile = attackProfile;
        ClearButton();
        for (int i = 0; i < _attackProfile.Count; i++)
        {
            GameObject actionButton =  Instantiate(_attackButton, _buttonSpawnPosition);
            actionButton.transform.localPosition = new Vector3(0, 0, 0);

            if (actionButton.TryGetComponent(out ActionButton buttonScript))
            {
                buttonScript.ButtonSetup(_attackProfile[i]);
            }
        }
        _attackProfile = null;
    }
    private void ClearButton()
    {
        for (int i = 0; i < _buttonSpawnPosition.transform.childCount; i++)
        {
            Destroy(_buttonSpawnPosition.transform.GetChild(i).gameObject);
        }
    }
    public void ChangeAttackDetailText(string attackName, string attackDescription, Sprite attackSprite)
    {
        _attackName.text = attackName;
        _attackDescription.text = attackDescription;
        _attackImage.sprite = attackSprite;
    }
    public void ChangeStaminaUsage(float ideaUse, float logicUse)
    {
        if (ideaUse > 0)
        {
            _ideaUsage.text = "- " + ideaUse.ToString("F1");
        }
        else
        {
            _ideaUsage.text = ideaUse.ToString("F1");
        }

        if (logicUse > 0)
        {
            _logicUsage.text = "- " + logicUse.ToString("F1");
        }
        else
        {
            _logicUsage.text = logicUse.ToString("F1");
        }
    }
    public void OpenActionTab()
    {
        _actionTab.SetActive(true);
        _attackTab.SetActive(false);
    }
    public void OpenAttackTab()
    {
        _actionTab.SetActive(false);
        _attackTab.SetActive(true);
        TurnBasedUIManager.Instance.GenerateAttackButton(PlayerBrain.Instance.playerProfile.attackProfile);
    }
    public void OpenSkillTab()
    {
        _actionTab.SetActive(false);
        _attackTab.SetActive(true);
        TurnBasedUIManager.Instance.GenerateAttackButton(PlayerBrain.Instance.playerProfile.skillProfile);
    }
    public void Resting()
    {
        TurnBaseSystem.Instance.Resting();
    }

    public void OpenResult(bool isWin)
    {
        _resultingTab.SetActive(true);
        TurnBaseSystem turnbaseSystem = TurnBaseSystem.Instance;
        int expGain = turnbaseSystem.enemyUnit.currentProfile.reward.rewardEXP;

        if (isWin)
        {
            if (turnbaseSystem._currentTargetNPC != null)
            {
                if (turnbaseSystem._currentTargetNPC.TryGetComponent(out NPC npcScript))
                {
                    if (npcScript.isDefeated == false)
                    {
                        npcScript.isDefeated = true;
                        turnbaseSystem.playerUnit.currentProfile.stat.GainEXP(expGain);
                        _expGain.text = turnbaseSystem.enemyUnit.currentProfile.reward.rewardEXP.ToString();
                    }
                    else
                    {
                        //turnbaseSystem.playerUnit.currentProfile.stat.GainEXP((int)((float)expGain * ((float)_defeatEXPFactor/2))); Penalty when already defeated
                        //_expGain.text = ((int)((float)expGain * _defeatEXPFactor)).ToString();
                        turnbaseSystem.playerUnit.currentProfile.stat.GainEXP(expGain);
                        _expGain.text = turnbaseSystem.enemyUnit.currentProfile.reward.rewardEXP.ToString();
                    }
                }
            }
            Debug.Log(expGain);
        }
        else if (!isWin)
        {
            turnbaseSystem.playerUnit.currentProfile.stat.GainEXP((int)((float)expGain * _defeatEXPFactor));
            _expGain.text = ((int)((float)expGain * _defeatEXPFactor)).ToString();

            turnbaseSystem.playerUnit.currentProfile.stat.currentHP = 20;
        }

        _actionTab.SetActive(false);
        _currentLevel.text = turnbaseSystem.playerUnit.currentProfile.stat.currentLevel.ToString();
    }
    public void EndBattle()
    {
        _resultingTab.SetActive(false);
        _turnBaseSystem.EndBattle();
    }
}
