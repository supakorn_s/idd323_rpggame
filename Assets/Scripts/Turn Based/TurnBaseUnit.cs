using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TurnBaseUnit : MonoBehaviour
{
    [Header("General Information")]
    public UnitProfile currentProfile;

    [SerializeField] private string _unitName = "Unit Name";
    [SerializeField] private int _unitLevel;

    [SerializeField] private int _maxHP;
    public float currentHP;

    [SerializeField] private int _maxStamina;
    public float currentStamina;

    [SerializeField] private UnitProfile[] _unitProfile;
    [SerializeField] private List<AttackProfile> _attackProfile;
    public List<AttackProfile> attackProfile => _attackProfile;

    [Header("UI")]
    [SerializeField] private TextMeshProUGUI _name;
    [SerializeField] private TextMeshProUGUI _level;
    [SerializeField] private TextMeshProUGUI _hp;
    [SerializeField] private Image _unitImage;
    [SerializeField] private Slider _HPSlider;
    public bool updateStatus = false;

    private void Update()
    {
        UpdateStatus();
    }
    public virtual void SetUnitProfile(UnitProfile[] newProfile)
    {
        _unitProfile = newProfile;
        currentProfile = _unitProfile[0];
    }
    public virtual void SetUnit()
    {
        if (currentProfile == null) return;
        _unitName = currentProfile.unitName;
        _unitLevel = currentProfile.stat.currentLevel;
        _maxHP = currentProfile.stat.GetCurrentMaxHP;
        currentProfile.stat.currentHP = _maxHP;
        currentHP = _maxHP;
        _maxStamina = currentProfile.stat.maxAIStamina;
        currentProfile.stat.currentStamina = _maxStamina;
        currentStamina = _maxStamina;
        _attackProfile = currentProfile.attackProfile;

        //Set UI
        _name.text = _unitName;
        _level.text = _unitLevel.ToString();
        _unitImage.sprite = currentProfile.unitSprite;
        _HPSlider.maxValue = _maxHP;
        _HPSlider.value = _maxHP;

        _hp.text = _maxHP.ToString("F1");
    }

    public void UpdateStatus()
    {
        if (updateStatus)
        {
            float targetHP = Mathf.MoveTowards(_HPSlider.value, currentHP, Time.deltaTime * 50);
            _HPSlider.value = targetHP;
            _hp.text = _HPSlider.value.ToString("F1");
            Debug.Log("Updating");
            if (_HPSlider.value <= 0)
            {
                _HPSlider.value = 0;
                updateStatus = false;
                TurnBaseSystem.Instance.state = turnState.WIN;
                //TurnBaseSystem.Instance.EndBattle();
                TurnBasedUIManager.Instance.OpenResult(true);
            }
            if (_HPSlider.value > _HPSlider.maxValue)
            {
                _HPSlider.value = _HPSlider.maxValue;
            }
            if (_HPSlider.value == currentHP)
            {
                updateStatus = false;
                //TurnBaseSystem.Instance.state = turnState.ENEMY_TURN;
            }

        }
    }
    public void DebugSetHPtoLow()
    {
        if (currentProfile != null)
        {
            currentProfile.stat.currentHP = 10;
        }
        currentHP = 10;
        _hp.text = _maxHP.ToString("F1");
    }
}
