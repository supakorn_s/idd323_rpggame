using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum turnState { START, PLAYER_TURN, ENEMY_TURN, WIN, LOSE }
public class TurnBaseSystem : MonoBehaviour
{
    [Header("Turn State")]
    public turnState state = turnState.START;
    public Coroutine playerTurn;
    public Coroutine enemyTurn;

    [Header("Unit")]
    [SerializeField] private TurnBaseUnit _enemyUnit;
    [SerializeField] private TurnBasePlayer _playerUnit;
    [HideInInspector] public GameObject _currentTargetNPC;
    public TurnBaseUnit enemyUnit => _enemyUnit;
    public TurnBasePlayer playerUnit => _playerUnit;
    [Header("Canvas")]
    [SerializeField] private GameObject _battleCanvas;

    [Header("Player Hud")]
    [SerializeField] private GameObject _actionTab;
    [SerializeField] private GameObject _attackTab;
    [SerializeField] private GameObject _resultTab;

    [Header("Player")]
    [SerializeField] private PlayerController _playerController;

    private bool _inbattle = false;
    private static TurnBaseSystem _instance;
    public static TurnBaseSystem Instance => _instance;
    public bool inbattle => _inbattle;

    [Header("Animation")]
    [SerializeField]private Animator _animator;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    private void Start()
    {
        state = turnState.START;
        _battleCanvas.SetActive(false);
        _attackTab.SetActive(false);
        _resultTab.SetActive(false);
    }
    private void Update()
    {
        if (_inbattle)
        {
            switch (state)
            {
                case turnState.START:
                    if (NodeReader.Instance.CheckDialogueIsPlaying == false)
                    {
                        _actionTab.SetActive(true);
                        state = turnState.PLAYER_TURN;
                    }
                    break;
                case turnState.PLAYER_TURN:
                    
                    break;
                case turnState.ENEMY_TURN:
                    //StartCoroutine(EnemyTurn());
                    break;
                default:
                    _actionTab.SetActive(false);
                    break;
            }
        }
    }
    [ContextMenu("SetupBattle")]
    public void SetupBattle(UnitProfile[] newProfile)
    {
        state = turnState.START;
        _inbattle = true;
        _enemyUnit.SetUnitProfile(newProfile);
        _playerUnit.SetUnitProfile(PlayerBrain.Instance.playerProfile);
        _playerController.state = playerState.UNMOVEABLE;
        _battleCanvas.SetActive(true);
        _enemyUnit.SetUnit();
        _playerUnit.SetUnit();

        //TurnBasedUIManager.Instance.GenerateAttackButton(PlayerBrain.Instance.playerProfile.attackProfile);
        _actionTab.SetActive(false);

        // Randomize profile or other stuff for Intro battle here
        if (newProfile[0].introBattle != null)
        {
            NodeReader.Instance.StartDialogue(newProfile[0].introBattle);
        }
        else
        {
            NodeReader.Instance.BattleCutIn();
        }

        //BattleDialogue.Instance.StartBattleDialogue();
        //string StartingSentence = "Lets do this!";
        //BattleDialogue.Instance.ChangeSentence(_playerUnit.currentProfile, StartingSentence);



        SoundManager.Instance.PlayBattleBGM(0);
    }
    [ContextMenu("EndBattle")]
    public void EndBattle()
    {
        WinLoseManager.Instance.WinCheck();
        _inbattle = false;
        _playerController.state = playerState.MOVEABLE;
        _battleCanvas.SetActive(false);

        SoundManager.Instance.ResumeWorldBGM();
    }

    public void CurrentNPCTarget(GameObject target)
    {
        _currentTargetNPC = target;
    }
    private void EnterDialogue(DialogueGraph newDialogue)
    {
        _actionTab.SetActive(false);
        _attackTab.SetActive(false);
        NodeReader.Instance.StartDialogue(newDialogue);
    }
    public void OpenActionTab()
    {
        _actionTab.SetActive(true);
        _attackTab.SetActive(false);
    }
    public void OpenAttackTab()
    {
        _actionTab.SetActive(false);
        _attackTab.SetActive(true);
    }
    public void OpenSkilllTab()
    {
        _actionTab.SetActive(false);
        _attackTab.SetActive(false);
    }
    public void PlayerAttackAction(AttackProfile attack)
    {
        if (_playerUnit.currentIdea < attack.ideaCost) return;
        if (_playerUnit.currentLogic < attack.logicCost) return;

        SoundManager.Instance.PlayRandomBattleFightSFX();

        _playerUnit.currentIdea -= attack.ideaCost;
        _playerUnit.currentLogic -= attack.logicCost;
        _playerUnit.UpdateStatus();

        _enemyUnit.currentProfile.stat.TakeDamage(_playerUnit.currentProfile.stat.GetCurrentAttack, attack.damage);
        _enemyUnit.currentHP = _enemyUnit.currentProfile.stat.currentHP;
        _enemyUnit.updateStatus = true;

        _playerUnit.updateStatus = true;

        _actionTab.SetActive(false);
        _attackTab.SetActive(false);

        //state = turnState.ENEMY_TURN;
        if (attack.attackAnim != null)
        {
            _animator.Play(attack.attackAnim.name);
        }
        else
        {
            _animator.Play("Anim_PlayerAttack_01");
        }

        StartCoroutine(PlayerTurn(1.5f));
    }
    public void PlayerSkillAction(AttackProfile attack, bool isDesign)
    {
        if (_playerUnit.currentIdea < attack.ideaCost) return;
        if (_playerUnit.currentLogic < attack.logicCost) return;

        SoundManager.Instance.PlayRandomBattleFightSFX();

        _playerUnit.currentIdea -= attack.ideaCost;
        _playerUnit.currentLogic -= attack.logicCost;
        _playerUnit.UpdateStatus();

        switch (isDesign)
        {
            case true:
                _enemyUnit.currentProfile.stat.TakeSpecialDamage(_playerUnit.currentProfile.stat.GetCurrentAttack, attack.damage, _playerUnit.currentProfile.stat.design, isDesign);
                break;


            case false:
                _enemyUnit.currentProfile.stat.TakeSpecialDamage(_playerUnit.currentProfile.stat.GetCurrentAttack, attack.damage, _playerUnit.currentProfile.stat.code, isDesign);
                break;
        }
        _enemyUnit.currentHP = _enemyUnit.currentProfile.stat.currentHP;
        _enemyUnit.updateStatus = true;

        _playerUnit.updateStatus = true;

        _actionTab.SetActive(false);
        _attackTab.SetActive(false);

        //state = turnState.ENEMY_TURN;
        if (attack.attackAnim != null)
        {
            _animator.Play(attack.attackAnim.name);
        }
        else
        {
            _animator.Play("Anim_PlayerAttack_01");
        }

        StartCoroutine(PlayerTurn(1.5f));
    }
    public void EnemyAttackAction(AttackProfile attack)
    {
        if (_enemyUnit.currentStamina < attack.staminaCost) return;

        _enemyUnit.currentStamina -= attack.staminaCost;
        _enemyUnit.UpdateStatus();

        if (attack.isDesignSkill || attack.isCodeSkill || attack.heal)
        {
            if (attack.isDesignSkill)
            {
                _playerUnit.currentProfile.stat.TakeSpecialDamage(_enemyUnit.currentProfile.stat.GetCurrentAttack, attack.damage, _enemyUnit.currentProfile.stat.design, true);
            }
            if (attack.isCodeSkill)
            {
                _playerUnit.currentProfile.stat.TakeSpecialDamage(_enemyUnit.currentProfile.stat.GetCurrentAttack, attack.damage, _enemyUnit.currentProfile.stat.design, false);
            }
            if (attack.heal)
            {
                if (attack.healRate > 0)
                {
                    float healAmount = (float)_enemyUnit.currentProfile.stat.GetCurrentMaxHP * ((float)attack.healRate / 100);
                    _enemyUnit.currentProfile.stat.currentHP += (int)healAmount;
                }
                if (attack.healAmount > 0)
                {
                    _enemyUnit.currentProfile.stat.currentHP += attack.healAmount;
                }
                if (_enemyUnit.currentProfile.stat.currentHP > _enemyUnit.currentProfile.stat.GetCurrentMaxHP)
                    _enemyUnit.currentProfile.stat.currentHP = _enemyUnit.currentProfile.stat.GetCurrentMaxHP;
                _enemyUnit.currentHP = _enemyUnit.currentProfile.stat.currentHP;
                _enemyUnit.UpdateStatus();
                _enemyUnit.updateStatus = true;
            }
        }
        else
        {
            _playerUnit.currentProfile.stat.TakeDamage(_enemyUnit.currentProfile.stat.GetCurrentAttack, attack.damage);
        }
        _playerUnit.currentHP = _playerUnit.currentProfile.stat.currentHP;
        _playerUnit.updateStatus = true;

        _actionTab.SetActive(false);
        _attackTab.SetActive(false);

        SoundManager.Instance.PlayRandomBattleFightSFX();
        if (attack.attackAnim != null)
        {
            _animator.Play(attack.attackAnim.name);
        }
        else
        {
            _animator.Play("Anim_EnemyAttack_01");
        }
    }
    public IEnumerator PlayerTurn(float waitTime)
    {
        yield return new WaitUntil(() => _enemyUnit.updateStatus = true);
        yield return new WaitForSeconds(waitTime);
        if (state == turnState.WIN || state == turnState.LOSE) yield break;
        state = turnState.ENEMY_TURN;
        if (_enemyUnit.currentHP > 0)
        {
            StartCoroutine(EnemyTurn());
        }
    }
    public IEnumerator PlayerHealTurn(float waitTime)
    {
        yield return new WaitUntil(() => _playerUnit.updateStatus = true);
        yield return new WaitForSeconds(waitTime);
        if (state == turnState.WIN || state == turnState.LOSE) yield break;
        state = turnState.ENEMY_TURN;
        if (_enemyUnit.currentHP > 0)
        {
            StartCoroutine(EnemyTurn());
        }
    }
    public IEnumerator EnemyTurn()
    {
        int attack = Random.Range(0, _enemyUnit.attackProfile.Count);
        EnemyAttackAction(_enemyUnit.attackProfile[attack]);

        yield return new WaitUntil(() => playerUnit.updateStatus = true);
        yield return new WaitForSeconds(1.5f);
        if (state == turnState.WIN || state == turnState.LOSE) yield break;
        OpenActionTab();
        state = turnState.PLAYER_TURN;
    }
    public void Resting()
    {
        int restIdea = (int)(_playerUnit.currentIdea + (_playerUnit.currentProfile.stat.GetCurrentIdea * 0.4f));
        int restLogic = (int)(_playerUnit.currentLogic + (_playerUnit.currentProfile.stat.GetCurrentLogic * 0.4f));
        if (restIdea > _playerUnit.currentProfile.stat.GetCurrentIdea) restIdea = _playerUnit.currentProfile.stat.GetCurrentIdea;
        if (restLogic > _playerUnit.currentProfile.stat.GetCurrentLogic) restLogic = _playerUnit.currentProfile.stat.GetCurrentLogic;
        _playerUnit.currentProfile.stat.currentIdea = restIdea;
        _playerUnit.currentProfile.stat.currentLogic = restLogic;
        _playerUnit.currentIdea = restIdea;
        _playerUnit.currentLogic = restLogic;
        _playerUnit.UpdateStatus();
        _playerUnit.updateStatus = true;
        //_enemyUnit.updateStatus = true;
        _actionTab.SetActive(false);

        SoundManager.Instance.PlayRandomHealingSFX();
        _animator.Play("Anim_PlayerHeal_01");
        StartCoroutine(PlayerHealTurn(1.5f));
    }
    public void SkillHeal(AttackProfile attack)
    {
        if (_playerUnit.currentIdea < attack.ideaCost) return;
        if (_playerUnit.currentLogic < attack.logicCost) return;
        _playerUnit.currentIdea -= attack.ideaCost;
        _playerUnit.currentLogic -= attack.logicCost;

        if (attack.healRate > 0)
        {
            float healAmount = (float)_playerUnit.currentProfile.stat.GetCurrentMaxHP * ((float)attack.healRate / 100);
            _playerUnit.currentProfile.stat.currentHP += (int)healAmount;
        }
        if (attack.healAmount > 0)
        {
            _playerUnit.currentProfile.stat.currentHP += attack.healAmount;
        }
        if (_playerUnit.currentProfile.stat.currentHP > _playerUnit.currentProfile.stat.GetCurrentMaxHP) 
            _playerUnit.currentProfile.stat.currentHP = _playerUnit.currentProfile.stat.GetCurrentMaxHP;

        _playerUnit.currentHP = _playerUnit.currentProfile.stat.currentHP;
        _playerUnit.UpdateStatus();
        _playerUnit.updateStatus = true;
        //_enemyUnit.updateStatus = true;

        _actionTab.SetActive(false);
        _attackTab.SetActive(false);

        //state = turnState.ENEMY_TURN;
        SoundManager.Instance.PlayRandomHealingSFX();
        if (attack.attackAnim != null)
        {
            //_animator.Play(attack.attackAnim.name);
            _animator.Play("Anim_PlayerHeal_01");
        }
        else
        {
            _animator.Play("Anim_PlayerHeal_01");
        }

        StartCoroutine(PlayerHealTurn(1.5f));
    }
}
