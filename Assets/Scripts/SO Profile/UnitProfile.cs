using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Profile_Name", menuName = "SO/Profile/Unit", order = 0)]
public class UnitProfile : ScriptableObject
{

    #region Private Variable
    [Header("General Information")]
    [SerializeField] private string _unitName = "Unit Name";
    [SerializeField] private Sprite _unitSprite;
    //[SerializeField] private int _unitLevel = 1;

    [Header("Character Stat")]
    [SerializeField] private CharacterStat _stat;

    [Header("Attack Profile")]
    [SerializeField] private List<AttackProfile> _attackProfile;
    [SerializeField] private List<AttackProfile> _skillProfile;

    [Header("Starting Dialogue")]
    [SerializeField] private DialogueGraph _startingDialogue;
    
    [Header("Battle Dialogue")]
    [SerializeField] private DialogueGraph _introBattle;

    [Header("(Enemy) Reward")]
    [SerializeField] private Reward _reward;
    #endregion

    #region Public Getter
    public string unitName => _unitName;
    public Sprite unitSprite => _unitSprite;
    public CharacterStat stat => _stat;
    public List<AttackProfile> attackProfile => _attackProfile;
    public List<AttackProfile> skillProfile => _skillProfile;

    public DialogueGraph startingDialogue => _startingDialogue;
    public DialogueGraph introBattle => _introBattle;
    public Reward reward => _reward;
    #endregion

    public void ResetDefaultState()
    {
        _stat = new CharacterStat();
    }
    [ContextMenu("Resetting Fighting Stat Temporary Stat")]
    public void ResettingFightingStat()
    {
        DefaultStartingLevel();

        _stat.vitality = 2;
        _stat.intelligence = 2;
        _stat.design = 2;
        _stat.code = 2;
        _stat.defence = 2;

        _stat.maxHP = 100;
        _stat.maxIdea = 50;
        _stat.maxLogic = 50;
        _stat.baseATK = 20;
        _stat.baseDEF = 10;
        _stat.skillPoint = 0;

        _stat.currentHP = _stat.GetCurrentMaxHP;
        _stat.currentIdea = _stat.GetCurrentIdea;
        _stat.currentLogic = _stat.GetCurrentLogic;
        _stat.currentStamina = _stat.maxAIStamina;
    }
    [ContextMenu("Reset Level")]
    public void DefaultStartingLevel()
    {
        _stat.currentLevel = 1;
        _stat.currentEXP = 0;
        _stat.requireEXP = 100;
    }
    public void RestoreCharacter()
    {
        _stat.currentHP = _stat.GetCurrentMaxHP;
        _stat.currentIdea = _stat.GetCurrentIdea;
        _stat.currentLogic = _stat.GetCurrentLogic;
        _stat.currentStamina = _stat.maxAIStamina;
    }
    public void SetNewProfile(Sprite unitSprite, int vit, int intel, int des, int cod, int def, int skillpts)
    {
        _unitSprite = unitSprite;
        stat.vitality = vit;
        stat.intelligence = intel;
        stat.design = des;
        stat.code = cod;
        stat.defence = def;
        stat.skillPoint = skillpts;
    }
    public void ChangeName(string newName)
    {
        _unitName = newName;
    }
    public void AddNewSkill(AttackProfile newSkill)
    {
        _skillProfile.Add(newSkill);
    }
    public void ResetSkill(List<AttackProfile> newSkillList)
    {
        _skillProfile.Clear();
        for (int i = 0; i < newSkillList.Count; i++)
        {
            _skillProfile.Add(newSkillList[i]);
        }
    }
}

[System.Serializable]
public class CharacterStat
{
    [Header("Character Stat")]
    public int currentLevel = 1;
    public int currentEXP = 0;
    public int currentHP;
    public int currentIdea;
    public int currentLogic;
    public int currentStamina;


    public int requireEXP = 100;
    public int maxAIStamina = 0;

    public int vitality = 0;
    public int intelligence = 0;
    public int design = 0;
    public int code = 0;
    public int defence = 0;

    public int maxHP = 100;
    public int maxIdea = 50;
    public int maxLogic = 50;
    public int baseATK = 20;
    public int baseDEF = 10;

    [Header("SKILL POINTS")]
    public int skillPoint = 0;

    [Header("Level up extra")]
    [SerializeField] private LevelingStat[] _levelingstat;

    public void GainEXP(int EXPGain)
    {
        int newEXP = requireEXP - EXPGain;
        int remainExp = EXPGain;


        currentEXP += EXPGain;
        while (remainExp > 0)
        {
            if (currentEXP >= requireEXP)
            {
                remainExp = currentEXP - requireEXP;
                //levelIncrease += 1;
                LevelingUp();
                currentEXP = remainExp;
            }
            else
            {
                remainExp = 0;
            }

        }

    }
    public void LevelingUp()
    {
        currentLevel += 1;
        currentEXP = 0;
        requireEXP = (int)((requireEXP * 1.5));
        LevelingStatCheck();
    }
    private void LevelingStatCheck()
    {
        for (int i = 0; i < _levelingstat.Length; i++)
        {
            if (_levelingstat[i].LevelRank == currentLevel)
            {
                maxHP += _levelingstat[i].increaseBaseHP;
                maxIdea += _levelingstat[i].increaseBaseIdea;
                maxLogic += _levelingstat[i].increaseBaseLogic;
                baseATK += _levelingstat[i].increaseBaseAtk;
                baseDEF += _levelingstat[i].increaseBaseDef;
                skillPoint += _levelingstat[i].skillPoint;
            }
        }
    }
    public void TakeDamage(float attackDamage, float attackMoveDamage)
    {
        float damageCalculation = ((attackDamage + attackMoveDamage) * ((float)100 / (100 + GetCurrentDefence)));
        currentHP -= (int)damageCalculation;
        Debug.Log(damageCalculation);
    }
    public void TakeSpecialDamage(float attackDamage, float attackMoveDamage, float designCodeValue, bool isDesignDamage)
    {
        float damage = 0;
        if (isDesignDamage)
        {
            damage = (((attackDamage+ attackMoveDamage) * ((float)(100 + (designCodeValue - design)) / 100)) * ((float)100 / (100 + GetCurrentDefence)));
            //currentHP -= (int)((attackDamage * ((float)(100 + (designCodeValue - design)) / 100)) * ((float)100 / GetCurrentDefence));
        }
        else
        {
            damage = (((attackDamage + attackMoveDamage) * ((float)(100 + (designCodeValue - code)) / 100)) * ((float)100 / (100 + GetCurrentDefence)));
            //currentHP -= (int)((attackDamage * ((float)(100 + (designCodeValue - code)) / 100)) * ((float)100 / GetCurrentDefence));
        }
        currentHP -= (int)damage;
        Debug.Log(damage);
    }
    public int GetCurrentMaxHP
    {
        get
        {
            int actualHP = maxHP + (vitality * 5);
            return actualHP;
        }
    }
    public int GetCurrentIdea
    {
        get
        {
            int actualIdea = maxIdea + (design * 5);
            return actualIdea;
        }
    }
    public int GetCurrentLogic
    {
        get
        {
            int actualLogic = maxLogic + (code * 5);
            return actualLogic;
        }
    }
    public int GetCurrentAttack
    {
        get
        {
            int actualAttack = baseATK + (intelligence * 5);
            return actualAttack;
        }
    }
    public int GetCurrentDefence
    {
        get
        {
            int actualDefence = baseDEF + (defence * 5);
            return actualDefence;
        }
    }

}

[System.Serializable]
public class Reward
{
    [SerializeField] private int _rewardEXP = 100;
    public int rewardEXP => _rewardEXP;
}

[System.Serializable]
public class LevelingStat
{
    public int LevelRank = 0;
    [SerializeField] private int _increaseBaseHP;
    [SerializeField] private int _increaseBaseIdea;
    [SerializeField] private int _increaseBaseLogic;
    [SerializeField] private int _increaseBaseAtk;
    [SerializeField] private int _increaseBaseDef;
    [SerializeField] private int _skillPoint;
    public int increaseBaseHP => _increaseBaseHP;
    public int increaseBaseIdea => _increaseBaseIdea;
    public int increaseBaseLogic => _increaseBaseLogic;
    public int increaseBaseAtk => _increaseBaseAtk;
    public int increaseBaseDef => _increaseBaseDef;
    public int skillPoint => _skillPoint;
}