using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Attack_name_attack", menuName = "SO/Profile/Attack", order = 0)]
public class AttackProfile : ScriptableObject
{
    [Header("General Information")]
    [SerializeField] private string _attackName = "Attack Name";
    [TextArea][SerializeField] private string _attackDescription = "This unit use the attack";
    [SerializeField] private Sprite _attackIcon;
    [SerializeField] private AnimationClip _attackAnim;

    [SerializeField] private float _staminaCost = 0;
    [SerializeField] private float _ideaCost = 0;
    [SerializeField] private float _logicCost = 0;
    [SerializeField] private float _damage = 1;
    [SerializeField] private int _critRate = 5; // In percent
    [SerializeField] private bool _heal = false;
    [SerializeField] private bool _isDesignSkill = false;
    [SerializeField] private bool _isCodeSkill = false;
    [SerializeField] private int _healRate = 0; // In percent
    [SerializeField] private int _healAmount = 0; // In HP unit

    #region Public Getter
    public string attackName => _attackName;
    public string attackDescription => _attackDescription;
    public Sprite attackIcon => _attackIcon;
    public AnimationClip attackAnim => _attackAnim;

    public float staminaCost => _staminaCost;
    public float ideaCost => _ideaCost;
    public float logicCost => _logicCost;
    public float damage => _damage;
    public int critRate => _critRate;
    public bool heal => _heal;
    public bool isDesignSkill => _isDesignSkill;
    public bool isCodeSkill => _isCodeSkill;
    public int healRate => _healRate;
    public int healAmount => _healAmount;
    #endregion
}
