using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCTriggerCollider : MonoBehaviour
{
    private NPC _npc;
    private WorldNPC _worldNpc;
    private void Start()
    {
        //_npc = GetComponentInParent<NPC>();
        if (transform.parent.TryGetComponent(out NPC npcScript))
        {
            _npc = npcScript;
        }
        if (transform.parent.TryGetComponent(out WorldNPC wnpcScript))
        {
            _worldNpc = wnpcScript;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (_npc != null) _npc.inInteractRange = true;
            if (_worldNpc != null) _worldNpc.inInteractRange = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if(_npc != null)_npc.inInteractRange = false;
            if (_worldNpc != null) _worldNpc.inInteractRange = false;
        }
    }
}
