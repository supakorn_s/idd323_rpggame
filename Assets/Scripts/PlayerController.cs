using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

public enum playerState { MOVEABLE, UNMOVEABLE, OCCUPIED };
public class PlayerController : MonoBehaviour
{

    private static PlayerController _instance;
    public static PlayerController Instance => _instance;

    [Header("Player State")]
    public playerState state = playerState.MOVEABLE;

    [Header("Player Stat")]
    [SerializeField] private float _speed = 5.0f;
    [SerializeField] private float _movementDelay = 0.05f;
    [SerializeField] private Transform _nextMovePoint;

    [Header("Animation")]
    [SerializeField] private Animator _animator;
    [SerializeField] private string _idleTrigger = "Idle";
    [SerializeField] private AnimationClip _walking;
    [HideInInspector] public bool playingAnimation = false;

    [Header("Audio")]
    [SerializeField] private AudioSource _audioSource;
    [SerializeField] private List<AudioClip> _walkingSound;

    [Header("Tilemap and Collision Checker")]
    [SerializeField] private Tilemap _groundTilemap;
    [SerializeField] private Tilemap _collisionTilemap;
    [SerializeField] private LayerMask _collisionObject;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        _nextMovePoint.parent = null;
    }

    private void Update()
    {
        switch (state)
        {
            case playerState.MOVEABLE:
                Movement();
                break;
            case playerState.OCCUPIED:
                
                break;
        }
    }

    void Movement()
    {
        if (SceneLoader.Instance.transitioning) return;
        transform.position = Vector3.MoveTowards(transform.position, _nextMovePoint.position, _speed * Time.deltaTime);
        if (Vector3.Distance(transform.position, _nextMovePoint.position) <= _movementDelay)
        {
            Vector2 direction = PlayerInputController.Instance.GetMovementValue;
            if (direction.x != 0 || direction.y != 0)
            {
                if (CanMove(direction))
                {
                    //_animator.SetBool("Moving", true);
                    playingAnimation = true;
                    _animator.Play(_walking.name);

                    if (!Physics2D.OverlapCircle(_nextMovePoint.position+ new Vector3(direction.x, direction.y, 0f), 0.2f, _collisionObject))
                    {
                        _nextMovePoint.position += new Vector3(direction.x, direction.y, 0f);
                    }
                }
            }
        }
        if (transform.position == _nextMovePoint.position)
        {
            //_animator.SetBool("Moving", false);
            _animator.SetTrigger(_idleTrigger);
            playingAnimation = false;
        }


    }
    bool CanMove(Vector2 direction)
    {
        Vector3Int gridPosition = _groundTilemap.WorldToCell(_nextMovePoint.position + (Vector3)direction);
        if (!_groundTilemap.HasTile(gridPosition) || _collisionTilemap.HasTile(gridPosition))
        {
            return false;
        }

        return true;
    }

    private void PlayOneShot(AudioClip audioClip)
    {
        _audioSource.PlayOneShot(audioClip);
    }
    public void PlayWalkingSound()
    {
        if (_walkingSound.Count == 0) return;
        int newInt = Random.Range(0, _walkingSound.Count);
        _audioSource.pitch = (Random.Range(1.75f, 2.5f));
        PlayOneShot(_walkingSound[newInt]);
        //_audioSource.pitch = 1;
    }
}
