using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public enum unitState { IDLE, MOVING, OCCUPIED }
public class NPC : MonoBehaviour
{
    [Header("Unit State")]
    public unitState state = unitState.IDLE;

    [Header("Unit Profile")]
    [SerializeField] private UnitProfile _unitProfile;

    [Header("UI")]
    [SerializeField] private TextMeshProUGUI _name;
    [SerializeField] private SpriteRenderer _unitSprite;
    [SerializeField] private GameObject _npcName;
    [SerializeField] private GameObject _instruction;
    [HideInInspector] public bool inInteractRange = false;
    private bool _hovering = false;

    [Header("BATTLE")]
    public bool isDefeated = false;

    private void Start()
    {
        if (_unitProfile != null)
        {
            _name.text = _unitProfile.unitName;
            if (_unitProfile.unitSprite != null)
            {
                _unitSprite.sprite = _unitProfile.unitSprite;
            }
        }
        else
        {
            _name.text = "OBJECT";
        }
        _instruction.SetActive(false);
        _npcName.SetActive(false);
    }
    private void Update()
    {
        NPCState();
    }

    private void NPCState()
    {
        switch(state)
        {
            case (unitState.IDLE):
                MouseHovering();
                NPCNotificationBox();
                Interaction();
                break;
        }
    }
    public void NPCNotificationBox()
    {
        switch (inInteractRange)
        {
            case true:
                _instruction.SetActive(true);
                _npcName.SetActive(true);
                break;
            case false:
                if (!_hovering)
                {
                    _instruction.SetActive(false);
                    _npcName.SetActive(false);
                }
                else
                {
                    _instruction.SetActive(false);
                }
                break;
        }
    }
    private void Interaction()
    {
        if (inInteractRange && PlayerController.Instance.state == playerState.MOVEABLE && !PlayerController.Instance.playingAnimation)
        {
            if (PlayerInputController.Instance.Interact())
            {
                TurnBaseSystem.Instance.CurrentNPCTarget(this.gameObject);
                PlayDialogue(_unitProfile.startingDialogue);
            }
            if (PlayerMouseDetection.Instance.CheckMouseOnObject(this.gameObject))
            {
                if (PlayerInputController.Instance.LeftMouseClick())
                {
                    TurnBaseSystem.Instance.CurrentNPCTarget(this.gameObject);
                    PlayDialogue(_unitProfile.startingDialogue);
                }
            }
        }
    }
    private void PlayDialogue(DialogueGraph graph)
    {
        NodeReader.Instance.StartDialogue(graph);
    }



    public void MouseHovering()
    {
        //_hovering = true;
        //_npcName.SetActive(true);
        if (PlayerMouseDetection.Instance.CheckMouseOnObject(this.gameObject) && !GameManager.Instance.isPaused)
        {
            _hovering = true;
            _npcName.SetActive(true);
        }
        else
        {
            _hovering = false;
            _npcName.SetActive(false);
        }
    }
    public void MouseNotHovering()
    {
        _hovering = false;
        _npcName.SetActive(false);
    }
    public void ChangeProfile(UnitProfile newProfile)
    {
        _unitProfile = newProfile;
    }
}
