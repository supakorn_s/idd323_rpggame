using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerMusic : MonoBehaviour
{
    [SerializeField] private AudioClip _newBGM;

    private void Update()
    {
       
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Event();
        }
    }

    public void Event()
    {
        if (SoundManager.Instance.bgmSource.clip != _newBGM)
        {
            SoundManager.Instance.TriggerFade(_newBGM);
        }
    }
}
