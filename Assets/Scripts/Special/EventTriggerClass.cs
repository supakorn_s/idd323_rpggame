using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EventTriggerClass : MonoBehaviour
{
    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.TryGetComponent(out PlayerBrain brainScript))
            {
                Event();
                Destroy(this.gameObject);
            }
        }
    }
    public virtual void Event()
    {

    }
}
