using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeProfileEvent : EventClass
{
    [SerializeField] private UnitProfile _newDialogue;

    public override void Event()
    {
        _npc.ChangeProfile(_newDialogue);
    }

}
