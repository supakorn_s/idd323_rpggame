using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GettingNewSkillEvent : EventClass
{
    [SerializeField] private DialogueGraph _dialogueToPlay;
    [SerializeField] private AttackProfile[] _newSkill;
    private UnitProfile _playerProfile;

    private void Start()
    {
        _npc = GetComponent<NPC>();
        _playerProfile = PlayerBrain.Instance.playerProfile;
    }
    public override void Event()
    {
        for (int i = 0; i < _newSkill.Length; i++)
        {
            _playerProfile.AddNewSkill(_newSkill[i]);
        }
        NodeReader.Instance.StartDialogue(_dialogueToPlay);
    }

}
