using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDialogue : EventTriggerClass
{
    [SerializeField] private DialogueGraph _dialogueToPlay;

    public override void Event()
    {
        NodeReader.Instance.StartDialogue(_dialogueToPlay);
    }
}
