using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealBox : MonoBehaviour
{

    [SerializeField] private bool _resetEverything = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            if (collision.gameObject.TryGetComponent(out PlayerBrain brainScript))
            {
                Debug.Log("Nice");
                if (_resetEverything)
                {
                    brainScript.playerProfile.ResettingFightingStat();
                }
                else
                {
                    brainScript.playerProfile.RestoreCharacter();
                }
            }
        }
    }
}
