using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveEvent : EventClass
{
    [SerializeField] private Vector2 _targetLocation;

    public override void Event()
    {
        _npc.gameObject.transform.position = _targetLocation;
    }
}
