using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class EventClass : MonoBehaviour
{
    protected NPC _npc;
    private void Start()
    {
        _npc = GetComponent<NPC>();
    }

    private void Update()
    {
        if (_npc.isDefeated)
        {
            Event();
            Destroy(this);
        }
    }

    public virtual void Event()
    {
        
    }
}
