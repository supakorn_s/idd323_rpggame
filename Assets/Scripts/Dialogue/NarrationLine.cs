using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Narration Line", menuName = "Narration/Line", order = 0)]
public class NarrationLine : ScriptableObject
{
    [SerializeField] private NarrationLine _speaker;
    [Multiline(4)][SerializeField] private string _text;

    public NarrationLine speaker => _speaker;
    public string text => _text;
}
