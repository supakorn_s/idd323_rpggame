#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NodeReader))]
public class NodeReaderCustomEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        NodeReader _nodeReader = (NodeReader)target;
        if (GUILayout.Button("Start Dialogue"))
        {
            _nodeReader.StartDialogue(_nodeReader.graph);
        }
    }
}
#endif
