using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using XNode;

public class NodeReader : MonoBehaviour
{
    private static NodeReader _instance;
    public static NodeReader Instance => _instance;

    [Header("Currently played dialogue")]
    public DialogueGraph graph;

    [Header("UI")]
    [SerializeField] private GameObject _dialogueCanvas;
    [SerializeField] private GameObject _interactionCanvas;
    [SerializeField] private GameObject _choiceCanvas;
    [SerializeField] private Transform _choiceSpawnPosition;
    [SerializeField] private GameObject _dialogueChoicePrefab;
    [SerializeField] private Animator _dialogueAnim; 
    public TextMeshProUGUI speaker;
    public TextMeshProUGUI dialogue;
    public Image characterImage;

    [Header("Player")]
    [SerializeField] private PlayerController _playerController;

    [Header("Audio")]
    [SerializeField] private AudioSource _typingAudioSource;
    [SerializeField] private AudioClip _typingSound;

    #region Private
    private Coroutine _parser;
    private Coroutine _dialogue;
    private Coroutine _choices;
    private int _choiceNumber = -2;
    private int _chooseChoice = -2;
    private bool _dialogueComplete = false;
    private bool _typing = false;
    private string[] _dialogueData;
    private BaseNode _currentB;
    #endregion

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    private void Start()
    {
        _interactionCanvas.SetActive(false);
        _choiceCanvas.SetActive(false);
        _dialogueCanvas.SetActive(false);
        //StartDialogue(graph);
    }
    private void Update()
    {
        SkipDialogue();
        RunningDialogue();
    }
    public void StartDialogue(DialogueGraph newGraph)
    {
        graph = newGraph;
        _playerController.state = playerState.UNMOVEABLE;
        _dialogueCanvas.SetActive(true);
        _dialogueAnim.SetTrigger("in");

        foreach (BaseNode b in graph.nodes)
        {
            switch (b.GetString())
            {
                case "Start":
                    // Make this node a starting point
                    graph.current = b;
                    break;
            }
        }
        _parser = StartCoroutine(ParserNode());
    }
    public void CustomStartDialogue(string newText)
    {
        _dialogueCanvas.SetActive(true);
        _dialogueAnim.SetTrigger("in");
        _dialogueComplete = false;
        _dialogue = StartCoroutine(TypeSentence(dialogue, newText));

    }
    #region Structure
    IEnumerator ParserNode()
    {
        _dialogueComplete = false;
        BaseNode b = graph.current;
        string data = b.GetString();
        string[] dataParts = data.Split('/');
        
        _currentB = b;
        _dialogueData = dataParts;

        if (dataParts[0] == "Start")
        {
            NextNode("exit");
        }

        if (dataParts[0] == "DialogueNode")
        {
            // Run dialogue process
            _dialogue = StartCoroutine(TypeSentence(dialogue, dataParts[2]));
            speaker.text = dataParts[1];
            characterImage.sprite = b.GetSprite();

            /*yield return new WaitUntil(() => _dialogueComplete == true);
            dialogue.text = dataParts[2];
            yield return new WaitUntil(() => _typing == false);
            yield return new WaitUntil(() => PlayerInputController.Instance._inputMaster.Player.LeftMouse.ReadValue<float>() == 1);
            yield return new WaitUntil(() => PlayerInputController.Instance._inputMaster.Player.LeftMouse.ReadValue<float>() == 0);

            string nextNode = "exit " + b.GetInt(); 
            // "exit 0" = normal while <= 1 come from _priority, sesulted in different node
            NextNode(nextNode);*/
            yield return null;
        }

        if (dataParts[0] == "ChoiceNode")
        {
            // Run dialogue process
            _dialogue = StartCoroutine(TypeSentence(dialogue, dataParts[2]));
            speaker.text = dataParts[1];
            characterImage.sprite = b.GetSprite();

            _choices = StartCoroutine(ParseChoice());
            yield return null;
        }

        if (dataParts[0] == "End")
        {
            // Ending conversation here
            EndNode();
        }
        if (dataParts[0] == "BattleCutIn")
        {
            BattleCutIn();
        }
        if (dataParts[0] == "Battle")
        {
            BattleNode();
        }

    }
    IEnumerator CustomDialogue()
    {
        _dialogueComplete = false;
        _dialogue = StartCoroutine(TypeSentence(dialogue, "LMAO"));
        yield return null;
    }
    IEnumerator ParseChoice()
    {
        if (_choiceSpawnPosition.childCount > 0)
        {
            for (int i = 0; i < _choiceSpawnPosition.childCount; i++)
            {
                Destroy(_choiceSpawnPosition.GetChild(i).gameObject);
            }
        }
        _choiceCanvas.SetActive(true);
        string[] choiceTextArray = graph.current.GetStringArray();
        _choiceNumber = -2;
        _chooseChoice = -2;
        foreach (NodePort p in graph.current.Outputs)
        {
            _choiceNumber += 1;
            if (_choiceNumber >= 0)
            {
                GameObject dialogueChoice = Instantiate(_dialogueChoicePrefab, _choiceSpawnPosition);
                dialogueChoice.transform.localPosition = new Vector3(0, 0, 0);

                DialogueChoice choiceScript = dialogueChoice.GetComponent<DialogueChoice>();
                choiceScript.choiceText.text = choiceTextArray[_choiceNumber].ToString();
                choiceScript.choiceNumber = _choiceNumber;
                choiceScript.button.onClick.AddListener(delegate { ChooseChoice(choiceScript.choiceNumber); });
            }
        }
        _choiceCanvas.SetActive(false);
        yield return new WaitUntil(() => _dialogueComplete);
        _choiceCanvas.SetActive(true);
        yield return new WaitUntil(() => _chooseChoice != -2);

        for (int i = 0; i < _choiceSpawnPosition.childCount; i++)
        {
            Destroy(_choiceSpawnPosition.GetChild(i).gameObject);
        }
    }
    public void NextNode(string fieldName)
    {
        // Find the port with this name;
        if (_parser != null)
        {
            StopCoroutine(_parser);
            _parser = null;
        }

        foreach (NodePort p in graph.current.Ports)
        {
            // Check if this port is the one we're looking for.
            if (p.fieldName == fieldName)
            {
                graph.current = p.Connection.node as BaseNode;
                break;
            }
        }
        if (_dialogue != null) StopCoroutine(_dialogue);
        _parser = StartCoroutine(ParserNode());
    }
    #endregion
    public void RunningDialogueButton()
    {
        if (_dialogueComplete)
        {
            dialogue.text = _dialogueData[2];

            if (!_typing)
            {

                switch (graph.current.name)
                {
                    case "Choice":

                        break;
                    default:
                        string nextNode = "exit " + _currentB.GetInt();
                        // "exit 0" = normal while <= 1 come from _priority, sesulted in different node

                        _dialogueData = null;
                        _currentB = null;
                        NextNode(nextNode);
                        break;
                }

            }
        }
    }
    private void RunningDialogue()
    {
        if (_dialogueComplete)
        {
            dialogue.text = _dialogueData[2];

            if (!_typing)
            {
                switch (graph.current.name)
                {
                    // "exit 0" = normal while <= 1 come from _priority, sesulted in different node
                    case "Choice":
                        if (_chooseChoice != -2)
                        {
                            string nextNode = "exit " + _chooseChoice;
                            _dialogueData = null;
                            _currentB = null;
                            NextNode(nextNode);
                        }
                        break;
                    default:
                        if (PlayerInputController.Instance.SkipDialogue())
                        {
                            string nextNode = "exit " + _currentB.GetInt();
                            _dialogueData = null;
                            _currentB = null;
                            NextNode(nextNode);
                            return;
                        }
                        break;
                }
            }
        }
    }   // Spacebar
    public void SkipDialogueButton()
    {
        if (_typing)
        {
            switch (graph.current.name)
            {
                case "Choice":
                    _dialogueComplete = true;
                    break;
                default:
                    _dialogueComplete = true;
                    break;
            }
        }
    }
    private void SkipDialogue()
    {
        if (_typing)
        {
            /*if (PlayerInputController.Instance._inputMaster.Player.LeftMouse.triggered)
            {
                if (PlayerInputController.Instance._inputMaster.Player.LeftMouse.ReadValue<float>() == 0)
                {
                    _dialogueComplete = true;
                }
            }*/

            if (PlayerInputController.Instance.SkipDialogue())
            {
                _dialogueComplete = true;
            }

        }
    }   // Spacebar
    IEnumerator TypeSentence(TextMeshProUGUI textBox, string sentence)
    {
        _typing = true;
        textBox.text = "";

        // Problem apply Rich Text here
        StartTypingSound();
        foreach (char letter in sentence.ToCharArray())
        {
            textBox.text += letter;
            _typingAudioSource.pitch = (Random.Range(1.75f, 2.5f));

            yield return new WaitForSeconds(0.025f);
            if (_dialogueComplete)
            {
                break;
            }
        }

        _dialogueComplete = true;
        _typing = false;
        _typingAudioSource.Stop();
    }

    #region Special Node
    public void EndNode()
    {
        if (_parser != null)
        {
            StopCoroutine(_parser);
            _parser = null;
        }
        _playerController.state = playerState.MOVEABLE;
        _dialogueAnim.SetTrigger("out");
        graph = null;
        //Debug.Log("End node");
    }
    public void BattleCutIn()
    {
        if (_parser != null)
        {
            StopCoroutine(_parser);
            _parser = null;
        }
        _dialogueAnim.SetTrigger("out");
        graph = null;
        //Debug.Log("End node");
    }
    public void BattleNode()
    {
        BaseNode b = graph.current;

        if (_parser != null)
        {
            StopCoroutine(_parser);
            _parser = null;
        }
        // VVVVV Play BATTLE ANIMATION HERE VVVVV
        //_dialogueAnim.SetTrigger("out");
        //_dialogueCanvas.SetActive(false);
        //-------------------------------------
        TurnBaseSystem.Instance.SetupBattle(b.GetUnitProfile());
    }
    public void ChooseChoice(int NewChoice)
    {
        _chooseChoice = NewChoice;
    }

    #endregion

    public bool CheckDialogueIsPlaying
    {
        get
        {
            switch (graph != null)
            {
                case true:
                    return true;
                case false:
                    return false;
            }
        }
    }

    private void StartTypingSound()
    {
        if (_typingSound == null) return;
        //_typingAudioSource.pitch = (Random.Range(1.75f, 2.5f));
        _typingAudioSource.clip = _typingSound;
        _typingAudioSource.Play();
    }
}
