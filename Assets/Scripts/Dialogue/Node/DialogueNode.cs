﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class DialogueNode : BaseNode 
{

	[Input] public int entry;
    [Output(dynamicPortList = true)] public int[] exit;

    [Header("Dialogue Information")]
    [SerializeField] private NarrationCharacter _character;
    [TextArea(5, 20)][SerializeField] private string _dialogueLine;

    [HideInInspector] public int nextOutputPriority = 0;

    //public string speakerName;
    //public Sprite sprite;

    public override string GetString()
    {
        return "DialogueNode/" + _character.characterName + "/" + _dialogueLine;
    }
    public override Sprite GetSprite()
    {
        return _character.characterSprite;
    }
    public override int GetInt()
    {
        return nextOutputPriority;
    }
}