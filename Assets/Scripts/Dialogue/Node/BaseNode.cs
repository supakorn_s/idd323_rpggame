﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class BaseNode : Node {

	public virtual string GetString()
    {
        return null;
    }
    public virtual string[] GetStringArray()
    {
        return null;
    }
    public virtual Sprite GetSprite()
    {
        return null;
    }
    public virtual int GetInt()
    {
        return 0;
    }
    public virtual UnitProfile[] GetUnitProfile()
    {
        return null;
    }
}