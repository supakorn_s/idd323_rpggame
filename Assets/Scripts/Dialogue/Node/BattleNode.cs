﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class BattleNode : BaseNode
{
    [Input] public int entry;
    public UnitProfile[] unitBattleProfile;

    public override string GetString()
    {
        return "Battle";
    }
    public override UnitProfile[] GetUnitProfile()
    {
        return unitBattleProfile;
    }
}