﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XNode;

public class ChoiceNode : BaseNode 
{

    [Input] public int entry;
    [Output(dynamicPortList = true)] public int[] exit;

    [Header("Dialogue Information")]
    [SerializeField] private NarrationCharacter _character;
    [TextArea(2, 15)] [SerializeField] private string _dialogueLine;
    [TextArea(1, 5)] [SerializeField] private string[] _choiceLine;
    [HideInInspector] public int nextOutputPriority = 0;

    public override string GetString()
    {
        return "ChoiceNode/" + _character.characterName + "/" + _dialogueLine;
    }
    public override string[] GetStringArray()
    {
        return _choiceLine;
    }
    public override Sprite GetSprite()
    {
        return _character.characterSprite;
    }
    public override int GetInt()
    {
        return nextOutputPriority;
    }
}