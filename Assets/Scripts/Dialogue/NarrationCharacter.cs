using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Character", menuName = "Narration/Character", order = 1)]
public class NarrationCharacter : ScriptableObject
{
    [SerializeField] private Sprite _characterSprite;
    [SerializeField] private string _characterName;

    public Sprite characterSprite => _characterSprite;
    public string characterName => _characterName;

    public void ChangePlayerNarration(Sprite newSprite, string name)
    {
        _characterSprite = newSprite;
        _characterName = name;
    }
}
