using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DialogueChoice : MonoBehaviour
{
    public TextMeshProUGUI choiceText;
    public int choiceNumber;
    public Button button;


    public void OnClickUI()
    {
        SoundManager.Instance.OnClickUI();
    }
    public void OnCancelledUI()
    {
        SoundManager.Instance.OnCancelledUI();
    }
    public void OnHoverUI()
    {
        SoundManager.Instance.OnHoverUI();
    }
}
