using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CharacterCreationMneu : MonoBehaviour
{
    [Header("Save slot")]
    [SerializeField] private UnitProfile _basePlayerProfile;
    [SerializeField] private UnitProfile _playerProfileSlot;

    [Header("Character creation")]
    [SerializeField] private TMP_InputField _characterName;
    [SerializeField] private Image _characterImage;
    [SerializeField] private List<Sprite> _characterSprite;
    private int _currentCharacter = 0;

    [Header("Character Skill and Stat")]
    [SerializeField] private int _startingSkillPoint = 10;
    [SerializeField] private int _currentSkillPoint = 10;
    [SerializeField] private TextMeshProUGUI _totalSkillPoints;
    [SerializeField] private List<int> _numberOfStat;
    [SerializeField] private List<TextMeshProUGUI> _statNumber;
    [SerializeField] private List<Button> _increaseButton;
    [SerializeField] private List<Button> _decreaseButton;

    private void Start()
    {
        SetImage(_characterSprite[0]);
        _currentSkillPoint = _startingSkillPoint;
        _totalSkillPoints.text = _startingSkillPoint.ToString();
        for (int i = 0; i < _numberOfStat.Count ; i++)
        {
            _numberOfStat[i] = 0;
            _statNumber[i].text = _numberOfStat[i].ToString();
        }
        SetButton();
    }

    public void ChangeCharacterImageRight()
    {
        _currentCharacter += 1;
        if (_currentCharacter > (_characterSprite.Count - 1))
        {
            _currentCharacter = 0;
        }
        SetImage(_characterSprite[_currentCharacter]);
    }
    public void ChangeCharacterImageLeft()
    {
        _currentCharacter -= 1;
        if (_currentCharacter < 0)
        {
            _currentCharacter = _characterSprite.Count - 1;
        }
        SetImage(_characterSprite[_currentCharacter]);
    }
    public void ChangeCharacterName()
    {
        if (_characterName.text != "")
        {
            _playerProfileSlot.ChangeName(_characterName.text);
        }
        else
        {
            _playerProfileSlot.ChangeName("Johnson Joestar");
        }
    }

    private void SetImage(Sprite newCharacterImage)
    {
        _characterImage.sprite = newCharacterImage;
    }
    public void SetProfile()
    {
        _playerProfileSlot.SetNewProfile(_characterImage.sprite, _numberOfStat[0], _numberOfStat[1], _numberOfStat[2], _numberOfStat[3], _numberOfStat[4],
            _currentSkillPoint);
        _playerProfileSlot.DefaultStartingLevel();
        _playerProfileSlot.RestoreCharacter();
        _playerProfileSlot.ResetSkill(_basePlayerProfile.skillProfile);
        ChangeCharacterName();
    }
    
    private void ResetProfile()
    {
        _playerProfileSlot = _basePlayerProfile;
    }
    private void IncreaseStat(int statNumber)
    {
        if (_currentSkillPoint > 0)
        {
            _currentSkillPoint -= 1;
            _totalSkillPoints.text = _currentSkillPoint.ToString();
            _numberOfStat[statNumber] += 1;

            _statNumber[statNumber].text = _numberOfStat[statNumber].ToString();

        }
    }
    private void DecreaseStat(int statNumber)
    {
        if (_currentSkillPoint < _startingSkillPoint && _numberOfStat[statNumber] != 0)
        {
            _currentSkillPoint += 1;
            _totalSkillPoints.text = _currentSkillPoint.ToString();
            _numberOfStat[statNumber] -= 1;

            _statNumber[statNumber].text = _numberOfStat[statNumber].ToString();

        }
    }
    private void SetButton()
    {
        for (int i = 0; i < _increaseButton.Count; i++)
        {
            //_increaseButton[i]
            int statSlot = i;

            _increaseButton[i].onClick.AddListener(delegate { IncreaseStat(statSlot); });
            _decreaseButton[i].onClick.AddListener(delegate { DecreaseStat(statSlot); });
        }
    }
}
