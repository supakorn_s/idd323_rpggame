using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class MenuTabManager : MonoBehaviour
{
    [SerializeField] private List<GameObject> _tabList;
    [SerializeField] private List<GameObject> _specialObject;
    [SerializeField] private TextMeshProUGUI _header;
    [SerializeField] private GameObject _MenuTab;
    [SerializeField] private GameObject _characterTab;
    [SerializeField] private GameObject _characterStatTab;
    [SerializeField] private GameObject _SettingTab;
    [SerializeField] private ResolutionSetting _resolutionSetting;

    [Header("BUILD/WEB")]
    [SerializeField] private bool _isWebBuild = false;

    #region Player Stat
    private UnitProfile _playerProfile;
    [SerializeField] private List<Image> _playerImage;
    [SerializeField] private CharacterStat _currentStat;
    [SerializeField] private TextMeshProUGUI _skillPoint;
    [SerializeField] private TextMeshProUGUI _playerName;
    [SerializeField] private TextMeshProUGUI _vitality;
    [SerializeField] private TextMeshProUGUI _intelligence;
    [SerializeField] private TextMeshProUGUI _design;
    [SerializeField] private TextMeshProUGUI _code;
    [SerializeField] private TextMeshProUGUI _defence;
    [SerializeField] private TextMeshProUGUI _level;
    [SerializeField] private TextMeshProUGUI _HP;
    [SerializeField] private TextMeshProUGUI _currentHP;
    [SerializeField] private TextMeshProUGUI _idea;
    [SerializeField] private TextMeshProUGUI _logic;
    [SerializeField] private TextMeshProUGUI _baseAttack;
    [SerializeField] private TextMeshProUGUI _baseDefence;
    [SerializeField] private TextMeshProUGUI _expNeed;
    [SerializeField] private TextMeshProUGUI _expHave;
    [SerializeField] private List<Button> _increaseButton;
    [SerializeField] private List<int> _shownStat;

    #endregion

    private void Start()
    {
        _resolutionSetting.dropDownCurrentStartResolution();
        WebCheck();
        for (int i = 0; i < _increaseButton.Count; i++)
        {
            int statSlot = i;
            _increaseButton[i].onClick.AddListener(delegate { IncreaseStat(statSlot); });
        }

        for (int i = 0; i < _tabList.Count; i++)
        {
            _tabList[i].SetActive(false);
        }
        _MenuTab.SetActive(false);
    }
    private void Update()
    {
        OpenCloseMenuTab();
    }
    private void OpenCloseMenuTab()
    {
        if (SceneLoader.Instance.transitioning == false && NodeReader.Instance.graph == null && !TurnBaseSystem.Instance.inbattle)
        {
            if (PlayerInputController.Instance.Tab() || PlayerInputController.Instance.Escape())
            {
                if (GameManager.Instance.isPaused)
                {
                    Resume();
                }
                else
                {
                    Pause();
                }
            }
        }
    }
    public void OpenCloseSubTab(int tabNumber)
    {
        for (int i = 0; i < _tabList.Count; i++)
        {
            _tabList[i].SetActive(false);
        }
        _tabList[tabNumber].SetActive(true);

        switch (tabNumber)
        {
            case 0:
                _header.text = "STAT";
                break;
            case 1:
                _header.text = "CHARACTER";
                break;
            case 2:
                _header.text = "SETTINGS";
                break;
            default:
                _header.text = "MENU";
                break;
        }
    }

    private void CheckPlayerInformation()
    {
        _playerProfile = PlayerBrain.Instance.playerProfile;
        _playerName.text = _playerProfile.unitName;
        UpdateStatText();

        _shownStat.Clear();
        _shownStat.Add(_playerProfile.stat.vitality);
        _shownStat.Add(_playerProfile.stat.intelligence);
        _shownStat.Add(_playerProfile.stat.design);
        _shownStat.Add(_playerProfile.stat.code);
        _shownStat.Add(_playerProfile.stat.defence);
        for (int i = 0; i < _playerImage.Count; i++)
        {
            _playerImage[i].sprite = _playerProfile.unitSprite;
        }

    }
    private void Pause()
    {
        CheckPlayerInformation();
        PlayerController.Instance.state = playerState.UNMOVEABLE;
        _MenuTab.SetActive(true);
        GameManager.Instance.isPaused = true;
        Time.timeScale = 0.0f;
    }
    private void Resume()
    {
        PlayerController.Instance.state = playerState.MOVEABLE;
        _MenuTab.SetActive(false);
        GameManager.Instance.isPaused = false;
        Time.timeScale = 1f;
    }
    public void loadScene(int SceneNumber)
    {
        SceneLoader.Instance.startLoadLevel(SceneNumber);
    }
    public void IncreaseStat(int statNumber)
    {
        if (_playerProfile.stat.skillPoint > 0)
        {
            _playerProfile.stat.skillPoint -= 1;
            _skillPoint.text = _playerProfile.stat.skillPoint.ToString();

            _shownStat[statNumber] += 1;

            switch (statNumber)
            {
                case 0:
                    _vitality.text = _shownStat[statNumber].ToString("F0");
                    _playerProfile.stat.vitality += 1;
                    break;
                case 1:
                    _intelligence.text = _shownStat[statNumber].ToString("F0");
                    _playerProfile.stat.intelligence += 1;
                    break;
                case 2:
                    _design.text = _shownStat[statNumber].ToString("F0");
                    _playerProfile.stat.design += 1;
                    break;
                case 3:
                    _code.text = _shownStat[statNumber].ToString("F0");
                    _playerProfile.stat.code += 1;
                    break;
                case 4:
                    _defence.text = _shownStat[statNumber].ToString("F0");
                    _playerProfile.stat.defence += 1;
                    break;
            }
            UpdateStatText();
        }
    }

    private void UpdateStatText()
    {
        _vitality.text = _playerProfile.stat.vitality.ToString("F0");
        _intelligence.text = _playerProfile.stat.intelligence.ToString("F0");
        _design.text = _playerProfile.stat.design.ToString("F0");
        _code.text = _playerProfile.stat.code.ToString("F0");
        _defence.text = _playerProfile.stat.defence.ToString("F0");
        _level.text = _playerProfile.stat.currentLevel.ToString();
        _skillPoint.text = _playerProfile.stat.skillPoint.ToString();

        _HP.text = _playerProfile.stat.GetCurrentMaxHP.ToString("F0");
        _currentHP.text = _playerProfile.stat.currentHP.ToString("F0");
        _idea.text = _playerProfile.stat.GetCurrentIdea.ToString("F0");
        _logic.text = _playerProfile.stat.GetCurrentLogic.ToString("F0");
        _baseAttack.text = _playerProfile.stat.GetCurrentAttack.ToString("F0");
        _baseDefence.text = _playerProfile.stat.GetCurrentDefence.ToString("F0");

        _expHave.text = _playerProfile.stat.currentEXP.ToString("F0");
        _expNeed.text = _playerProfile.stat.requireEXP.ToString("F0");

        for (int i = 0; i < _increaseButton.Count; i++)
        {
            if (_playerProfile.stat.skillPoint == 0)
            {
                _increaseButton[i].gameObject.SetActive(false);
            }
            else
            {
                _increaseButton[i].gameObject.SetActive(true);
            }
        }
    }

    private void WebCheck()
    {
        switch (_isWebBuild)
        {
            case true:
                if (_specialObject.Count != 0)
                {
                    for (int i = 0; i < _specialObject.Count; i++)
                    {
                        _specialObject[i].SetActive(false);
                    }
                }
                break;

            case false:
                if (_specialObject.Count != 0)
                {
                    for (int i = 0; i < _specialObject.Count; i++)
                    {
                        _specialObject[i].SetActive(true);
                    }
                }
                break;
        }
    }






    public void OnClickUI()
    {
        SoundManager.Instance.OnClickUI();
    }
    public void OnCancelledUI()
    {
        SoundManager.Instance.OnCancelledUI();
    }
    public void OnHoverUI()
    {
        SoundManager.Instance.OnHoverUI();
    }
}