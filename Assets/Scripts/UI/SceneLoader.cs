using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public bool transitioning = false;

    [SerializeField] Animator transition;
    [SerializeField] GameObject image;
    [SerializeField] float transitiontime;

    private static SceneLoader _instance;
    public static SceneLoader Instance => _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        image.SetActive(true);
    }

    public void startLoadLevel(int level)
    {
        StartCoroutine(loadlevel(level));
    }

    public void exitToDestop()
    {
        StartCoroutine(exitGame());
    }

    IEnumerator loadlevel(int levelindex)
    {
        Time.timeScale = 1;

        transition.SetTrigger("FadeOut");

        yield return new WaitForSeconds(transitiontime);

        SceneManager.LoadScene(levelindex);

    }
    IEnumerator exitGame()
    {
        Time.timeScale = 1;

        transition.SetTrigger("FadeOut");

        yield return new WaitForSeconds(transitiontime);

        Application.Quit();

    }

    public void TransitioningBegin()
    {
        transitioning = true;
    }
    public void TransitioningEnd()
    {
        transitioning = false;
    }
}
