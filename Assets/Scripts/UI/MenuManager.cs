using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [Header("Menu List")]
    [SerializeField] private GameObject _mainMenu;
    [SerializeField] private GameObject _characterCreationTab;
    [SerializeField] private GameObject _settingMenu;
    [SerializeField] private GameObject _creditMenu;
    [SerializeField] private List<GameObject> _specialObject;
    [SerializeField] private ResolutionSetting _resolutionSetting;

    [Header("BUILD/WEB")]
    [SerializeField] private bool _isWebBuild = false;

    private void Awake()
    {
        RunStartupCheck();
        WebCheck();
    }
    private void Start()
    {
        _mainMenu.SetActive(true);
        _settingMenu.SetActive(false);
        _creditMenu.SetActive(false);
        _characterCreationTab.SetActive(false);

        SoundManager.Instance.PlayWorldBGM(0);
    }

    public void OpenCharacterCreationMenu()
    {
        CloseAll();
        _characterCreationTab.SetActive(true);
    }
    public void OpenMainMenu()
    {
        CloseAll();
        _mainMenu.SetActive(true);
    }
    public void OpenSetting()
    {
        CloseAll();
        _settingMenu.SetActive(true);
    }
    public void OpenCredit()
    {
        CloseAll();
        _creditMenu.SetActive(true);
    }
    public void ExitGame()
    {
        SceneLoader.Instance.exitToDestop();
    }
    private void CloseAll()
    {
        _mainMenu.SetActive(false);
        _settingMenu.SetActive(false);
        _creditMenu.SetActive(false);
        _characterCreationTab.SetActive(false);
    }

    public void loadScene(int SceneNumber)
    {
        SoundManager.Instance.PauseWorldBGM();
        SoundManager.Instance.ClearWorldBGM();
        SceneLoader.Instance.startLoadLevel(SceneNumber);
    }



    private void RunStartupCheck()
    {
        _settingMenu.SetActive(true);
        _resolutionSetting.dropDownCurrentStartResolution();
    }
    private void WebCheck()
    {
        switch (_isWebBuild)
        {
            case true:
                if (_specialObject.Count != 0)
                {
                    for (int i = 0; i < _specialObject.Count; i++)
                    {
                        _specialObject[i].SetActive(false);
                    }
                }
                break;

            case false:
                if (_specialObject.Count != 0)
                {
                    for (int i = 0; i < _specialObject.Count; i++)
                    {
                        _specialObject[i].SetActive(true);
                    }
                }
                break;
        }
    }


    public void OnClickUI()
    {
        SoundManager.Instance.OnClickUI();
    }
    public void OnCancelledUI()
    {
        SoundManager.Instance.OnCancelledUI();
    }
    public void OnHoverUI()
    {
        SoundManager.Instance.OnHoverUI();
    }
}
