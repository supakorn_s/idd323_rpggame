using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum cameraState { MOVEABLE, UNMOVEABLE }
public class CustomCamera : MonoBehaviour
{
    public cameraState state = cameraState.MOVEABLE;

    [Header("Camera Setting")]
    [SerializeField] private Vector2 _startingPosition;
    [SerializeField] private Vector2 _margin = new Vector2(10, 10);
    [SerializeField] private Vector2 _smooth = new Vector2(5, 5);
    [SerializeField] private Vector2 _minBorder = new Vector2(-25, -25);
    [SerializeField] private Vector2 _maxBorder = new Vector2(25, 25);
    [SerializeField] private Transform _targetToFollow;
    // Variable and Ref
    Camera _cam;

    void Start()
    {
        _cam = GetComponentInChildren<Camera>();
        if (_startingPosition != null)
        {
            transform.position = _startingPosition;
        }
    }

    private void Update()
    {
        if (_cam == null)
        {
            return;
        }
        switch (state)
        {
            case cameraState.MOVEABLE:
                CameraMovement();
                break;
            case cameraState.UNMOVEABLE:

                break;
        }
    }

    void CameraMovement()
    {
        float targetX = transform.position.x;
        float targetY = transform.position.y;
        if (CheckX) targetX = Mathf.Lerp(transform.position.x, _targetToFollow.position.x, _smooth.x * Time.deltaTime);
        if (CheckY) targetY = Mathf.Lerp(transform.position.y, _targetToFollow.position.y, _smooth.y * Time.deltaTime);
        targetX = Mathf.Clamp(targetX, _minBorder.x, _maxBorder.x);
        targetY = Mathf.Clamp(targetY, _minBorder.y, _maxBorder.y);


        transform.position = new Vector3(targetX, targetY, transform.position.z);
    }
    bool CheckX
    {
        get
        {
            return Mathf.Abs(transform.position.x - _targetToFollow.position.x) > _margin.x;
        }
    }
    bool CheckY
    {
        get
        {
            return Mathf.Abs(transform.position.y - _targetToFollow.position.y) > _margin.y;
        }
    }
}
