using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SingletonClass<T> : MonoBehaviour where T : Component
{
    private static T _instance;
    public static T Instance => _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            _instance = this as T;
        }
        DontDestroyOnLoad(this);
    }

    private void OnDestroy()
    {
        if (_instance == this)
        {
            _instance = null;
        }
    }
}
