using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SingletonStaticClass<T> : MonoBehaviour where T : Component
{
    private static T _instance;
    public static T Instance => _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(gameObject);
            return;
        }
        transform.parent = null;

        _instance = this as T;
        DontDestroyOnLoad(gameObject);
    }
}
