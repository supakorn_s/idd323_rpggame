using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinLoseManager : MonoBehaviour
{
    public bool isWin;
    [SerializeField] private List<NPC> _NPCToWin;

    [SerializeField] private GameObject _winCanvas;

    private static WinLoseManager _instance;
    public static WinLoseManager Instance { get { return _instance; } }

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    private void Start()
    {
        _winCanvas.SetActive(false);
    }
    private void Update()
    {
        if (isWin)
        {
            _winCanvas.SetActive(true);
        }
    }
    public void WinCheck()
    {
        int winCheck = 0;
        for (int i = 0; i < _NPCToWin.Count; i++)
        {
            if (_NPCToWin[i].isDefeated)
            {
                winCheck++;
            }
        }

        if (winCheck >= (_NPCToWin.Count))
        {
            isWin = true;
        }
    }
}
